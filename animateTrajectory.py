#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pickle
import itertools
import csv
import numpy as np
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import pathPlanning_v2
import searchShelf

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")
MAP_IMAGE = os.path.join(DATA_PATH, "map/Map_image.png") #プレーンなやつ

DRAW_THRESHOLD_MIN = 0
DRAW_THRESHOLD_MAX = 59

#会場の広さに関連する数字
#ちなみにBLEデータは左下が原点らしい
ENV_X = 110 #ヨコ方向
ENV_Y = 76.5 #タテ方向

PICK_TIME = 0
DRAW_SIM_PATH = True
DRAW_OPT_PATH = False
DRAW_TURN_POINT = False
# startTime / wearingStart / endTime
TIME_TABLE_LIST = [ [], [1485239145.,1485240508.,1485252057.],
                    [1485239148.,1485240513.,1485248722.],
                    [1485239234.,1485240524.,1485252460.],
                    [1485239243.,1485240545.,1485252313.],
                    [1485239286.,1485240550.,1485252407.],
                    [1485239291.,1485240555.,1485252344.],
                    [1485239328.,1485240562.,1485252227.],
                    [1485239334.,1485240568.,1485252085.]]


def run(agentNo, drawFlag, dataPath):
    # 各pickleファイルの読み込み
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    # xlim = mapInfoList[0][0]
    # ylim = mapInfoList[0][1]
    startPos = (43.3,46.6)


    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
    # print(gridMap)
    cellSize = gridMap[0][1].posx - gridMap[0][0].posx

    with open(os.path.join(DATA_PATH,"wms/WMS_comb.csv"), 'r') as f:
        wmsData = csv.reader(f)

        # header行を読み飛ばす
        header = next(wmsData)
        next(wmsData)

        wmsList = []
        for line in wmsData:
            # print(line)
            terminalID = line[0]
            shelfNO = int(line[1])
            utime = int(line[2])

            if int(terminalID) == agentNo:
                if utime < TIME_TABLE_LIST[agentNo][1]:
                    print(shelfNO, utime, "xxxx")
                    continue
                wmsList.append([shelfNO, utime])
                print(utime, shelfNO)

    print("length of wmsList...", len(wmsList))
    # リストを時間でソート
    # wmsList.sort(key=lambda x:x[1])

    passList = []
    with open(os.path.join(dataPath,"wms/nearFromBLE"+str(agentNo)+".csv"), 'r') as f:
        pList = csv.reader(f)
        header = next(pList)
        for line in pList:
            utime = float(line[0])
            posx = float(line[1])
            posy = float(line[2])
            action = line[3]
            if action == "Pass":
                passList.append([utime, (posx, posy), action])

    eventList = []
    eventList.extend(wmsList)
    eventList.extend(passList)
    eventList.sort(key=lambda x:x[0])
    print("length of eventList...", len(eventList))


# pickleDumpArray[agentNo].append([step, sim.getGlobalTime(), agentsPos[agentNo], agentsDestPos[agentNo], agentsDestAttribute[agentNo]])
    with open(os.path.join(DATA_PATH, 'simulation/'+ str(agentNo) + '_simulatedTrajectory.pickle'), mode = 'rb') as f:
        agentTrajectory = pickle.load(f)

    result = []
    pathLength = 0.
    prevStep = None
    prevTime = 0.
    prevPos = None
    prevState = None
    prevPick = "start"
    prevRecord = None
    for record in agentTrajectory:
        # print(record)
        if prevPos is None:
            prevPos = np.array(record[2])
            prevStep = record[0]
            prevTime = record[1]
            currentState = None
            # prevWmsTime = 0
            prevRecord = record
            continue
        currentPos = np.array(record[2])
        distance = np.linalg.norm(currentPos - prevPos)
        pathLength += distance
        # print(currentPos, prevPos, distance)
        # print(record[-1])
        prevState = currentState
        currentState = record[-1][0]
        # print(currentState)

        if prevState == 'Pick' and currentState == 'Turn':
        # if record[-1][0] == 'Pick':
            print("pick", prevRecord[-1][1])
            step = prevRecord[0]
            time = prevRecord[1]
            diffStep = step - prevStep
            diffTime = time - prevTime
            pick = prevRecord[-1][1]
            pathLength = 0.
            prevTime = time
            prevStep = step
            prevPick = pick
        prevRecord = record
        prevPos = currentPos

    if drawFlag:

        #map画像の読み込み
        img = plt.imread(MAP_IMAGE)
        magX =  len(img[0])/ENV_X
        magY =  -1 * len(img)/ENV_Y

        # 表示用pltの設定
        #図のオブジェクト作成と，イメージ表示
        fig = plt.figure(figsize=(int(len(img[0])/75), int(len(img)/75)))
        ax = fig.add_subplot(111)
        ax.spines["right"].set_color("none")  # 右消し
        ax.spines["left"].set_color("none")   # 左消し
        ax.spines["top"].set_color("none")    # 上消し
        ax.spines["bottom"].set_color("none") # 下消し
        ax.tick_params(labelbottom="off",bottom="off") # x軸の削除
        ax.tick_params(labelleft="off",left="off") # y軸の削除
        ax.imshow(img)

        turnPointX = []
        turnPointY = []
        pickPointX = []
        pickPointY = []
        passedPointX = []
        passedPointY = []

        fname = os.path.join(DATA_PATH,"occupancyPath/pathFileList_"+ str(agentNo)+ ".txt")
        pathList = []
        with open(fname, 'r') as f:
            data = f.readlines()
            pathList = [line.split("\n")[0] for line in data]

        # 各パスの描画と方向転換点，ピック点を描画
        optPathList = []
        for fName in pathList[DRAW_THRESHOLD_MIN:DRAW_THRESHOLD_MAX]:
            pickleName = fName[:-3]+"pickle"
            pickleName = os.path.join(DATA_PATH, "path/" +pickleName.split("/")[-1])
            with open(pickleName, mode='rb') as f:
                path = pickle.load(f)

            prevDirection = None
            for (i,address) in enumerate(path):
                node = gridMap[address[0][1]][address[0][0]]
                if i == 0:
                    # print("Red path is ", path.routeCost, " in ", len(path) -1, " step\n")
                    prevNode = node
                    prevDirection = address[-1]
                    # pickPointX.append(node.posx)
                    # pickPointY.append(node.posy)
                    continue
                if i == len(path) -1:
                    if len(fName[:-3].split(",")[-1].split(" ")) > 1:
                        passedPointX.append(node.posx *magX)
                        passedPointY.append(node.posy *magY + len(img))
                    else:
                        pickPointX.append(node.posx *magX)
                        pickPointY.append(node.posy *magY + len(img))
                else:
                    # if DRAW_OPT_PATH:
                    optPathList.append([[node.posx *magX, prevNode.posx *magX], [node.posy *magY + len(img), prevNode.posy *magY + len(img)]])
                    prevNode = node
                    # print(address)
                    if address[-1] != prevDirection:
                        turnPointX.append(node.posx *magX)
                        turnPointY.append(node.posy *magY + len(img))
                    prevDirection = address[-1]
        # if startPos is not None:
            # pt = ax.scatter(startPos[0], startPos[1], color = '#2ca9e1')


        if DRAW_TURN_POINT:
            pt1 = ax.scatter(turnPointX,turnPointY,color = '#07d34f',s = 20)
        pt2 = ax.scatter(passedPointX,passedPointY,color = '#A15DC4', s = 70)
        pt3 = ax.scatter(pickPointX,pickPointY,color = '#c92b29', s = 70)


        stepThresholdMin = 0
        stepThresholdMax = 0
        startDest = 0
        endDest = "767"

        startStep = 0
        endStep = len(agentTrajectory) - 1
        startFlag = True
        endFlag = False
        for record in agentTrajectory:
            if startFlag and record[-1][1] == startDest:
                startFlag = False
                startStep = record[0]
            if not endFlag and record[-1][1] == endDest:
                endFlag = True
            if endFlag and not record[-1][1] == endDest:
                endStep = record[0]
                break
        # print(startDest, endDest)
        # print(pathList[DRAW_THRESHOLD_MIN])
        # print(pathList[DRAW_THRESHOLD_MAX])
        # print(startStep,endStep,len(agentTrajectory))

        # startStep = 0
        # endStep = len(agentTrajectory) -1
        agentPosx = [pos[2][0] for pos in agentTrajectory[startStep :endStep]]
        agentPosy = [pos[2][1] for pos in agentTrajectory[startStep :endStep]]
        timeCount = [float(pos[1]) for pos in agentTrajectory[startStep :endStep]]
        # agentPosx = [pos[2][0] for pos in agentTrajectory]
        # agentPosy = [pos[2][1] for pos in agentTrajectory]
        # timeCount = [float(pos[1]) for pos in agentTrajectory]

        prevPosx = agentPosx[0]
        prevPosy = agentPosy[0]
        simPathList = []
        for i in range(1, len(agentPosx)):
            posx = agentPosx[i]
            posy = agentPosy[i]
            # print(posx, posy
            # if DRAW_SIM_PATH:
            #     plt.plot([posx, prevPosx], [posy, prevPosy], color = '#2ca9e1')
            simPathList.append([[posx *magX, prevPosx *magX], [posy *magY + len(img), prevPosy *magY + len(img)]])
            prevPosx = posx
            prevPosy = posy

        objs = []
        optDrawListX = []
        optDrawListY = []
        simDrawListX = []
        simDrawListY = []
        if DRAW_SIM_PATH:
            for i in range(0,int(len(simPathList)/10)):
                # print(opt,opt[0][1], opt[1][1])
                # print(simPathList[i*10], i, len(simPathList)/10)
                simDrawListX.extend([sim[0][1] for sim in simPathList[(i-1)*10:i*10]])
                simDrawListY.extend([sim[1][1] for sim in simPathList[(i-1)*10:i*10]])
                # for o in optDrawList:
                    # imOpt, = ax.plot(opt[0],opt[1], color = '#07d34f', lw = 2)
                imSim, = ax.plot(simDrawListX,simDrawListY, color = '#2ca9e1', lw = 4)
                objs.append([imSim])
            simDrawListX.extend([sim[0][1] for sim in simPathList[(int(len(simPathList)/10)-1)*10:]])
            simDrawListY.extend([sim[1][1] for sim in simPathList[(int(len(simPathList)/10)-1)*10:]])
            ani = animation.ArtistAnimation(fig, objs, interval=100, repeat=True)
            ani.save(os.path.join(DATA_PATH,'sim.mp4'))

        if DRAW_OPT_PATH:
            for i in range(0,int(len(optPathList)/5)):
                # print(opt,opt[0][1], opt[1][1])
                optDrawListX.extend([opt[0][1] for opt in optPathList[(i-1)*5:i*5]])
                optDrawListY.extend([opt[1][1] for opt in optPathList[(i-1)*5:i*5]])
                # for o in optDrawList:
                    # imOpt, = ax.plot(opt[0],opt[1], color = '#07d34f', lw = 2)
                imOpt, = ax.plot(optDrawListX,optDrawListY, color = '#07d34f', lw = 4)
                objs.append([imOpt])
            optDrawListX.extend([opt[0][1] for opt in optPathList[(int(len(optPathList)/5)-1)*5:]])
            optDrawListY.extend([opt[1][1] for opt in optPathList[(int(len(optPathList)/5)-1)*5:]])
            ani = animation.ArtistAnimation(fig, objs, interval=200, repeat=True)
            ani.save(os.path.join(DATA_PATH,'opt.mp4'))
        print(len(simPathList), len(optPathList))
        plt.show()



if __name__ == "__main__":
    agentNo = 4
    drawFlag = True
    run(agentNo, drawFlag, DATA_PATH)
