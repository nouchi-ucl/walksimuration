#!/usr/bin/python
# -*- coding: utf-8 -*-

# import wmsPlanning_multi_v2
import os
import wmsPlanning_multi_v4

# runlist = [7,8]
# runlist = [4,5,6]
# runlist = [3,4,5,6,7,8]
runlist = [6,7,5,3]
DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")

for userNum in runlist:
    wmsPlanning_multi_v4.run(DATA_PATH, userNum, False)
    # wmsPlanning_multi_v2.run(userNum, False)
