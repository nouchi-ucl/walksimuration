#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import searchShelf

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

mapInfoList = []
obstacles = []
shelf = []

# pointがx1,x2,y1,y2に囲まれた矩型領域内に存在するかどうかの判定
# x1<x2, y1<y2でないといけないので引数に注意
def inRectangle(point,x1,x2,y1,y2):
    x = point[0]
    y = point[1]
    if x1 <= x and x <= x2 and y1 <= y and y <= y2:
        return True
    else:
        return False


def searchShelfFromCoordinate(pos):
    shelfList = []
    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelfList = pickle.load(f)
    shelfID = None
    for shelf in shelfList:
        x1 = shelf[1]
        x2 = shelf[2]
        y1 = shelf[3]
        y2 = shelf[4]
        if inRectangle(pos, x1,x2,y1,y2):
            shelfID = shelf[0]
            break
    print("searchShelf ",shelfID, pos)

    return shelfID


# # pickleファイルからオブジェクトを復元
with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
    mapInfoList = pickle.load(f)

with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
    shelf = pickle.load(f)

with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
    obstacles = pickle.load(f)

xlim = mapInfoList[0][0]
ylim = mapInfoList[0][1]
print("mapSize...", xlim,ylim)

# 別であらかじめ作成しておいたgridMapをロード
# makeOccupancyGrid.py
with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
    gridMap = pickle.load(f)
# print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
# print(gridMap)
cellSize = gridMap[0][1].posx - gridMap[0][0].posx
print("cellSize", cellSize)

with open(os.path.join(DATA_PATH, "occupancyPath/wayPointList_6.pickle"), 'rb') as f:
    pathData = pickle.load(f)

# for data in pathData:
#     print(data)

for data in pathData:
    if data[-1] == 'Pick':
        print(data)
        shelfID = int(data[-2][0].split(".")[0])
        # print(shelfID)
