#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef


# MAP_Path = os.path.join("/media/sf_Ubuntu-simpy/workspace/sample_dataset_20170602/WarehouseB_30min/map")
# MAP_Path = os.path.join("/Users/nouchi/Desktop/ipin_comp/map")
DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp/pickle")

ALLOW_DIAGONAL_MOVE = False
DIAGONAL_MOVE_COST = 1.4
REQUIRED_PATH_NUM = 10
TURN_COST = 2.0


# x,yの座標からその点が含まれるセルのアドレスを返す point -> gridMap[address[1]][address[0]]
def searchNodeFromPoint(gridMap, point, cellSize):
    address = None
    for line in gridMap:
        yDist = abs(line[0].posy - point[1])
        if yDist <= cellSize:
            for node in line:
                xDist = abs(node.posx - point[0])
                if xDist <= cellSize:
                    address = node.address
    if address is None:
        print("Target point ", point, " is out of map region")
        sys.exit()
    return address

# パスの更新:1マス進んだ先のノード(newNode)をpathに追加していく
# 端部のノードの更新, routeへ追加, path全体コストの更新
# コストは斜め方向に移動する場合は x1.4
# 前回の移動方向に対して回転している場合はコストを追加
def updatePath(path, newNode):
    newPath = copy.deepcopy(path)

    newPath.prevNode = newPath.edgeNode
    newPath.edgeNode = newNode
    newPath.route.append(newNode)
    # if path.isTurn(newNode):
    #     newPath.routeCost += TURN_COST
    #
    moveFlag, turnFlag, direction = newPath.calcDirection(newNode,True)
    if turnFlag:
        newPath.routeCost += TURN_COST

    if moveFlag == False:
        return newPath
    elif direction %10 == 0.0:
        newPath.routeCost += newNode.cost
    else:
        newPath.routeCost += newNode.cost * DIAGONAL_MOVE_COST
    # if direction == 'up' or direction == 'down' or direction == 'right' or direction == 'left':
    #     newPath.routeCost += newNode.cost
    # else:
    #     newPath.routeCost += newNode.cost * DIAGONAL_MOVE_COST
    newPath.moveDirection = direction

    return newPath

# 作成したマップを使って,2地点間の最小コスト経路を探索
# requiredGoalPathNum の本数分探索が完了したら終了
# 通過セルが1つでも違う場合は別の経路として扱う
# 計算上,最速でゴールに到着したpathが最小コストになるとは限らない
#   方向転換によるコスト,斜めへの移動があるため
def pathPlanning(gridMap, startNode, goalNode):
    print("Path Planning...")
    requiredGoalPathNum = REQUIRED_PATH_NUM
    goalPath = []
    currentPath = []
    neighborNodeList = []
    currentPath.append(pathDef.Path(gridMap[startNode[1]][startNode[0]]))
    goal = gridMap[goalNode[1]][goalNode[0]]
    # print("goal ", goal)
    lastFindCount = 0
    stepCount = 0
    while(True):
        if lastFindCount != 0 and lastFindCount + 10 < stepCount:
            print("goalPath is not enough ...", len(goalPath), " / ", requiredGoalPathNum)
            return goalPath
        prevPath = copy.deepcopy(currentPath)
        currentPath.clear()
        print("In ",stepCount," step, there are ",len(prevPath)," route.")
        stepCount += 1
        for path in prevPath:
            # print("currentNode ", path.edgeNode.address, "/pos ", path.edgeNode.posx, path.edgeNode.posy)
            neighborNodeList.clear()
            if path.edgeNode.neighborUp is not None:
                neighborNodeList.append(path.edgeNode.neighborUp)
            if path.edgeNode.neighborDown is not None:
                neighborNodeList.append(path.edgeNode.neighborDown)
            if path.edgeNode.neighborRight is not None:
                neighborNodeList.append(path.edgeNode.neighborRight)
            if path.edgeNode.neighborLeft is not None:
                neighborNodeList.append(path.edgeNode.neighborLeft)

            if ALLOW_DIAGONAL_MOVE:
                if path.edgeNode.neighborRightUp is not None:
                    neighborNodeList.append(path.edgeNode.neighborRightUp)
                if path.edgeNode.neighborRightDown is not None:
                    neighborNodeList.append(path.edgeNode.neighborRightDown)
                if path.edgeNode.neighborLeftUp is not None:
                    neighborNodeList.append(path.edgeNode.neighborLeftUp)
                if path.edgeNode.neighborLeftDown is not None:
                    neighborNodeList.append(path.edgeNode.neighborLeftDown)

            # for neighborAdd in path.edgeNode.neighborNode:
            for neighborAdd in neighborNodeList:
                neighbor = gridMap[neighborAdd[1]][neighborAdd[0]]
                # print("neighbor ", neighbor.address, "/pos ", neighbor.posx, neighbor.posy)
                if neighbor.address == goal.address:
                    newPath = updatePath(path, neighbor)
                    goalPath.append(newPath)
                    print("Find path! cost is ", newPath.routeCost, ":  step is ", len(newPath.route))
                    lastFindCount = stepCount
                    if len(goalPath) >= requiredGoalPathNum:
                        return goalPath
                    else:
                        continue

                elif neighbor.isObstacle :
                    continue
                elif path.isPassedPoint(neighbor):
                    continue
                elif neighbor.isStepped:
                    newCost = path.routeCost + neighbor.cost
                    if newCost < neighbor.minStepCost:
                        newPath = updatePath(path, neighbor)
                        addX = neighbor.address[0]
                        addY = neighbor.address[1]
                        gridMap[addY][addX].minStepCost = newCost
                        gridMap[addY][addX].minPathFromStart = newPath
                        currentPath.append(newPath)
                    else :
                        continue
                else :
                    newCost = path.routeCost + neighbor.cost
                    newPath = updatePath(path, neighbor)
                    addX = neighbor.address[0]
                    addY = neighbor.address[1]
                    # print(addX, addY, len(gridMap[0]), len(gridMap))
                    gridMap[addY][addX].minStepCost = newCost
                    gridMap[addY][addX].minPathFromStart = newPath
                    gridMap[addY][addX].isStepped = True
                    currentPath.append(newPath)

    return goalPath


def run(startPoint, goalPoint):
    mapInfoList = []
    obstacles = []
    shelf = []

    # # pickleファイルからオブジェクトを復元
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    print("mapSize...", xlim,ylim)

    # 別であらかじめ作成しておいたgridMapをロード
    # makeOccupancyGrid.py
    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)

    cellSize = gridMap[0][1].posx - gridMap[0][0].posx
    print("cellSize", cellSize)
    startNode = searchNodeFromPoint(gridMap, startPoint, cellSize)
    gridMap[startNode[1]][startNode[0]].isStepped = True
    goalNode = searchNodeFromPoint(gridMap, goalPoint, cellSize)
    path = pathPlanning(gridMap, startNode, goalNode)
    if len(path) == 0:
        print("Could not find path to ", goalPoint, " from ", startPoint, ". \nSo exit this program")
        sys.exit()

    # パスをコストの低いものから順にソート
    path = sorted(path, key=attrgetter('routeCost'))

    print("Find path, cost is ", path[0].routeCost)
    # for node in path[0].route:
        # print(node.posx, node.posy, "\n")


    with open(os.path.join(DATA_PATH, "goal_path.txt"), 'w') as f:
        totalCost = 0.
        for node in path[0].route:
            totalCost += node.cost
            f.write("("+str(node.address[0])+","+str(node.address[1])+")  "+str(node.isObstacle)+"  ("+str(node.posx)+","+str(node.posy)+") "+str(totalCost)+"\n")

    # 表示用pltの設定
    fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
    ax1.set_xlim(xlim[0]-10,xlim[1]+10)
    ax1.set_ylim(ylim[0]-10,ylim[1]+10)
    print("xlim", xlim[0],"  ",xlim[1])
    print("ylim", ylim[0],"  ",ylim[1])

    # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
    # 単純にpathの探索をするだけなら必要ない
    obstacleList = []
    obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
    obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
    print("obstacles...",len(obstacleList))

    for (x1, x2, y1, y2) in obstacleList:
        plt.plot([x1,x2],[y1,y1] , color = '#000000')
        plt.plot([x2,x2],[y1,y2] , color = '#000000')
        plt.plot([x2,x1],[y2,y2] , color = '#000000')
        plt.plot([x1,x1],[y2,y1] , color = '#000000')

    # コストの低いpath上位5ルートまでをマップに表示
    colorList = ['#a1a900','#e4007f', '#2ca9e1', '#07d34f', '#c92b29']
    for j in range(0, len(path) if len(path) < 5 else 5, -1):
        for (i,node) in enumerate(path[j].route):
            if i == 0:
                print("Red path is ", path[j].routeCost, " in ", len(path[j].route) -1, " step\n")
                prevNode = node
                continue
            else:
                plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = colorList[j])
                prevNode = node

    # for (i,node) in enumerate(path[1].route):
    #     if i == 0:
    #         print("Blue path is ", path[1].routeCost, " in ", len(path[1].route), " step\n")
    #         prevNode = node
    #         continue
    #     else:
    #         plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#2ca9e1')
    #         prevNode = node
    #
    # for (i,node) in enumerate(path[2].route):
    #     if i == 0:
    #         print("Green path is ", path[2].routeCost, " in ", len(path[2].route), " step\n")
    #         prevNode = node
    #         continue
    #     else:
    #         plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#07d34f')
    #         prevNode = node

    plt.show()


if __name__ == '__main__':
    startPoint = (43.3,46.6)
    goalPoint = (85.,30.)
    # cellSize = 0.5

    run(startPoint, goalPoint)
