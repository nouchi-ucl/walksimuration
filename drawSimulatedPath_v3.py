#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pickle
import itertools
import csv
import numpy as np
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import pathPlanning_v2
import searchShelf
import fileIO
import configDef

conf = configDef.Config()
DATA_PATH = conf.dataPath
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

# MIN MAX共に-1指定で全体を描画します
DRAW_THRESHOLD_MIN = -1
DRAW_THRESHOLD_MAX = -1

PICK_TIME = 0
DRAW_SIM_PATH = False
DRAW_OPT_PATH = True
DRAW_TURN_POINT = False
DRAW_WMS = True if conf.confType == "ipin" else False
DRAW_OUTR_WALL = False

def run(agentNo, drawFlag, dataPath):
    # 各pickleファイルの読み込み
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0]
    ylim = mapInfoList[1]
    startPos = (43.3,46.6)


    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
    # print(gridMap)
    # cellSize = gridMap[0][1].posx - gridMap[0][0].posx
    cellSize = gridMap.cellSize


    eventList = fileIO.loadPassList(os.path.join(dataPath,"wms/pathList"+str(agentNo)+".csv"))
    eventList.sort(key=lambda x:x[0])
    print("length of eventList...", len(eventList))

    recordPath = os.path.join(dataPath, "record/")
    fname = os.path.join(recordPath, "record_"+str(agentNo)+".pickle")
    with open(fname, 'rb') as f:
        records = pickle.load(f)

    records = records[5:]

    fname = os.path.join(DATA_PATH,"occupancyPath/pathFileList_"+ str(agentNo)+ ".txt")
    pathList = []
    with open(fname, 'r') as f:
        data = f.readlines()
        pathList = [line.split("\n")[0] for line in data]

# pickleDumpArray[agentNo].append([step, sim.getGlobalTime(), agentsPos[agentNo], agentsDestPos[agentNo], agentsDestAttribute[agentNo]])
    # with open(os.path.join(DATA_PATH, 'simulation/'+ str(agentNo) + '_simulatedTrajectory.pickle'), mode = 'rb') as f:
    #     agentTrajectory = pickle.load(f)

    # result = []
    # pathLength = 0.
    # prevStep = None
    # prevTime = 0.
    # prevPos = None
    # prevState = None
    # prevPick = "start"
    # prevRecord = None
    # for record in records:
    #     # print(record)
    #     if prevPos is None:
    #         prevPos = np.array(record[2])
    #         prevStep = record[0]
    #         prevTime = record[1]
    #         currentState = None
    #         # prevWmsTime = 0
    #         prevRecord = record
    #         continue
    #     currentPos = np.array(record[2])
    #     distance = np.linalg.norm(currentPos - prevPos)
    #     pathLength += distance
    #     # print(currentPos, prevPos, distance)
    #     # print(record[-1])
    #     prevState = currentState
    #     currentState = record[-1][0]
    #     # print(currentState)
    #
    #     if prevState == 'Pick' and currentState == 'Turn':
    #     # if record[-1][0] == 'Pick':
    #         print("pick", prevRecord[-1][1])
    #         step = prevRecord[0]
    #         time = prevRecord[1]
    #         diffStep = step - prevStep
    #         diffTime = time - prevTime
    #         pick = prevRecord[-1][1]
    #
    #         pathLength = 0.
    #         prevTime = time
    #         prevStep = step
    #         prevPick = pick
    #     prevRecord = record
    #     prevPos = currentPos
    #
    # with open(os.path.join(DATA_PATH, "simulateResult_"+str(agentNo)+".txt"), "w") as f:
    #     f.write("Terminal ID:"+str(agentNo)+",pickTime:"+str(PICK_TIME)+"\n")
    #     f.write("prevPick, pick, diffStep, diffTime, moveTime, pathLength, length/time, time/length, time, diffTimeFromStart\n")
    #     for line in result:
    #         # print(line)
    #         f.write(",".join(map(str, line))+"\n")

    # drawFlag = False
    if drawFlag:
        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=((xlim[1] -xlim[0] +20)/10,(ylim[1] -ylim[0] +20)/10))
        # ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        # ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        ax1.set_xlim(xlim[0]-3,xlim[1]+3)
        ax1.set_ylim(ylim[0]-3,ylim[1]+3)
        # ax1.set_xlim(25,80)
        # ax1.set_ylim(0, 60)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])

        # # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
        # # 単純にpathの探索をするだけなら必要ない
        obstacleList = []
        obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
        obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
        if DRAW_OUTR_WALL:
            obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
        print("obstacles...",len(obstacleList))

        for (x1, x2, y1, y2) in obstacleList:
            ax1.add_patch(plt.Rectangle((x1,y1),x2-x1,y2-y1, fc ='#000000', fill = True))

        turnPointX = []
        turnPointY = []
        pickPointX = []
        pickPointY = []
        passedPointX = []
        passedPointY = []


        # 各パスの描画と方向転換点，ピック点を描画
        if DRAW_THRESHOLD_MIN == -1:
            drawMin = 0
        else:
            drawMin = DRAW_THRESHOLD_MIN
        if DRAW_THRESHOLD_MAX == -1:
            drawMax = len(pathList)
        else:
            drawMax = DRAW_THRESHOLD_MAX +1


        wmsList = []
        prevShelfID = -1
        if DRAW_WMS:
            with open(os.path.join(dataPath,"wms/WMS_comb_all.csv"),"r") as f:
                wmsAllData = csv.reader(f)
                header = next(wmsAllData)
                for line in wmsAllData:
                    if int(line[0]) == agentNo and prevShelfID != int(line[1]):
                        uTime = float(line[2])
                        shelfID = line[1]
                        shelfPos = searchShelf.run(shelfID, dataPath)
                        wmsList.append([uTime, shelfPos[0], shelfPos[1],"pass"])
                        prevShelfID = int(shelfID)
            wmsPickPosx = [line[1] for line in wmsList[drawMin: drawMax]]
            wmsPickPosy = [line[2] for line in wmsList[drawMin: drawMax]]

        for fName in pathList[drawMin:drawMax]:
            pickleName = fName[:-3]+"pickle"
            pickleName = os.path.join(DATA_PATH, "path/" +pickleName.split("/")[-1])
            with open(pickleName, mode='rb') as f:
                path = pickle.load(f)

            prevDirection = None
            for (i,address) in enumerate(path):
                node = gridMap.gridmap[address[0][1]][address[0][0]]
                if i == 0:
                    # print("Red path is ", path.routeCost, " in ", len(path) -1, " step\n")
                    prevNode = node
                    prevDirection = address[-1]
                    # pickPointX.append(node.posx)
                    # pickPointY.append(node.posy)
                    continue
                if i == len(path) -1:
                    if len(fName[:-3].split(",")[-1].split(" ")) > 1:
                        passedPointX.append(node.posx)
                        passedPointY.append(node.posy)
                    else:
                        pickPointX.append(node.posx)
                        pickPointY.append(node.posy)

                else:
                    if DRAW_OPT_PATH:
                        plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#07d34f')
                    prevNode = node
                    # print(address)
                    if address[-1] != prevDirection:
                        turnPointX.append(node.posx)
                        turnPointY.append(node.posy)
                    prevDirection = address[-1]
        # if startPos is not None:
            # pt = ax1.scatter(startPos[0], startPos[1], color = '#2ca9e1')


        if DRAW_TURN_POINT:
            pt1 = ax1.scatter(turnPointX,turnPointY,color = '#07d34f',s = 20)
        pt2 = ax1.scatter(passedPointX,passedPointY,color = '#A15DC4', s = 30)
        # pt3 = ax1.scatter(pickPointX,pickPointY,color = '#c92b29', s = 30)
        if DRAW_WMS:
            pt3 = ax1.scatter(wmsPickPosx,wmsPickPosy,color = '#c92b29', s = 30)

        stepThresholdMin = 0
        stepThresholdMax = 0
        # for i in range(DRAW_THRESHOLD_MIN):
        #     # print(i, DRAW_THRESHOLD_MIN)
        #     stepThresholdMin += result[i][2]
        # for i in range(DRAW_THRESHOLD_MAX):
        #     # print(i, DRAW_THRESHOLD_MAX, len(result))
        #     # print(result[i])
        #     stepThresholdMax += result[i][2]
        # print(stepThresholdMin, stepThresholdMax, agentTrajectory[0], agentTrajectory[0][2][0])

# pickleDumpArray[agentNo].append([step, sim.getGlobalTime(), agentsPos[agentNo], agentsDestPos[agentNo], agentsDestAttribute[agentNo]])

        # startDest = 0
        # endDest = "767"
        # # startDest = eventList[DRAW_THRESHOLD_MIN][1]
        # # endDest = eventList[DRAW_THRESHOLD_MAX][1]
        # startFlag = True
        # endFlag = False
        #
        # startStep = 0
        # endStep = len(agentTrajectory) - 1
        #
        # for record in agentTrajectory:
        #     if startFlag and record[-1][1] == startDest:
        #         startFlag = False
        #         startStep = record[0]
        #     if not endFlag and record[-1][1] == endDest:
        #         endFlag = True
        #     if endFlag and not record[-1][1] == endDest:
        #         endStep = record[0]
        #         break
        # print(startDest, endDest)
        # print(pathList[DRAW_THRESHOLD_MIN])
        # print(pathList[DRAW_THRESHOLD_MAX])
        # print(startStep,endStep,len(agentTrajectory))


        # agentPosx = [pos[2][0] for pos in agentTrajectory[startStep :endStep]]
        # agentPosy = [pos[2][1] for pos in agentTrajectory[startStep :endStep]]
        # timeCount = [float(pos[1]) for pos in agentTrajectory[startStep :endStep]]

        # agentPosx = [pos[2][0] for pos in agentTrajectory]
        # agentPosy = [pos[2][1] for pos in agentTrajectory]
        # timeCount = [float(pos[1]) for pos in agentTrajectory]
        #
        # prevPosx = agentPosx[0]
        # prevPosy = agentPosy[0]
        # for i in range(1, len(agentPosx)):
        #     posx = agentPosx[i]
        #     posy = agentPosy[i]
        #     # print(posx, posy)
        #     if DRAW_SIM_PATH:
        #         plt.plot([posx, prevPosx], [posy, prevPosy], color = '#2ca9e1')
        #     prevPosx = posx
        #     prevPosy = posy


        # 各パスの描画と方向転換点，ピック点を描画
        if DRAW_THRESHOLD_MIN == -1:
            recordMin = 0
        else:
            recordMin = DRAW_THRESHOLD_MIN -6
        if DRAW_THRESHOLD_MAX == -1:
            recordMax = len(pathList)
        else:
            # 0からカウントが行くので実際にはDRAW_THRESHOLD_MAXで指定した数+1まで表示される
            recordMax = DRAW_THRESHOLD_MAX -6

        passCount = 0
        turnCount = 0
        minPassCount = 0
        maxPassCount = 0
        drawMin = 0
        drawMax = len(records)
        recordPickPoint = []
        for (i,record) in enumerate(records):
            if record.pickState =="pass":
                recordPickPoint.append(record.centerPos)
                # print("pass now")
                passCount += 1
                if passCount == recordMin:
                    drawMin = i
                    minPassCount = passCount
                if passCount == recordMax:
                    drawMax = i
                    maxPassCount = passCount
            elif record.pickState == "turn":
                # print("turn now")
                turnCount += 1

        # for record in records:
        # ipin 00-10: 0/-51
        # ipin 10-20: -51/0
        for record in records[drawMin:drawMax]:
            if DRAW_SIM_PATH:
                plt.plot([record.centerPos[0], record.prevCenterPos[0]], [record.centerPos[1], record.prevCenterPos[1]], color = '#2ca9e1')
        print("pass:",passCount)
        print("turn:",turnCount)
        if DRAW_WMS:
            for (recordPos, posx, posy)in zip(recordPickPoint[recordMin:recordMax], wmsPickPosx,wmsPickPosy):
            # for (recordPos, posx, posy)in zip(recordPickPoint[recordMin:recordMax], pickPointX,pickPointY):
                print("record pos", recordPos, posx, posy)
            # for recordPos in recordPickPoint[recordMin:recordMax+5]:
            #     print("record pos", recordPos)
            # for (posx, posy) in zip(pickPointX,pickPointY):
            #     print("record pos pick", posx, posy)
            # print("final record", recordPickPoint[recordMax], wmsPickPosx[-1], wmsPickPosy[-1])
            print(len(wmsList),len(wmsPickPosx), len(recordPickPoint), len(recordPickPoint[recordMin:recordMax]))
        print("passCount,",minPassCount, maxPassCount)

        # pickX = [-14.97,-19.27,-21.97,-21.97,-17.17,-17.17]
        # pickY = [21.1,21.1,21.1,61.8,61.8,76.6]
        pickX = [-19.27,-21.97,-21.97,-17.17,-17.17]
        pickY = [21.1,21.1,61.8,61.8,76.6]
        plt.scatter(pickX, pickY, color = '#c92b29', s = 30)
        plt.savefig("/Users/nouchi/Desktop/figure1.png")
        plt.show()

if __name__ == "__main__":
    agentNo = 0
    drawFlag = True
    run(agentNo, drawFlag, DATA_PATH)
