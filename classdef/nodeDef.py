#!/usr/bin/python
# -*- coding: utf-8 -*-

class Node():

    def __init__(self,x,y,addX,addY, mapSize, cellSize):
        cellSizeHalf = cellSize /2
        self.posx = x
        self.posy = y
        self.address = (addX,addY)
        # self.neighborNode = []
        self.cost = 1.0
        self.isObstacle = False
        self.isShelf = False
        self.isStepped = False
        self.minStepCost = 0.
        self.minPathFromStart = None
        self.minDistObstacle = 1000

        self.neighborAccessFlag = [False,False,False, False,False,False, False,False,False, False]

        self.uc = (self.posx, self.posy + cellSizeHalf)
        self.dc = (self.posx, self.posy - cellSizeHalf)
        self.rc = (self.posx + cellSizeHalf, self.posy)
        self.lc = (self.posx - cellSizeHalf, self.posy)
        self.ru = (self.posx + cellSizeHalf, self.posy + cellSizeHalf)
        self.rd = (self.posx + cellSizeHalf, self.posy - cellSizeHalf)
        self.lu = (self.posx - cellSizeHalf, self.posy + cellSizeHalf)
        self.ld = (self.posx - cellSizeHalf, self.posy - cellSizeHalf)


        self.neighborUp = None
        self.neighborDown = None
        self.neighborRight = None
        self.neighborLeft = None

        self.neighborRightUp = None
        self.neighborRightDown = None
        self.neighborLeftUp = None
        self.neighborLeftDown = None

        self.neighborUp = (self.address[0], self.address[1] + 1)
        self.neighborDown = (self.address[0], self.address[1] - 1)
        self.neighborRight = (self.address[0] + 1, self.address[1])
        self.neighborLeft = (self.address[0] - 1, self.address[1])

        self.neighborRightUp = (self.address[0] + 1, self.address[1] + 1)
        self.neighborRightDown = (self.address[0] + 1, self.address[1] - 1)
        self.neighborLeftUp = (self.address[0] - 1, self.address[1] + 1)
        self.neighborLeftDown = (self.address[0] - 1, self.address[1] - 1)

        # if self.neighborUp[0] < 0 or self.neighborUp[1] < 0:
        #     self.neighborUp = None
        # if self.neighborDown[0] < 0 or self.neighborDown[1] < 0:
        #     self.neighborDown = None
        # if self.neighborRight[0] < 0 or self.neighborRight[1] < 0:
        #     self.neighborRight = None
        # if self.neighborLeft[0] < 0 or self.neighborLeft[1] < 0:
        #     self.neighborLeft = None
        # if self.neighborRightUp[0] < 0 or self.neighborRightUp[1] < 0:
        #     self.neighborRightUp = None
        # if self.neighborRightDown[0] < 0 or self.neighborRightDown[1] < 0:
        #     self.neighborRightDown = None
        # if self.neighborLeftUp[0] < 0 or self.neighborLeftUp[1] < 0:
        #     self.neighborLeftUp = None
        # if self.neighborLeftDown[0] < 0 or self.neighborLeftDown[1] < 0:
        #     self.neighborLeftDown = None
