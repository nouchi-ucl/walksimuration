#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import datetime

class SensorData():
    def __init__(self,uTime,uTimeMicro,angularVelocity,acceleration,magnetism,gryoTemperature,temperature,pressure,deltaT):
        self.uTime = uTime
        self.uTimeMiri = uTimeMicro
        self.time = datetime.datetime.fromtimestamp(uTime) + datetime.timedelta(microseconds=uTimeMicro)
        self.angularVelocityX = angularVelocity[0]
        self.angularVelocityY = angularVelocity[1]
        self.angularVelocityZ = angularVelocity[2]
        self.accelerationX = acceleration[0]
        self.accelerationY = acceleration[1]
        self.accelerationZ = acceleration[2]
        self.magnetismX = magnetism[0]
        self.magnetismY = magnetism[1]
        self.magnetismZ = magnetism[2]
        self.gTemperatureX = gryoTemperature[0]
        self.gTemperatureY = gryoTemperature[1]
        self.gTemperatureZ = gryoTemperature[2]
        self.temperature = temperature
        self.pressure = pressure
        self.deltaT = deltaT
