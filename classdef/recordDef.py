import numpy as np

from classdef import compassModelDef

class record(object):
    def __init__(self, agent, step, currentTime, prevTime):
        self.centerPos = agent.centerPos
        self.rightFootPos = agent.rightFootPos
        self.leftFootPos = agent.leftFootPos

        self.direction = agent.direction
        self.velocity = agent.velocity
        self.currentTime = currentTime

        self.prevCenterPos = agent.prevCenterPos
        self.prevRightFootPos = agent.prevRightFootPos
        self.prevLeftFootPos = agent.prevLeftFootPos

        self.prevDirection = agent.prevDirection
        self.prevVelocity = agent.prevVelocity
        self.prevTime = prevTime

        self.diffCenterPos = np.array(self.centerPos) - np.array(self.prevCenterPos)
        self.diffRightFootPos = np.array(self.rightFootPos) - np.array(self.prevRightFootPos)
        self.diffLeftFootPos = np.array(self.leftFootPos) - np.array(self.prevLeftFootPos)

        self.diffDirection = self.direction - self.prevDirection
        # diffDirectionを-pi~piの範囲に収めるための処理
        if self.diffDirection < -np.pi:
            self.diffDirection += 2*np.pi
        if np.pi < self.diffDirection:
            self.diffDirection -= 2*np.pi

        self.diffVelocity = np.array(self.velocity) - np.array(self.prevVelocity)
        self.diffTime = self.currentTime - self.prevTime

        self.step = step
        self.destPos = agent.destPos
        # 前step->今で移動した足
        self.movedFoot = "right" if agent.nextStepSide == "left" else "left"
        # 今->次stepで移動する足
        self.nextStepSide = agent.nextStepSide

        # 回転移動中かどうか "straight" > "start" > "turn" > "end" > "straight"の順に遷移
        self.turnState = agent.turnState
        # ピック作業を行っているかどうかの状態保存
        self.pickState = agent.pickState
        # # 一つ前の状態
        # self.prevTurnState = "straight"

    # txt出力用に整形
    def output4txt(self):
        outTxt0 = "step: %04i dest(%.3f,%.3f) %s is moving " % (self.step, self.destPos[0], self.destPos[1], self.movedFoot)
        outTxt1 = "current:(%.3f,%.3f)(%.3f,%.3f)(%.3f,%.3f), %.3f,(%.3f,%.3f), %.3f\n"%(self.centerPos[0], self.centerPos[1], self.rightFootPos[0], self.rightFootPos[1], self.leftFootPos[0], self.leftFootPos[1],self.direction, self.velocity[0], self.velocity[1], self.currentTime)
        # outTxt = ("step", self.step, "dest ", self.destPos, " move", self.movedFoot,
        #          "    pos:(", self.centerPos, self.rightFootPos, self.leftFootPos, ") direction", self.direction, "vel", self.velocity, "time", self.currentTime,
        #          "prevpos:(", self.prevCenterPos, self.prevRightFootPos, self.prevLeftFootPos, ") direction", self.prevDirection, "vel", self.prevVelocity, "time", self.prevTime,
        #          "diff   :(", self.diffCenterPos, self.diffRightFootPos, self.diffLeftFootPos, ") direction", self.diffDirection, "vel", self.diffVelocity, "time", self.diffTime)

        return outTxt0+outTxt1
