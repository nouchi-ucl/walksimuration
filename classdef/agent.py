import simpy
import numpy as np

# class agentの定義
class agent(object):
    def __init__(self, env, direction, velocity, position):
        # シミュレーション環境への参照
        self.env = env

        # agentの速度に関係するパラメータ
        # 現在向いている向き float -pi ~ pi
        self.direction = direction
        # 現在の時速(絶対値) float 0 ~
        self.velocity = velocity
        self.vAdapted = np.array([0.,0.])
        # agentの移動速度の上限(スカラ値)
        self.vMax = 0.
        # agentの移動に最適な速度(絶対値)
        self.vPrefAbs = 0.
        # vPrefAbs * direction
        self.vPref = np.array(self.vPrefAbs * self.direction)
        # 回避行動をした上で速度の絶対値が最大になるような速度
        self.vOpt = np.array([0.,0.])

        # その他のパラメータ
        # 現在位置 nparray(float[x,y]) の行列
        self.position = position
        # 直前の位置 nparray(float[x,y]) の行列
        self.prevLocation = np.array([0,0])
        # グループに属する場合に追従するagent
        self.follow = None
        # グループのリーダー or グループに属していない場合にTrue
        self.isLeader = False

        self.effort = 0.0


    def updateFollower(self, group):
        # member(list[agent]) の中から追従するのに最適なagentを決定する
        # この関数の外で定義されているべき変数 : esa, vMax
        # followすべきagentがいればTrue いなければFalseを返す
        distMin = 99999.
        energy = group.effort
        follow = None

        for agent in group.member:
            distPaEsa = np.linalg.norm(self.position - esa.position)
            distPbEsa = np.linalg.norm(agent.position - esa.position)
            if self == agent or distPaEsa <= distPbEsa:
                continue
            else :
                distPbPa = np.linalg.norm(agent.position - self.position)
                fab = np.linalg.norm(np.cross((agent.position - self.position), (esa.position - self.position))) * group.effort
                if fab > 0 and distPbPa < distMin and np.dot(self.vAdapted, self.vPref) > 0:
                    vAdapted = (self.position - agent.position) / distPbPa * vMax
                    distMin = distPbPa
                    follow = agent

        if follow is None:
            self.isLeader = True
            return False
        else:
            self.vAdapted = vAdapted
            self.follow = follow
            self.isLeader = False
            return True

    def computeEffort(self, group):
        # targetはagentが回避するべきgroup
        va = np.array(self.velocity * [np.cos(self.direction), np.sin(self.direction)])
        vg = np.array(group.velocity * [np.cos(group.direction), np.sin(group.direction)])
        effort = np.cross((va - vg),(self.position - group.position))
        self.effort = np.linalg.norm(effort)

    def isSameGroup(self, agent):
        #  targetは対象のagent
        # selfとagentが同じグループに属する条件を満たしていればtrueを、そうでないならfalseを返す
        distPos = np.linalg.norm(agent.position - self.position)
        vself = np.array(self.velocity * [np.cos(self.direction), np.sin(self.direction)])
        vagent = np.array(agent.velocity * [np.cos(agent.direction), np.sin(agent.direction)])
        distVel = np.linalg.norm(vagent - vself)

        if distPos < pThreshold and distVel < vThreshold :
            return True
        else :
            return False

    def findErEl(group):
        # 対象のグループの自身から見て一番外側にいるagentを左右それぞれ１人探します
        # (グループの中心を正面に見て時計回り方向(-θ)を右(Er)反時計回り方向(+θ)を左(El))
        vC = group.position - self.position
        angleCenter = np.arctan2(vC[0],vC[1])
        rightMax = 0
        leftMax = 0
        eR = None
        eL = None

        for member in group.member:
            vT = member.position - self.position
            angleTarget = np.arctan2(vT[0],vT[1])
            angle = angleCenter - angleTarget
            if angle <= rightMax:
                rightMax = angle
                eR = member
            if angle >= leftMax:
                leftMax = angle
                eL = member
        return eR, eL
