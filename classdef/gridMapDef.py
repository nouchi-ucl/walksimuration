import itertools
import sys
import matplotlib.pyplot as plt

from classdef import nodeDef_v2
import configDef
import pathPlanning_v4


conf = configDef.Config()

def inRectangle(point,x1,x2,y1,y2):
    x = point[0]
    y = point[1]
    if x1 <= x and x <= x2 and y1 <= y and y <= y2:
        return True
    else:
        return False

class GridMap(object):

    def __init__(self, xlim, ylim, cellSize):
        # マップの初期化(マップを格子状に切って各セルの初期化まで)
        # マップの中心を基準にしている(centerCell)
        xMin = xlim[0]
        xMax = xlim[1]
        yMin = ylim[0]
        yMax = ylim[1]

        # 外周の壁もセルとしておきたいので外に1マスずつ拡張している
        sizeX = (round(xMin/cellSize) -1, round(xMax/cellSize) +1)
        sizeY = (round(yMin/cellSize) -1, round(yMax/cellSize) +1)
        # centerCell = (round((startx - xMin)/cellSize)+2, round((starty - yMin)/cellSize)+2)
        mapSize = (sizeX[1] - sizeX[0] +1, sizeY[1] - sizeY[0] +1)
        print("mapGridSize ", mapSize)
        print("sizeX",sizeX, "  sizeY",sizeY)
        # print([[(cellSize * x + startx,cellSize * y + starty, (x - sizeX[0], y - sizeY[0])) for x in range(sizeX[0] ,sizeX[0] +2)] for y in range(sizeY[0], sizeY[0] +2)])
        # print("\n\n\n")
        # print([[Node(cellSize * x + startx, cellSize * y + starty, x - sizeX[0], y - sizeY[0], mapSize).address for x in range(sizeX[0],sizeX[0] +2)] for y in range(sizeY[0], sizeY[0] +2)])

        # 各セルに初期化したノードインスタンスを置いていく
        gridMap = [[nodeDef_v2.NodeV2(cellSize * x, cellSize * y, x - sizeX[0], y - sizeY[0], mapSize, cellSize) for x in range(sizeX[0] ,sizeX[1] +1)] for y in range(sizeY[0], sizeY[1] +1)]

        # return gridMap
        self.gridmap = gridMap
        self.cellSize = cellSize
        self.xMin = xMin
        self.xMax = xMax
        self.yMin = yMin
        self.yMax = yMax
        # self.startx  = startx
        # self.starty = starty
        # self.centerCell = centerCell
        self.mapSize = mapSize
        self.obstacleList = []
        self.shelfList = []
        self.bleList = []

    def addBLE(self, bleList):
        self.bleList = bleList

    def addCost(self):
        gridMap = self.gridmap
        cellSize = self.cellSize

        xMax = self.mapSize[0]
        yMax = self.mapSize[1]
        # print("addObstacle",xMax, yMax)
        for i, j in itertools.product(range(yMax), range(xMax)):
            node = gridMap[i][j]

            if i == 0:
                node.neighborDown = None
                node.neighborRightDown = None
                node.neighborLeftDown = None
            if j == 0:
                node.neighborLeft = None
                node.neighborLeftUp = None
                node.neighborLeftDown = None
            if i == yMax -1:
                node.neighborUp = None
                node.neighborRightUp = None
                node.neighborLeftUp = None
            if j == xMax -1:
                node.neighborRight = None
                node.neighborRightUp = None
                node.neighborRightDown = None

            if node.isObstacle or node.isShelf:
                if node.isObstacle:
                    obstacleFlag = True
                else:
                    obstacleFlag = False
                if i != 0:
                    if obstacleFlag:
                        gridMap[i-1][j].neighborUp = None
                    if gridMap[i-1][j].cost != conf.obstacleCost:
                        gridMap[i-1][j].cost = conf.neighborObstacleCost

                    if j != 0:
                        if obstacleFlag:
                            gridMap[i-1][j-1].neighborRightUp = None
                        if gridMap[i-1][j-1].cost != conf.obstacleCost:
                            gridMap[i-1][j-1].cost = conf.neighborObstacleCost

                if j != 0:
                    if obstacleFlag:
                        gridMap[i][j-1].neighborRight = None
                    if gridMap[i][j-1].cost != conf.obstacleCost:
                        gridMap[i][j-1].cost = conf.neighborObstacleCost

                    if i != yMax-1:
                        if obstacleFlag:
                            gridMap[i+1][j-1].neighborRightDown = None
                        if gridMap[i+1][j-1].cost != conf.obstacleCost:
                            gridMap[i+1][j-1].cost = conf.neighborObstacleCost

                if i != yMax-1:
                    if obstacleFlag:
                        gridMap[i+1][j].neighborDown = None
                    if gridMap[i+1][j].cost != conf.obstacleCost:
                        gridMap[i+1][j].cost = conf.neighborObstacleCost

                    if j != xMax-1:
                        if obstacleFlag:
                            gridMap[i+1][j+1].neighborLeftDown = None
                        if gridMap[i+1][j+1].cost != conf.obstacleCost:
                            gridMap[i+1][j+1].cost = conf.neighborObstacleCost

                if j != xMax-1:
                    if obstacleFlag:
                        gridMap[i][j+1].neighborLeft = None
                    if gridMap[i][j+1].cost != conf.obstacleCost:
                        gridMap[i][j+1].cost = conf.neighborObstacleCost

                    if i != 0:
                        if obstacleFlag:
                            gridMap[i-1][j+1].neighborLeftUp = None
                        if gridMap[i-1][j+1].cost != conf.obstacleCost:
                            gridMap[i-1][j+1].cost = conf.neighborObstacleCost

            # 対象のnodeがobstacleでない場合，周囲の各nodeへの移動が可能かどうかを判定する
            # accessFlagを用いて+2以上のnodeへは移動できないとする
            #   隣のnodeがobstacle->そのnodeに+2 obstacle nodeに隣接するnodeに+1
            #       9がobstacleなら9 (+2), 8,6 (+1)
            #   移動方向とaccessFlagの対応はPCのテンキーと同じ
            #       移動前node(現在地):5 上方向8 から時計回りに 9 6 3 2 1 4 7
            if not node.isObstacle:
                accessFlag = [0,0,0, 0,0,0, 0,0,0, 0]

                if node.neighborAccessFlag[1]:
                    accessFlag[1] += 2
                    accessFlag[2] += 1
                    accessFlag[4] += 1
                if node.neighborAccessFlag[2]:
                    accessFlag[1] += 1
                    accessFlag[2] += 2
                    accessFlag[3] += 1
                if node.neighborAccessFlag[3]:
                    accessFlag[2] += 1
                    accessFlag[3] += 2
                    accessFlag[6] += 1
                if node.neighborAccessFlag[4]:
                    accessFlag[1] += 1
                    accessFlag[4] += 2
                    accessFlag[7] += 1
                if node.neighborAccessFlag[6]:
                    accessFlag[3] += 1
                    accessFlag[6] += 2
                    accessFlag[9] += 1
                if node.neighborAccessFlag[7]:
                    accessFlag[4] += 1
                    accessFlag[7] += 2
                    accessFlag[8] += 1
                if node.neighborAccessFlag[8]:
                    accessFlag[7] += 1
                    accessFlag[8] += 2
                    accessFlag[9] += 1
                if node.neighborAccessFlag[9]:
                    accessFlag[6] += 1
                    accessFlag[8] += 1
                    accessFlag[9] += 2

                if accessFlag[1] >=2:
                    node.neighborLeftDown = None
                if accessFlag[2] >=2:
                    node.neighborDown = None
                if accessFlag[3] >=2:
                    node.neighborRightDown = None
                if accessFlag[4] >=2:
                    node.neighborLeft = None
                if accessFlag[6] >=2:
                    node.neighborRight = None
                if accessFlag[7] >=2:
                    node.neighborLeftUp = None
                if accessFlag[8] >=2:
                    node.neighborUp = None
                if accessFlag[9] >=2:
                    node.neighborRightUp = None

        self.gridmap = gridMap

    def addObstacle(self, obstacleList):
        # 初期化されたマップに障害物の情報を追加していく
        # 正確には各セルについて
        #   セルの中心が障害物の内部か？
        #       True ->セル全体を障害物とする
        #       False->四隅の各点が障害物の内部かどうかで隣のセルへ移動できるかどうかを判定
        # 各セル間の移動の可否を設定していく
        # またセル自体が障害物でない場合は他の障害物との距離によってそのセルへの移動コストを設定する
        # セルの中心が障害物に近いほど高いコストを持つようにする

        self.obstacleList = obstacleList
        gridMap = self.gridmap
        cellSize = self.cellSize
        xMax = self.mapSize[0]
        yMax = self.mapSize[1]
        cellSizeHalf = cellSize/2
        testCount = 0
        # print("addObstacle",xMax, yMax)
        for i, j in itertools.product(range(yMax), range(xMax)):
            node = gridMap[i][j]

            x = node.posx
            y = node.posy
            center = (x,y)
            # minDist = node.minDistObstacle

            for (x1,x2,y1,y2) in obstacleList:
                if node.isObstacle:
                    node.minDistObstacle = 0.
                    testCount +=1
                    break
                # セルの中心が障害物の内部なら,隣接セルからこのセルへの移動を不可に
                if inRectangle(center,x1,x2,y1,y2):
                    node.cost = conf.obstacleCost
                    node.isObstacle = True
                    node.neighborAccessFlag[0] = True
                # 四隅の各点が障害物の内部にあるかどうかで移動できない隣接セルを設定
                else:
                    if inRectangle(node.uc,x1,x2,y1,y2):
                        node.neighborAccessFlag[8] = True
                    if inRectangle(node.dc,x1,x2,y1,y2):
                        node.neighborAccessFlag[2] = True
                    if inRectangle(node.rc,x1,x2,y1,y2):
                        node.neighborAccessFlag[6] = True
                    if inRectangle(node.lc,x1,x2,y1,y2):
                        node.neighborAccessFlag[4] = True
                    if inRectangle(node.ru,x1,x2,y1,y2):
                        node.neighborAccessFlag[9] = True
                    if inRectangle(node.rd,x1,x2,y1,y2):
                        node.neighborAccessFlag[3] = True
                    if inRectangle(node.lu,x1,x2,y1,y2):
                        node.neighborAccessFlag[7] = True
                    if inRectangle(node.ld,x1,x2,y1,y2):
                        node.neighborAccessFlag[1] = True

            if i % 10 == 0 and j == 0:
                print("add obstacle info to Node... ",i," / ",yMax)


        print ("obstacleCount...",testCount)
        self.gridmap = gridMap

    def addShelf(self, shelfList):
        self.shelfList = shelfList
        gridMap = self.gridmap
        cellSize = self.cellSize

        cellSizeHalf = cellSize/2
        xMax = self.mapSize[0]
        yMax = self.mapSize[1]
        # print("addObstacle",xMax, yMax)
        for i, j in itertools.product(range(yMax), range(xMax)):
            node = gridMap[i][j]

            x = node.posx
            y = node.posy
            center = (x,y)
            # minDist = node.minDistObstacle

            for (x1,x2,y1,y2) in shelfList:
                if node.isObstacle or node.isShelf:
                    break

                # セルの中心が棚の内部なら,コストを引き上げる
                if inRectangle(center,x1,x2,y1,y2):
                    node.cost = conf.obstacleCost
                    node.isShelf = True
                    # node.neighborAccessFlag[0] = True

            if i % 10 == 0 and j == 0:
                print("add shelf info to Node... ",i," / ",yMax)

        self.gridmap = gridMap

    def drawMap(self):
        print("draw grid map...\n")
        fig, (ax1) = plt.subplots(1,1,figsize=((self.xMax-self.xMin+20)/10,(self.yMax-self.yMin+20)/10))
        ax1.set_xlim(self.xMin-10,self.xMax+10)
        ax1.set_ylim(self.yMin-10,self.yMax+10)
        print("xlim", self.xMin,"  ",self.xMax)
        print("ylim", self.yMin,"  ",self.yMax)

        for (x1, x2, y1, y2) in obstacleList:
            ax1.add_patch(plt.Rectangle((x1,y1),x2-x1,y2-y1, fc ='#303030', fill = False))

        for (x1, x2, y1, y2) in shelfList:
            ax1.add_patch(plt.Rectangle((x1,y1),x2-x1,y2-y1, fc ='#303030', fill = False))

        bleX = [ b.posx for b in self.bleList]
        bleY = [ b.posy for b in self.bleList]
        print(len(self.bleList))
        pt1 = plt.scatter(bleX,bleY,color = '#2ca9e1')
        plt.savefig("/Users/nouchi/Desktop/map.png")

        plt.show()

    def nearNonObstCell(self, point):
        gridMap = self.gridmap
        cellSize = self.cellSize

        address = self.searchNodeFromPoint(point)
        targetCell = gridMap[address[1]][address[0]]
        if not targetCell.isObstacle and not targetCell.isShelf:
            return (targetCell.posx,targetCell.posy)
        else:
            address = targetCell.address
            distance = 1
            xMax = self.mapSize[0]
            yMax = self.mapSize[1]
            while(True):
                print(distance, address, xMax, yMax)
                if address[1] +distance <= yMax:
                    cell = gridMap[address[1]+distance][address[0]]
                    if not cell.isObstacle and not cell.isShelf:
                        return (cell.posx,cell.posy+0.15)
                if 0 <= address[1] - distance:
                    cell = gridMap[address[1]-distance][address[0]]
                    if not cell.isObstacle and not cell.isShelf:
                        return (cell.posx,cell.posy-0.15)
                if address[0] +distance <= xMax:
                    cell = gridMap[address[1]][address[0]+distance]
                    if not cell.isObstacle and not cell.isShelf:
                        return (cell.posx+0.15,cell.posy)
                if 0 <= address[0] -distance:
                    cell = gridMap[address[1]][address[0]-distance]
                    if not cell.isObstacle and not cell.isShelf:
                        return (cell.posx-0.15,cell.posy)
                distance += 1

    def searchNodeFromPoint(self, point):
        # x,yの座標からその点が含まれるセルのアドレスを返す point -> gridMap[address[1]][address[0]]
        # Nodeインスタンスそのものを返すわけではないので注意

        gridMap = self.gridmap
        cellSize = self.cellSize
        address = None
        for line in gridMap:
            # print(type(line[0].posy), type(point[1]))
            yDist = abs(line[0].posy - float(point[1]))
            if yDist <= cellSize:
                for node in line:
                    xDist = abs(node.posx - float(point[0]))
                    if xDist <= cellSize:
                        address = node.address

                        # hoge
                        address = (address[0] -1, address[1] -1)


                        # print(address, point)
        if address is None:
            print("Target point ", point, " is out of map region")
            print(self.xMin,self.xMax,self.yMin,self.yMax)
            sys.exit()
        print("searchNodeFrom Point:", point, cellSize, "is ", address)

        return address

    def stepNewNode(self, goal, currentNode, neighborAdd, direction, step):
        # pathの末端のセルから隣接するセル(neighborAdd)へ移動する際の計算を行う
        #   移動先がgoal　-> pathをgoalPathに追加
        #   移動先がobstacke/shelf/自身が以前通ったnode(ループが発生している) -> 移動できない(新しいpathとして追加しない)
        #   移動先が既に他のpathが通過している
        #       -> 他のpathがこのnodeを通った時のコストより大きい -> 移動しない(新しいpathとして追加しない)
        #       -> ~~のコストより小さい -> 新しいpathとして追加
        #   移動先はまだどのpathも通ったことのないnodeである -> 新しいpathとして追加
        # 返り値は移動先がgoalの場合はTrue それ以外はFalse

        gridMap = self.gridmap
        cellSize = self.cellSize

        neighbor = gridMap[neighborAdd[1]][neighborAdd[0]]

        if neighbor.step is None:
            # return updatePath(currentNode, neighbor, direction)
            return currentNode.updatePath(neighbor, direction)
            # return True, neighbor

        # 移動先がgoalの場合
        if neighbor.address == goal.address:
            # print("target node is goal node")
            return currentNode.updatePath(neighbor, direction)

        elif step > neighbor.step:
            return False, neighbor
        else:
            return currentNode.updatePath(neighbor, direction)
