#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import datetime

class BleData():
    def __init__(self,uTime,uTimeMicro,dataCount,dataList):
        self.uTime = uTime
        self.uTimeMiri = uTimeMicro
        self.time = datetime.datetime.fromtimestamp(uTime) + datetime.timedelta(microseconds=uTimeMicro)
        self.dataCount = dataCount
        self.dataList = dataList
