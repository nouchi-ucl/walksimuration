#!/usr/bin/python
# -*- coding: utf-8 -*-

import math

from classdef import nodeDef

class Path():

    def __init__(self, node):
        self.edgeNode = node
        self.prevNode = None
        self.moveDirection = None
        self.route = []
        self.route.append([node.address, False])
        self.routeCost = node.cost

    def isPassedPoint(self, target, gridMap):
        for address in self.route:
            node = gridMap[address[0][1]][address[0][0]]
            if node.address == target.address:
                return True
            else:
                return False

    def calcDirection(self, newNode, turnFlag):

        prevAdd = self.prevNode.address
        newAdd = newNode.address

        # baseVector = np.array(1,0)
        diffVector = (newAdd[0] - prevAdd[0], newAdd[1] - prevAdd[1])
        angle =  math.atan2(newAdd[1] - prevAdd[1], newAdd[0]- prevAdd[1]) *20
        if turnFlag :
            if self.moveDirection is None:
                if diffVector == (0,0):
                    return False, False, 0.
                else:
                    return True, False, angle
            elif diffVector == (0,0):
                return False, False, 0.
            elif angle == self.moveDirection:
                return True, False, angle
            else:
                return True, True, angle
        else:
            if diffVector == (0,0):
                return False, 0.
            else:
                return True, angle


        # diffX = newAdd[0] - prevAdd[0]
        # diffY = newAdd[1] - prevAdd[1]
        # if diffX > 0:
        #     if diffY > 0:
        #         return 'rightUp'
        #     elif diffY < 0:
        #         return 'rightDown'
        #     else:
        #         return 'right'
        # elif diffX < 0:
        #     if diffY > 0:
        #         return 'leftUp'
        #     elif diffY < 0:
        #         return 'leftDown'
        #     else :
        #         return 'left'
        # else:
        #     if diffY > 0:
        #         return 'up'
        #     elif diffY < 0:
        #         return 'down'
        #     else:
        #         return 'noMove'

    # def isTurn(self, newNode):
    #     if self.moveDirection is None:
    #         return False
    #     moveFlag, newDirection = self.calcDirection(newNode)
    #     if newDirection == self.moveDirection or moveFlag == False:
    #         return False
    #     else:
    #         return True
