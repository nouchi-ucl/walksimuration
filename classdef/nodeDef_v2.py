#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import configDef

conf = configDef.Config()
ROUTE_COST_THRESHOLD = conf.routeCostThreshold

# import gridMapDef

class NodeV2():

    def __init__(self,x,y,addX,addY, mapSize, cellSize):
        cellSizeHalf = cellSize /2
        self.posx = x
        self.posy = y
        self.address = (addX,addY)
        # self.neighborNode = []
        self.cost = 1.0
        self.isObstacle = False
        self.isShelf = False
        # self.isStepped = False
        # self.minStepCost = 0.
        # self.minPathFromStart = None
        self.minCost = None
        self.minCostPrevNode = None
        self.step = None
        self.prevDirection = None
        self.minDistObstacle = 1000

        self.neighborAccessFlag = [False,False,False, False,False,False, False,False,False, False]

        self.uc = (self.posx, self.posy + cellSizeHalf)
        self.dc = (self.posx, self.posy - cellSizeHalf)
        self.rc = (self.posx + cellSizeHalf, self.posy)
        self.lc = (self.posx - cellSizeHalf, self.posy)
        self.ru = (self.posx + cellSizeHalf, self.posy + cellSizeHalf)
        self.rd = (self.posx + cellSizeHalf, self.posy - cellSizeHalf)
        self.lu = (self.posx - cellSizeHalf, self.posy + cellSizeHalf)
        self.ld = (self.posx - cellSizeHalf, self.posy - cellSizeHalf)


        self.neighborUp = None
        self.neighborDown = None
        self.neighborRight = None
        self.neighborLeft = None

        self.neighborRightUp = None
        self.neighborRightDown = None
        self.neighborLeftUp = None
        self.neighborLeftDown = None

        self.neighborUp = (self.address[0], self.address[1] + 1)
        self.neighborDown = (self.address[0], self.address[1] - 1)
        self.neighborRight = (self.address[0] + 1, self.address[1])
        self.neighborLeft = (self.address[0] - 1, self.address[1])

        self.neighborRightUp = (self.address[0] + 1, self.address[1] + 1)
        self.neighborRightDown = (self.address[0] + 1, self.address[1] - 1)
        self.neighborLeftUp = (self.address[0] - 1, self.address[1] + 1)
        self.neighborLeftDown = (self.address[0] - 1, self.address[1] - 1)

    def crossObstacle(self, targetNode, gridMap, obstacleList):
        startPos = np.array((startNode.posx, startNode.posy))
        targetPos = np.array((targetNode.posx, targetNode.posy))
        targetLine = startPos - targetPos
        for (x1, x2, y1, y2) in obstacleList:
            point1 = np.array((x1,y1))
            point2 = np.array((x1,y2))
            point3 = np.array((x2,y2))
            point4 = np.array((x2,y1))
            line1 = point2 - point1
            line2 = point3 - point2
            line3 = point4 - point3
            line4 = point1 - point4

            if np.cross(targetLine, point1 - startPos) * np.cross(targetLine, point2 -startPos) <= 0:
                # print("1-true", startPos, targetPos, x1,x2,y1,y2)
                if np.cross(line1, startPos - point1) * np.cross(line1, targetPos - point1) <= 0:
                    return True
            if np.cross(targetLine, point2 - startPos) * np.cross(targetLine, point3 -startPos) <= 0:
                # print("2-true", startPos, targetPos, x1,x2,y1,y2)
                if np.cross(line2, startPos - point2) * np.cross(line2, targetPos - point2) <= 0:
                    return True
            if np.cross(targetLine, point3 - startPos) * np.cross(targetLine, point4 -startPos) <= 0:
                # print("3-true", startPos, targetPos, x1,x2,y1,y2)
                if np.cross(line3, startPos - point3) * np.cross(line3, targetPos - point3) <= 0:
                    return True
            if np.cross(targetLine, point4 - startPos) * np.cross(targetLine, point1 -startPos) <= 0:
                # print("4-true", startPos, targetPos, x1,x2,y1,y2)
                if np.cross(line4, startPos - point4) * np.cross(line4, targetPos - point4) <= 0:
                    return True

        return False

    def updatePath(self, neighbor, direction):
        # パスの更新:1マス進んだ先のノード(newNode)をpathに追加していく
        # 端部のノードの更新, routeへ追加, path全体コストの更新
        # コストは斜め方向に移動する場合は x1.4
        # 前回の移動方向に対して回転している場合はコストを追加
        updateFlag = False
        moveCost = 0.
        if direction == "up" or direction == "down" or direction == "right" or direction == "left":
            directionCost = 1.
        else :
            directionCost = 1.4
        if self.minCost is None:
            moveCost = neighbor.cost * directionCost
        else:
            moveCost = self.minCost + neighbor.cost * directionCost

        if moveCost < ROUTE_COST_THRESHOLD:
            if neighbor.minCost is None or neighbor.minCost > moveCost:
                neighbor.step = self.step + 1
                neighbor.minCost = moveCost
                neighbor.minCostPrevNode = self.address
                neighbor.prevDirection = direction
                updateFlag = True

        return updateFlag, neighbor
