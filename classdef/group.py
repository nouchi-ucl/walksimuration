import simpy
import numpy as np

# class Groupの定義
class group(object):

    def __init__(self, env, direction, velocity, leader):
        # シミュレーション環境への参照
        self.env = env

# グループ速度Vgについて 各agentの速度ではない
        # 現在向いている向き float [-pi ~ pi]
        # x軸方向正の向きを0にして反時計回りに 0~(+pi) 時計回りに0~(-pi)
        self.direction = direction
        # 現在の時速(絶対値) float [0 ~ ]
        self.velocity = velocity
        self.vAdapted = np.array([0.,0.])
        # groupの移動速度の上限(スカラ値)
        # leaderの移動速度上限にしておく(何かまずいことあるかな？)
        # リーダーの更新のところで設定
        self.vMax = 0.
        # groupの移動に最適な速度(絶対値)
        # これもリーダーに合わせるか
        self.vPrefAbs = 0.
        # vPrefAbs * direction
        self.vPref = np.array(self.vPrefAbs * self.direction)
        # 回避行動をした上で速度の絶対値が最大になるような速度
        self.vOpt = np.array([0.,0.])

        # position はグループを構成する全agentのpositionの平均値
        # 初期値はleaderの位置にしておく
        self.position = leader.position

# グループの構成
        # グループリーダー agent型
        self.leader = leader
        # グループメンバーを格納するlist[agent]
        self.menber = []
        self.menber.append(leader)

        # グループの回避方向を決める要素 float
        self.effort = 0.0

    # グループに所属する各メンバーのfollow関係の更新
    # 誰もfollowしていないagentがグループのleaderになる
    def updateLeader(self):
        leader = None
        vMax = None
        vPref = None
        for agent in self.member:
            # followすべきagentがいればTrue, いなければFalseが帰ってくる
            if not agent.updateFollower(self.member):
                # self.leader = agent
                # self.vMax = agent.vMax
                # self.vPrefAbs = agent.vPrefAbs
                leader = agent
                vMax = agent.vMax
                vPrefAbs = agnet.vPrefAbs
        return leader, vMax, vPrefAbs

    # target : 回避するべきグループ(self以外)
    def computeEffort(self, target):
        effort = 0.0
        for agent in self.menber:
            effort += agent.computeEffort(target)
        return effort


    def updataGroupPos(self):
        pos = np.array([0.,0.])
        for agent in self.member:
            pos += agent.position
        pos = pos / len(self.member)
        return pos

    def isMember(self, agent):
        result = False
        for member in self.member:
            if member == agent:
                result = True
                break
        return result
