#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
import os
# import rvo2
from classdef import recordDef
import commonCalc
import configDef
conf = configDef.Config()


class compass(object):
    def __init__(self, agentNo, direction, initVelocity, maxSpeed, position, stepTime, wayPointList, memoryFlg):

        # シミュレータ上でのagentのID
        self.agentNo = agentNo

        # agentの速度に関係するパラメータ

        # 現在の時速(絶対値) float 0 ~
        self.velocity = initVelocity
        # 一つ前のステップでの速度
        self.prevVelocity = 0
        # agentの移動速度の上限(絶対値 float)
        self.vMax = maxSpeed
        # agentの移動に最適な速度(絶対値 float)
        self.vPrefAbs = self.vMax /2
        # 現在向いている向き float -pi ~ pi
        self.direction = direction
        # 一つ前のステップでの向き
        self.prevDirection = direction
        # (float x, float y) = vPrefAbs * direction
        self.vPref = self.vPrefAbs * self.direction
        # 回避行動をした上で速度の絶対値が最大になるような速度
        self.vOpt = np.array([0.,0.])

        # その他のパラメータ
        # 現在位置 nparray(float[x,y]) の行列
        # center : 中心(腰のxy座標)
        if conf.confType == "ipin":
            self.centerPos = wayPointList[0][2]
        else:
            self.centerPos = position
        # 1step前の中心の位置
        agentRadius = conf.radius / 4
        # 右足の位置
        rightAngle = self.direction - np.pi/2
        if rightAngle < -np.pi:
            rightAngle += 2*np.pi
        self.rightFootPos = (self.centerPos[0] +agentRadius * np.cos(rightAngle), self.centerPos[1] +agentRadius * np.sin(rightAngle))

        # 左足の位置
        leftAngle = self.direction + np.pi/2
        if np.pi < leftAngle:
            leftAngle -= 2*np.pi
        self.leftFootPos = (self.centerPos[0] +agentRadius *np.cos(leftAngle), self.centerPos[1] +agentRadius * np.sin(leftAngle))

        # self.leftFootPos = self.centerPos
        # 右足の位置
        # self.rightFootPos = self.centerPos
        # 一つ前の位置も保存しておく
        self.prevCenterPos = self.centerPos
        self.prevLeftFootPos = self.leftFootPos
        self.prevRightFootPos = self.rightFootPos
        # 次に移動させる足
        self.nextStepSide = "right"

        # agentが通過するべき目標点のリスト
        #[ セル座標(x,y), 前の目標点からの直線距離, prevDestPoint(x,y), currentDestPoint(x,y) ,action(Pass or Pick)
        self.destinationList = wayPointList
        # agnetが現在目指しているdestinationListの番号
        self.destNo = 0
        # agnetが現在目指しているdestination
        self.currentDest = self.destinationList[self.destNo]
        # agentが現在目指している地点の座標
        self.destPos = [float(self.destinationList[0][3][0]), float(self.destinationList[0][3][1])]
        # agentの移動軌跡を保存するかどうかの判定
        self.memoryFlg = memoryFlg
        # agentの移動軌跡 memoryFlgがtrueならここに各ステップでの状態が保存される
        self.trajectory = []
        # agentが各目標点に到着した時の状態の保存
        self.arrivalList = []
        # agentが最終目的地点に到着している場合True
        self.arrivalFlg = False
        # agentが直進しているか曲がり移動をしているかどうか
        # straight > start > turn > end > straight の順で遷移
        self.turnState = "straight"
        # 一つ前の状態
        self.prevTurnState = "straight"

        # ピック作業を行っているかの状態保存
        self.pickState = "walk"

        # # グループに属する場合に追従するagent
        # self.follow = None
        # # グループのリーダー or グループに属していない場合にTrue
        # self.isLeader = False
        #
        # self.effort = 0.0

    # 次に踏み出す足(nextStepSide)の切り替えをするだけ
    def changeNextStep(self):
        self.nextStepSide = "right" if self.nextStepSide == "left" else "left"

    def checkTurnState(self):
        deffDirection = self.direction - self.prevDirection
        # digree
        # directionMinThreshold = conf.directionMinThreshold
        # print("\ncheckTurnState", deffDirection, 15 / (2* np.pi))
        # if abs(deffDirection) > 15 / (2* np.pi):
        print("\ncheckTurnState", self.direction*180/np.pi, self.prevDirection*180/np.pi, deffDirection*180/np.pi, conf.directionMinThreshold )
        if abs(deffDirection) *180/np.pi > conf.directionMinThreshold:
            if self.prevTurnState == "turn" or self.prevTurnState == "start":
                self.turnState = "turn"
            else:
                self.turnState = "start"
        else:
            if self.prevTurnState == "turn" or self.prevTurnState == "start":
                self.turnState = "end"
            else:
                self.turnState = "straight"

    # 現在の位置情報をprevPosにコピー
    def copy2PrevState(self):
        print("copy prevState",self.direction*180/np.pi, self.prevDirection*180/np.pi, self.prevVelocity)
        self.prevCenterPos = self.centerPos
        self.prevLeftFootPos = self.leftFootPos
        self.prevRightFootPos = self.rightFootPos

        self.prevVelocity = self.velocity
        # self.prevDirection = self.direction
        self.prevTurnState = self.turnState
        return

    def fixLatestFootPos(self, right, left):
        self.trajectory[-1].rightFootPos = right
        self.trajectory[-1].leftFootPos = left
        self.prevRightFootPos = right
        self.prevLeftFootPos = left
        return

    # 1つ前のステップから移動しているかどうかを判定
    # centerPosで判断
    def isDontMove(self):
        if self.centerPos == self.prevCenterPos:
            return True
        else:
            return False

    # # memoryFlgがTrueの場合にtrajectoryリストに現在の状態を追加
    # # step数, 実時間, 速度, 中心・左足・右足の位置, 方向, 目標地点
    def memoryCurrentStep(self, step, currentTime, prevTime):
        if self.memoryFlg:
            newRecord = recordDef.record(self, step, currentTime, prevTime)
            self.trajectory.append(newRecord)
        self.pickState = "walk"
        # if self.memoryFlg:
        #     self.trajectory.append([step, time, self.velocity, self.centerPos, self.leftFootPos, self.rightFootPos, self.direction, self.destPos])
        return

    # 他に保存しないといけない要素はある？
    def memoryArrivalStep(self, step, point):
        action = self.currentDest[-1]
        self.arrivalList.append([step, point, self.leftFootPos, self.rightFootPos, action])
        return

    # この時点で更新されていること
    # centerPos/rightFootPos/leftFootPos/velocity
    # prevCenterPos/prevRightFootPos/prevLeftFootPos/
    # まだ
    def checkNewStep(self, sim):
        # checkFlg = False

        centerPos = np.array(self.centerPos)
        prevCenterPos = np.array(self.prevCenterPos)
        rightFootPos = np.array(self.rightFootPos)
        prevRightFootPos = np.array(self.prevRightFootPos)
        leftFootPos = np.array(self.leftFootPos)
        prevLeftFootPos = np.array(self.prevLeftFootPos)

        centerVec = centerPos - prevCenterPos
        rightVec = rightFootPos - prevRightFootPos
        leftVec = leftFootPos - prevLeftFootPos

        targetVec = rightVec if self.nextStepSide== "right" else leftVec
        targetPos = rightFootPos if self.nextStepSide== "right" else leftFootPos
        prevTargetPos = prevRightFootPos if self.nextStepSide== "right" else prevLeftFootPos
        distCenterPos = np.linalg.norm(centerVec)

        direction = np.arctan2(centerVec[1], centerVec[0])


        diffDirection = direction - self.prevDirection
        print("centerPos, prevCenterPos, targetPos, prevTargetPos,\n", centerPos, prevCenterPos, targetPos, prevTargetPos)
        print("prevDirection, direction, diffDirection, simdirection\n", self.prevDirection*180/np.pi, self.direction*180/np.pi, diffDirection*180/np.pi, direction*180/np.pi)
        # diffDirectionを-pi~piの範囲に収めるための処理
        while(True):
            print("check diffDirection  ", diffDirection*180/np.pi)
            if -1*np.pi < diffDirection and diffDirection <= np.pi:
                break
            elif diffDirection <= np.pi:
                diffDirection += 2 * np.pi
            elif -1*np.pi < diffDirection:
                diffDirection -= 2 * np.pi
        # 回転角が大きすぎる場合は修正
        # IPINの時はやらない
        if not conf.confType == "ipin":
            print("turn digree",diffDirection ,diffDirection *180/np.pi)
            if abs(diffDirection)*180/np.pi > conf.directionMinThreshold:
                print("**** turn is detected ****", diffDirection*180/np.pi)
            if abs(diffDirection)*180/np.pi > conf.directionMaxThreshold:
                print("   diffDirection is too large")
                agentRadius = sim.getAgentRadius(self.agentNo)
                # diffDirection = conf.directionMaxThreshold / abs(diffDirection)
                diffDirection = conf.directionMaxThreshold *np.pi/180 if 0 <= diffDirection else -1* conf.directionMaxThreshold *np.pi/180
                newDirection = self.prevDirection + diffDirection
                print("   newDirection\n    ", self.prevDirection*180/np.pi, diffDirection*180/np.pi, newDirection*180/np.pi)
                # newCenterPos = (self.centerPos[0] + (distCenterPos /2) * np.cos(newDirection), self.centerPos[1] + (distCenterPos /2) * np.sin(newDirection))
                distCenterPos = np.linalg.norm(np.array(self.prevCenterPos) - np.array(self.centerPos))
                newCenterPos =(self.prevCenterPos[0] + distCenterPos * np.cos(newDirection), self.prevCenterPos[1] + distCenterPos * np.sin(newDirection))
                print("  newPos\n    ",self.prevCenterPos, self.centerPos, newCenterPos)
                self.centerPos = newCenterPos
                self.direction = newDirection
                if self.nextStepSide == "right":
                    angle = self.direction - np.pi/6
                    self.rightFootPos = (self.centerPos[0] +agentRadius * np.cos(angle), self.centerPos[1] +agentRadius * np.sin(angle))

                else :
                    angle = self.direction + np.pi/6
                    self.leftFootPos = (self.centerPos[0] +agentRadius *np.cos(angle), self.centerPos[1] +agentRadius * np.sin(angle))
                # self.vPref = self.vPrefAbs / 2 * self.direction
                self.vPref = (self.vPrefAbs * np.cos(self.direction), self.vPrefAbs * np.sin(self.direction))
                print("fixed direction and vPref,", self.direction, self.vPref)

            # 回転中は移動量を半分に
            if abs(diffDirection)*180/np.pi > conf.directionMinThreshold:
                distCenterPos = np.linalg.norm(np.array(self.prevCenterPos) - np.array(self.centerPos))
                newCenterPos = (self.prevCenterPos[0] + (distCenterPos /2) * np.cos(self.direction), self.prevCenterPos[1] + (distCenterPos /2) * np.sin(self.direction))

        if commonCalc.isClossLinesPoint(centerPos, prevCenterPos, targetPos, prevTargetPos):
        # if commonCalc.isClossLines(centerVec, targetVec):
            print("2 vector is clossing",centerPos, prevCenterPos, targetPos, prevTargetPos)
            agentRadius = sim.getAgentRadius(self.agentNo) / 2
            newDirection = self.prevDirection + diffDirection /2
            newCenterPos = (self.prevCenterPos[0] + (distCenterPos /2) * np.cos(newDirection), self.prevCenterPos[1] + (distCenterPos /2) * np.sin(newDirection))
            self.centerPos = newCenterPos
            self.direction = newDirection
            self.updateFootPosandvPref(sim)
            self.velocity = ( (self.centerPos[0] - self.prevCenterPos[0]) / conf.simStepTime, (self.centerPos[1] - self.prevCenterPos[1]) / conf.simStepTime)
            # if self.nextStepSide == "right":
            #     angle = self.direction - np.pi/6
            #     self.rightFootPos = (self.centerPos[0] +agentRadius * np.cos(angle), self.centerPos[1] +agentRadius * np.sin(angle))
            #
            # else :
            #     angle = self.direction + np.pi/6
            #     self.leftFootPos = (self.centerPos[0] +agentRadius *np.cos(angle), self.centerPos[1] +agentRadius * np.sin(angle))
            # self.vPref = (self.vPrefAbs * np.cos(self.direction), self.vPrefAbs * np.sin(self.direction))
        return

    # agentの出せる最大速度の変更
    # これによって関連するパラメータも変更する
    def setvMax(self, speed):
        self.vMax = speed
        self.vPrefAbs = self.vMax /2
        self.vPref = self.direction * self.vPrefAbs
        return

    def setvPref(self, step, conf):
        if self.currentDest[-1] == "Pick":
            distanceThreshold = conf.destinationDist
            # とりあえずこの関数内だけでnparrayにしてみる
            # 他の場所で不具合が出たらself.currentDestに突っ込む時にnparrayにする
            currentDestPos = np.array(self.currentDest[-2])
            # currentDest = self.currentDest[-2]
        else:
            distanceThreshold = conf.passPointDist
            currentDestPos = np.array(self.currentDest[-2])

        pos = np.array(self.centerPos)
        distance = np.linalg.norm(currentDestPos - pos)
        # print("current destination    :",currentDest, type(currentDest))
        # print("current agent position :",pos, type(pos))
        # dest = [num[0],num[1]]?


        # 現在地点が目標地点に十分近い場合
        if distance <= distanceThreshold:
            # 目標地点に到着した時の状態の保存
            destPoint = self.currentDest[-2]
            self.pickState = "turn" if self.currentDest[-1] == "Turn" else "pass"

            # 現状使ってなさそう
            # self.memoryArrivalStep(step, destPoint)

            # 現在の目標地点がagentの最終目的地点なら速度を(0,0)に
            # print("currentDest", self.currentDest, type(self.currentDest))
            # print("centerPos", self.centerPos, type(self.centerPos))
            if self.destNo == len(self.destinationList) -1:
                self.vPref = (0, 0)
                direction = (np.array(destPoint) - np.array(self.centerPos)) / distance
                # np.arctan2は(y,x)の指定らしい(確認済み)
                self.direction = np.arctan2(direction[1], direction[0])
                print("\n", direction, self.direction)
                self.arrivalFlg = True
                print("最終目的地点に到着しています")
                return

            # 次の目標点がある場合は目標地点の更新を行う
            self.destNo += 1
            self.currentDest = self.destinationList[self.destNo]
            self.destPos = [float(self.destinationList[self.destNo][3][0]), float(self.destinationList[self.destNo][3][1])]
        # print("currentDest", self.currentDest)

        # 速度の更新
        diffPos = (np.array(self.currentDest[-2]) - np.array(self.centerPos))
        newDirection = np.arctan2(diffPos[1], diffPos[0])
        diffDirection = newDirection - self.prevDirection
        # diffDirectionを-pi~piの範囲に収めるための処理
        while(True):
            if -1*np.pi < diffDirection and diffDirection <= np.pi:
                break
            elif diffDirection <= np.pi:
                diffDirection += 2 * np.pi
            elif -1*np.pi < diffDirection:
                diffDirection -= 2 * np.pi
            # print(diffDirection)

        self.direction = self.prevDirection + diffDirection
        vPref = (self.vPrefAbs * np.cos(self.direction), self.vPrefAbs * np.sin(self.direction))
        self.vPref = (vPref[0], vPref[1])
        print("vPref :", self.vPref, type(self.vPref), "direction", self.direction*180/np.pi)
        return

    # updateAgentState中にself.directionが変更される場合には呼ぶこと
    def updateFootPosandvPref(self, sim):
        agentRadius = sim.getAgentRadius(self.agentNo) / 2
        if self.nextStepSide == "right":
            angle = self.direction - np.pi/6
            self.rightFootPos = (self.centerPos[0] +agentRadius * np.cos(angle), self.centerPos[1] +agentRadius * np.sin(angle))

        else :
            angle = self.direction + np.pi/6
            self.leftFootPos = (self.centerPos[0] +agentRadius *np.cos(angle), self.centerPos[1] +agentRadius * np.sin(angle))
        # self.vPref = self.vPrefAbs / 2 * self.direction
        self.vPref = (self.vPrefAbs * np.cos(self.direction), self.vPrefAbs * np.sin(self.direction))
        return

    #
    def updateAgentState(self, sim, step, currentTime, prevTime):
        # とりあえず現在の状態をprevにコピー
        self.copy2PrevState()

        # シミュレータからagentの最新状態を取得
        agentNo = self.agentNo
        position = sim.getAgentPosition(agentNo)
        velocity = sim.getAgentVelocity(agentNo)
        # np.arctan2は(y,x)の指定らしい(確認済み)
        direction = np.arctan2(velocity[1], velocity[0])

        self.centerPos = position
        self.velocity = velocity
        self.direction = direction
        print("set new direction", self.prevDirection*180/np.pi, self.direction *180/np.pi)
        self.checkTurnState()
        self.updateFootPosandvPref(sim)
        self.checkNewStep(sim)
        self.memoryCurrentStep(step, currentTime, prevTime)
        self.changeNextStep()
        self.prevDirection = self.direction

        return

    def writeTrajectory2File(self, fname):
        # dirName = fname[:-len(fname.split("/")[:-1])]
        # print(dirName)
        # if not os.path.isdir(dirName):
        #     os.mkdir(dirName)
        with open(fname, 'w') as f:
            # f.write(recordDef.txtHeader())
            for record in self.trajectory:
                writeTxt = record.output4txt()
                f.write(writeTxt)
