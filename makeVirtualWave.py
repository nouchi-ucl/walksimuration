import numpy as np
import os
import pickle
import csv
import time
from scipy.interpolate import interp1d

import configDef
conf = configDef.Config()


def myResample(original_time, sensor_data, resampled_time):
    # print("\n\noriginal_time",len(original_time),"\n",original_time)
    # print("\n\nsensor_data",len(sensor_data),"\n", sensor_data)
    # print("\n\nresampled_time",len(resampled_time),"\n", resampled_time)
    interp = interp1d(original_time, sensor_data)
    return interp(resampled_time)

# 開始と終了の時刻があってないっぽい
def xyzResample(targetSensorData, resampledTime):
    # print(targetSensorData)
    # print("xyzResample,", len(targetSensorData), len(resampledTime))
    # print("\n\nresampled_time", len(resampledTime), "\n", resampledTime)
    xRes = myResample(targetSensorData[:,0], targetSensorData[:,1], resampledTime)
    # print("\n\nxRes", len(xRes), "\n", xRes)
    yRes = myResample(targetSensorData[:,0], targetSensorData[:,2], resampledTime)
    # print("\n\nyRes", len(yRes), "\n", yRes)
    zRes = myResample(targetSensorData[:,0], targetSensorData[:,3], resampledTime)
    # print("\n\nzRes", len(zRes), "\n", zRes)

    # test = np.concatenate([resampledTime, xRes, yRes, zRes])
    xyzWave = np.dstack([resampledTime,xRes,yRes,zRes])
    # print(xyzWave)
    return xyzWave

def loadCsv(fname):
    dataList= []
    with open(fname, 'r') as f:

        csvData = csv.reader(f)

        for line in csvData:
            utime = line[0]
            accX = line[1]
            accY = line[2]
            accZ = line[3]

            dataList.append([utime, accX, accY, accZ])
    return dataList

def searchAcc(accData, time):
    prevAcc = accData[0]
    for acc in accData:
        if prevAcc[0] <= time and time < acc[0]:
            # print(acc)
            acc = [float(item) for item in acc]
            prevAcc = [float(item) for item in prevAcc]
            deffTime = acc[0] - prevAcc[0]
            deffArray = [(acc[1]-prevAcc[1])/deffTime, (acc[2]-prevAcc[2])/deffTime, (acc[3]- prevAcc[3])/deffTime]
            return [prevAcc[1] + deffArray[0]*(time - prevAcc[0]), prevAcc[2] + deffArray[1]*(time - prevAcc[0]), prevAcc[3] + deffArray[2]*(time - prevAcc[0])]

def makeAccWave(sensorPath, record):
    frequency = conf.virtualWaveFrequency
    accWave = []
    startTime = record.prevTime
    endTime = record.currentTime
    if startTime == endTime:
        return accWave
    diffPos = np.linalg.norm((record.diffCenterPos[0],record.diffCenterPos[1]))
    # if record.movedFoot == "right":
    #     fname = os.path.join(sensorPath, "acc-reg-right.csv")
    # else:
    #     fname = os.path.join(sensorPath, "acc-reg-left.csv")
    fname = os.path.join(sensorPath, "acc-reg-right.csv") if record.movedFoot == "right" else os.path.join(sensorPath, "acc-reg-left.csv")
    walkWave = loadCsv(fname)
    stepWave = loadCsv(os.path.join(sensorPath, "acc-reg-footstep.csv"))
    with open(os.path.join(sensorPath, "stay.csv"), 'r') as f:
        csvData = csv.reader(f)
        for line in csvData:
            stableWave = [float(item) for item in line]
    # print(stableWave)
    stretchedWave = []
    for (walk, step) in zip(walkWave[:-1],stepWave[:-1]):
        # print(wave, diffPos)
        uTime = float(walk[0]) * (endTime - startTime)
        # uTime = startTime + float(walk[0]) * (endTime - startTime)
        x = float(walk[1]) + float(step[1]) + float(stableWave[0])
        y = float(walk[2]) + float(step[2]) + float(stableWave[1])
        z = float(walk[3]) * diffPos + float(step[3]) + float(stableWave[2])
        if z == "":
            z = 0
        stretchedWave.append([uTime, x, y, z])
        # print(uTime, x, y, z, len([uTime, x, y, z]))
    uTime = startTime
    resampledTime = np.arange(0, endTime - startTime, 1/conf.virtualWaveFrequency)
    # resampledTime = np.arange(startTime, endTime, 1/conf.virtualWaveFrequency)
    # print("hoge\n")
    # print(stretchedWave[0])
    # print(stretchedWave[-1])
    # print(resampledTime)
    # print(resampledTime[-1])
    accWave = xyzResample(np.array(stretchedWave), resampledTime)
    # print(accWave)
    accWave = [[wave[0]+startTime, wave[1], wave[2], wave[3]] for wave in accWave[0]]
    # while(uTime < endTime):
    #     # print(uTime,endTime)
    #     serchedAcc = searchAcc(stretchedWave, uTime)
    #     accWave.append([uTime, serchedAcc[0], serchedAcc[1], serchedAcc[2]])
    #     uTime += frequency
    return accWave
def makeStraightWave(sensorPath, record):
    frequency = conf.virtualWaveFrequency
    gyroWave = []
    startTime = record.prevTime
    endTime = record.currentTime
    timeRange = np.array([0, endTime - startTime])

    fname = os.path.join(sensorPath, "gyro-reg-right.csv") if record.movedFoot == "right" else os.path.join(sensorPath, "gyro-reg-left.csv")
    regWave = loadCsv(fname)

    stretchedWave = []
    for wave in regWave:
        # print(wave, diffPos)
        uTime = float(wave[0]) * (endTime - startTime)
        x = float(wave[1])
        y = float(wave[2])
        z = float(wave[3])
        stretchedWave.append([uTime, x, y, z])
        # print(uTime, x, y, z, len([uTime, x, y, z]))
    uTime = startTime
    resampledTime = np.arange(0, endTime - startTime, 1/conf.virtualWaveFrequency)
    gyroWave = xyzResample(np.array(stretchedWave), resampledTime)
    gyroWave = [[wave[0]+startTime, wave[1], wave[2], wave[3]] for wave in gyroWave[0]]

    # xRes = np.array([0,0])
    # yRes = np.array([0,0])
    # zRes = np.array([0,0])
    # resampledTime = np.arange(0, endTime - startTime, 1/conf.virtualWaveFrequency)
    # newXRes = myResample(timeRange, xRes, resampledTime)
    # newYRes = myResample(timeRange, yRes, resampledTime)
    # newZRes = myResample(timeRange, zRes, resampledTime)
    # gyroWave = np.dstack([resampledTime,newXRes,newYRes,newZRes])
    # gyroWave = [[wave[0]+startTime, wave[1], wave[2], wave[3]] for wave in gyroWave[0]]

    return gyroWave

def makeGyroWave(sensorPath, recordBlock):
    frequency = conf.virtualWaveFrequency
    gyroWave = []
    startRecord = recordBlock[0]
    endRecord = recordBlock[-1]
    startTime = startRecord.prevTime
    endTime = endRecord.currentTime

    # fname = os.path.join(sensorPath, "gyro-reg-right.csv") if record.movedFoot == "right" else os.path.join(sensorPath, "gyro-reg-left.csv")
    # walkWave = loadCsv(fname)

    diffDirection = 0
    diffTime = 0
    for record in recordBlock:
        diffDirection += record.diffDirection

        # startとendの時は線形に増加/減少するので積分結果は半分になる->時間を半分にして計算
        if record.turnState =="start" or record.turnState =="end":
            diffTime += record.diffTime /2
        else:
            diffTime += record.diffTime

    if diffDirection == 0:
        return gyroWave
    direVelocity = diffDirection / diffTime


    for record in recordBlock:
        turnWave = []
        startTime = record.prevTime
        endTime = record.currentTime
        if startTime == endTime :
            continue
        timeRange = np.array([0, endTime - startTime])
        xRes = np.array([0,0])
        yRes = np.array([0,0])
        zRes = np.array([0,0])
        resampledTime = np.arange(0, endTime - startTime, 1/conf.virtualWaveFrequency)

        if record.turnState == "start":
            yRes = [0, direVelocity]
        elif record.turnState == "end":
            yRes = [direVelocity, 0]
        else :
            yRes = [direVelocity, direVelocity]

        # if len(recordBlock) > 1:
            # print(record.turnState, startTime)
            # print(yRes)

        walkWave = makeStraightWave(sensorPath, record)

        newXRes = myResample(timeRange, xRes, resampledTime)
        newYRes = myResample(timeRange, yRes, resampledTime)
        newZRes = myResample(timeRange, zRes, resampledTime)
        turnWave = np.dstack([resampledTime,newXRes,newYRes,newZRes])
        turnWave = [[wave[0]+startTime, wave[1], wave[2], wave[3]] for wave in turnWave[0]]
        partWave = [[turn[0], turn[1]+ walk[1], turn[2] +walk[2], turn[3] +walk[3]] for (turn, walk) in zip(turnWave, walkWave)]

        gyroWave.extend(partWave)
    return gyroWave

def run(dataPath, fname, accFlag, gyroFlag):
    sensorPath = os.path.join(dataPath, conf.sensorPath)
    print(fname)
    with open(fname, 'rb') as f:
        records = pickle.load(f)

    records = records[5:]

    accWave = []
    gyroWave = []
    for record in records:
        print(record.currentTime, record.turnState)

    if accFlag:
        print("make  acc wave...")
        for record in records:
            accWave.extend(makeAccWave(sensorPath, record))

    # print("\n\noutput wave data",len(accWave),"\n")
    # for i in range(20):
    #     print(accWave[i])

    if gyroFlag:
        turnBlock = []
        print("make gyro wave...")
        for record in records:
            print(record.diffDirection, record.turnState)
            turnBlock.append(record)
            if record.turnState == "straight":
                gyroWave.extend(makeStraightWave(sensorPath, record))
                turnBlock = []

            if record.turnState == "end":
                gyroWave.extend(makeGyroWave(sensorPath, turnBlock))
                turnBlock = []

    accStart = accWave[0][0]
    gyroStart = gyroWave[0][0]
    accWave = [[wave[0] - accStart, wave[1], wave[2], wave[3]] for wave in accWave]
    gyroWave = [[wave[0] - gyroStart, wave[1], wave[2], wave[3]] for wave in gyroWave]
    date = time.strftime("%m%d_%H", time.localtime())

    saveDir = os.path.join(conf.virtualLogPath,"virtualLogData/"+date)
    if not os.path.isdir(saveDir):
        os.mkdir(saveDir)

    if accFlag:
        f = open(os.path.join(saveDir,fname[:-7].split("/")[-1] + "-acc.csv"), 'w') #ファイルが無ければ作る、の'a'を指定します
        csvWriter = csv.writer(f)
        csvWriter.writerows(accWave)
        f.close()
        print(" acc file is saved as ", fname[:-7], "-acc.csv")

    if gyroFlag:
        f = open(os.path.join(saveDir,fname[:-7].split("/")[-1] + "-gyro.csv"), 'w') #ファイルが無ければ作る、の'a'を指定します
        csvWriter = csv.writer(f)
        csvWriter.writerows(gyroWave)
        f.close()
        print("gyro file is saved as ", fname[:-7], "-gyro.csv")

if __name__ == '__main__':
    dataPath = conf.dataPath
    agentNo = 0
    fname = os.path.join(dataPath, "record/record_"+str(agentNo)+".pickle")
    accFlag = True
    gyroFlag = True
    run(dataPath, fname, accFlag, gyroFlag)
