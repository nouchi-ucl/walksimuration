#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
from operator import attrgetter
import numpy as np
import sys

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef_v2
from classdef import pathDef
from classdef import gridMapDef
import configDef
import fileIO



# 作成したマップを使って,2地点間の最小コスト経路を探索
# requiredGoalPathNum の本数分探索が完了したら終了
# 通過セルが1つでも違う場合は別の経路として扱う
# 計算上,最速でゴールに到着したpathが最小コストになるとは限らない
#   方向転換によるコスト,斜めへの移動があるため
def pathPlanning(conf, gridMap, startNode, goalNode):
    print("Path Planning... ", startNode, " ", goalNode)
    requiredGoalPathNum = conf.requiredPathNum
    findStepThreshold = conf.findStepThreshold
    goalPath = []
    # currentPath = []
    neighborNodeList = []
    # currentPath.append(pathDef.Path(gridMap[startNode[1]][startNode[0]]))
    prevStepNode = []
    tmpList = []
    currentStepNode = []
    nextStepNode = []
    currentStepNode.append(startNode)
    goal = gridMap.gridmap[goalNode[1]][goalNode[0]]
    goal.isObstacle = False
    prevGoalMinCost = goal.minCost
    # print("goal ", goal)
    lastFindCount = 0
    stepCount = 0
    breakCount = 0

    while(True):
        # 最後にgoalに到達したstepから一定以上新しいgoal pathがみつからない場合探索を終了する
        if lastFindCount != 0 and lastFindCount + findStepThreshold < stepCount:
            print("goalPath is not enough ...", len(goalPath), " / ", requiredGoalPathNum, " but new path reach the goal has not found in last ", findStepThreshold, " steps.")
            return goal

        if breakCount > findStepThreshold or len(currentStepNode) == 0:
            print("ERROR : route could not find. Is the goal Node is in any obstacle ?")
            return goal
        tmpList = copy.deepcopy(currentStepNode)
        for address in currentStepNode:
            edgeNode = gridMap.gridmap[address[1]][address[0]]
            if edgeNode.neighborUp is not None:
                updateFlag, neighbor = gridMap.stepNewNode(goal, edgeNode, edgeNode.neighborUp, 'up',stepCount)
                if updateFlag:
                    nextStepNode.append(neighbor.address)
            if edgeNode.neighborDown is not None:
                updateFlag, neighbor = gridMap.stepNewNode(goal, edgeNode, edgeNode.neighborDown, 'down',stepCount)
                if updateFlag:
                    nextStepNode.append(neighbor.address)
            if edgeNode.neighborRight is not None:
                updateFlag, neighbor = gridMap.stepNewNode(goal, edgeNode, edgeNode.neighborRight, 'right',stepCount)
                if updateFlag:
                    nextStepNode.append(neighbor.address)
            if edgeNode.neighborLeft is not None:
                updateFlag, neighbor = gridMap.stepNewNode(goal, edgeNode, edgeNode.neighborLeft, 'left',stepCount)
                if updateFlag:
                    nextStepNode.append(neighbor.address)

            if conf.allowDiagonalMove:
                if edgeNode.neighborRightUp is not None:
                    updateFlag, neighbor = gridMap.stepNewNode(goal, edgeNode, edgeNode.neighborRightUp, 'rightUp',stepCount)
                    if updateFlag:
                        nextStepNode.append(neighbor.address)
                if edgeNode.neighborRightDown is not None:
                    updateFlag, neighbor = gridMap.stepNewNode(goal, edgeNode, edgeNode.neighborRightDown, 'rightDown', stepCount)
                    if updateFlag:
                        nextStepNode.append(neighbor.address)
                if edgeNode.neighborLeftUp is not None:
                    updateFlag, neighbor = gridMap.stepNewNode(goal, edgeNode, edgeNode.neighborLeftUp, 'leftUp',stepCount)
                    if updateFlag:
                        nextStepNode.append(neighbor.address)
                if edgeNode.neighborLeftDown is not None:
                    updateFlag, neighbor = gridMap.stepNewNode(goal, edgeNode, edgeNode.neighborLeftDown, 'leftDown',stepCount)
                    if updateFlag:
                        nextStepNode.append(neighbor.address)

        if len(tmpList) == len(nextStepNode):
            breakCount += 1
        prevStepNode = copy.deepcopy(tmpList)
        currentStepNode = copy.deepcopy(nextStepNode)
        if stepCount % 50 == 0 and stepCount != 0:
            print("In ", stepCount," step, there are ", len(currentStepNode))
        stepCount += 1
        if prevGoalMinCost != goal.minCost:
            prevGoalMinCost = goal.minCost
            lastFindCount = stepCount
            print("   goal is found in this step:", stepCount)

    return goal


def run(conf, startPoint, goalPoint, action, displayFlag):
    dataPath = conf.dataPath
    picklePath = os.path.join(dataPath, "pickle")

    mapInfoList = []
    obstacles = []
    shelf = []

    # # pickleファイルからオブジェクトを復元
    with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(picklePath, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0]
    ylim = mapInfoList[1]

    # あらかじめ作成しておいたgridMapをロード
    # makeOccupancyGrid.py
    with open(os.path.join(picklePath, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    cellSize = gridMap.cellSize

    # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
    obstacleList = []
    if displayFlag:
        obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
        obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
        obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])


    startNode = gridMap.searchNodeFromPoint(startPoint)
    gridMap.gridmap[startNode[1]][startNode[0]].step = 0
    goalNode = gridMap.searchNodeFromPoint(goalPoint)
    goalPos = gridMap.gridmap[goalNode[1]][goalNode[0]]

    minCostPath = []

    # スタート地点とゴール地点が同じセル内に入っている場合の処理
    # 0mの移動として出力
    # スタート地点への探索はエラーになるため例外処理を挟まないといけない(探索が終了しない)
    if startNode == goalNode:
        currentNode = gridMap.gridmap[goalNode[1]][goalNode[0]]
        minCostPath.append([currentNode.address, (currentNode.posx, currentNode.posy), 0, currentNode.prevDirection])

    else:
        goal = pathPlanning(conf, gridMap, startNode, goalNode)
        if goal.minCostPrevNode is None:
            print("Could not find path to ", goalPoint, " from ", startPoint, ". \nSo exit this program")

            # 表示用pltの設定
            fig, (ax1) = plt.subplots(1,1,figsize=((self.xMax-self.xMin+20)/10,(self.yMax-self.yMin+20)/10))
            ax1.set_xlim(xlim[0]-10,xlim[1]+10)
            ax1.set_ylim(ylim[0]-10,ylim[1]+10)
            print("xlim", xlim[0],"  ",xlim[1])
            print("ylim", ylim[0],"  ",ylim[1])

            # # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
            # # 単純にpathの探索をするだけなら必要ない
            obstacleList = []
            obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
            obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
            # 外周の追加
            obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
            print("obstacles...",len(obstacleList))

            for (x1, x2, y1, y2) in obstacleList:
                ax1.add_patch(plt.Rectangle((x1,y1),x2-x1,y2-y1, fc ='#303030', fill = False))

            goal = gridMap.gridmap[goalNode[1]][goalNode[0]]
            start = gridMap.gridmap[startNode[1]][startNode[0]]
            ax1.scatter(goal.posx,goal.posy, color = "#ff0000", s = 20)
            ax1.scatter(start.posx,start.posy, color = "#00ff00", s = 20)
            plt.show()

        currentNode = goal
        while(True):
            minCostPath.append([currentNode.address, (currentNode.posx, currentNode.posy), currentNode.minCost, currentNode.prevDirection])
            # print(currentNode.address, currentNode.minCost, currentNode.step)
            if currentNode.minCostPrevNode is not None:
                # print("     ", currentNode.minCostPrevNode, gridMap.gridmap[currentNode.minCostPrevNode[1]][currentNode.minCostPrevNode[0]].address)
                currentNode = gridMap.gridmap[currentNode.minCostPrevNode[1]][currentNode.minCostPrevNode[0]]
            else :
                break
        minCostPath.reverse()
        # minCostPath = smoothing(minCostPath, gridMap, obstacleList)



    if not os.path.isdir(os.path.join(dataPath, "path")):
        os.mkdir(os.path.join(dataPath, "path"))
    startIDName = "%.3f,%.3f"%(startPoint[0],startPoint[1])
    endIDName = "%.3f,%.3f"% (goalPoint[0],goalPoint[1])
    writeName = "path/path_"+ startIDName+"_"+endIDName
    with open(os.path.join(dataPath, writeName + ".txt"), 'w') as f:
        prevDirection = None
        for node in minCostPath:
            if prevDirection is None or prevDirection == node[3]:
                turnFlag = "False"
            else :
                turnFlag = "True"
            prevDirection = node[3]
            f.write(str(node[0][0])+","+str(node[0][1])+","+str(node[1][0])+","+str(node[1][1])+","+str(node[2])+","+turnFlag+"\n")

    print("write min cost path to ", os.path.join(dataPath, writeName + ".txt"))
    with open(os.path.join(dataPath, writeName + ".pickle") ,mode = 'wb') as f:
    	pickle.dump(minCostPath, f)

    if displayFlag:
        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=((self.xMax-self.xMin+20)/10,(self.yMax-self.yMin+20)/10))
        ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])


        for (x1, x2, y1, y2) in obstacleList:
            ax1.add_patch(plt.Rectangle((x1,y1),x2-x1,y2-y1, fc ='#303030', fill = False))

        # コストの低いpath上位5ルートまでをマップに表示
        # colorList = ['#a1a900','#e4007f', '#2ca9e1', '#07d34f', '#c92b29']
        # for j in range(len(path) if len(path) < 5 else 5):
        for (i,address) in enumerate(minCostPath):
            node = gridMap.gridmap[address[0][1]][address[0][0]]
            if i == 0:
                # print("Red path is ", path[j].routeCost, " in ", len(path[j].route) -1, " step\n")
                prevNode = node
                continue
            else:
                plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#a1a900')
                prevNode = node
        plt.show()

    return writeName + ".txt"

if __name__ == '__main__':
    conf = configDef.Config()

    # 表示切替用のフラグ
    displayFlag = True
    run(conf, "Pass", displayFlag)
