#!/usr/bin/python
# -*- coding: utf-8 -*-

# ほぼipin専用コード センサデータを時間ごとに切り分けて　更にBLE受信データだけ切り離す

import os
import sys
import matplotlib.pyplot as plt
import pickle
import csv
import json
import time
import datetime

from classdef import sensorDataDef
from classdef import bleDataDef
import configDef

# センサデータを時間ごとに切り出してオブジェクト化, BLEデータがある場合は分離して別に格納する
def getSencorData(path):
    sensorData = []
    bleData = {}
    with open(path, mode = 'r') as f:
        lines = f.readlines()
        print (lines[0].split("\n")[0].split(" "))
        for line in lines:
            data = line.split("\n")[0].split(" ")
            uTime = int(data[0])
            uTimeMicro = int(data[1])
            angularVelocity = (float(data[2]),float(data[3]),float(data[4]))
            acceleration = (float(data[5]),float(data[6]),float(data[7]))
            magnetism = (float(data[8]),float(data[9]),float(data[10]))
            gryoTemperature = (float(data[11]),float(data[12]), float(data[13]))
            temperature = float(data[14])
            pressure = float(data[15])
            deltaT = float(data[16])
            sensorData.append(sensorDataDef.SensorData(uTime,uTimeMicro,angularVelocity,acceleration,magnetism,gryoTemperature,temperature,pressure,deltaT))

            # dataが18個以上→BLEデータあり　なのでBLEは別に格納
            if len(data) > 18:
                bleCount = int(data[17])
                bleDataDict = {}
                # print(len(data), data)
                for i in range(bleCount):
                    if len(data) < 19 + (i * 2):
                        break
                    if data[18 + (i*2)] == 'null':
                        macAdd = "null"
                    else:
                        macAdd = data[18 + (i*2)]
                    if data[19 + (i*2)] =='null':
                        intensity = "null"
                    else:
                        intensity = int(data[19 + (i*2)])

                    if macAdd in bleDataDict:
                        # print(bleDataList,"\n",bleDataList[macAdd], type(bleDataList[macAdd]))
                        tempList = bleDataDict[macAdd]
                        tempList.append(intensity)
                        bleDataDict[macAdd] = tempList
                        # bleDataList[macAdd].append[intensity]
                    else:
                        tempList = []
                        tempList.append(intensity)
                        bleDataDict.update({macAdd : tempList})
                    # bleDataList.append({macAdd: intensity})
                # bleData.append(BleData(uTime,uTimeMicro,bleCount,bleDataList))
                bleData.update({uTime+uTimeMicro/1000 : bleDataDict})
    return sensorData, bleData

def write4PDR(path, sensorData):
    with open(path+'_acc.csv', 'w') as f:
        writer = csv.writer(f, lineterminator='\n') # 改行コード（\n）を指定しておく
        writer.writerows([[data.uTime+data.uTimeMiri/1000, data.accelerationX, data.accelerationY, data.accelerationZ]for data in sensorData])

    with open(path+'_gyro.csv', 'w') as f:
        writer = csv.writer(f, lineterminator='\n') # 改行コード（\n）を指定しておく
        writer.writerows([[data.uTime+data.uTimeMiri/1000, data.angularVelocityX, data.angularVelocityY, data.angularVelocityZ]for data in sensorData])

    with open(path+'_magnetic.csv', 'w') as f:
        writer = csv.writer(f, lineterminator='\n') # 改行コード（\n）を指定しておく
        writer.writerows([[data.uTime+data.uTimeMiri/1000, data.magnetismX, data.magnetismY, data.magnetismZ]for data in sensorData])

    print("write to ", path, "_acc , _gyro, _magnetic .csv")

def run(config):
    dataPath = config.dataPath
    picklePath = os.path.join(dataPath, "pickle")

    for userNum in conf.userList:
        if not os.path.isdir(os.path.join(dataPath, 'sensor/Terminal'+ str(userNum))):
            continue

        sensorData, bleData = getSencorData(os.path.join(dataPath, 'sensor/Terminal'+str(userNum)+'/'+str(userNum)+'_Working_data.txt'))

        # 出力用各配列をpickleファイルとして保存
        with open(os.path.join(picklePath, 'agent'+str(userNum)+'_pickData.pickle'),mode = 'wb') as f:
            pickle.dump(sensorData, f)
        with open(os.path.join(picklePath, 'agent'+str(userNum)+'_bleReceiveData.pickle'),mode = 'wb') as f:
            pickle.dump(bleData, f)
        with open(os.path.join(dataPath, "ble"+str(userNum)+".json"), mode = "w") as f:
            json.dump(bleData, f, ensure_ascii=False, indent=4, sort_keys=True, separators=(',', ': '))
        # PDR用にcsv出力
        if not os.path.isdir(os.path.join(dataPath, 'csv')):
            os.mkdir(os.path.join(dataPath, 'csv'))
        write4PDR(os.path.join(dataPath, 'csv/agent'+str(userNum)), sensorData)


if __name__ == '__main__':
    conf = configDef.Config()
    run(conf)
