import numpy as np


# point1 point2を通る直線とpoint3との距離を返す
# point : (x,y)のタプル
def distancePointandLine(point1, point2, point3):
    u = np.array([point2[0] - point1[0], point2[1] - point1[1]])
    v = np.array([point3[0] - point1[0], point3[1] - point1[1]])
    L = abs(np.cross(u, v) / np.linalg.norm(u))
    return L

# x1,x2,y1,y2で囲まれた矩形とpointとの距離を返す
# pointが矩形内の場合は0を返す
def distancePointandRectangle(point,x1,x2,y1,y2):
    if inRectangle(point,x1,x2,y1,y2):
        minDist = 0
    else:
        minDist = distancePointandLine((x1,y1),(x2,y1),point)
        dist = distancePointandLine((x2,y1),(x2,y2),point)
        if dist < minDist:
            minDist = dist
        dist = distancePointandLine((x2,y2),(x1,y2),point)
        if dist < minDist:
            minDist = dist
        dist = distancePointandLine((x1,y2),(x1,y1),point)
        if dist < minDist:
            minDist = dist
    return minDist

# pointがx1,x2,y1,y2に囲まれた矩型領域内に存在するかどうかの判定
# x1<x2, y1<y2でないといけないので引数に注意
def inRectangle(point,x1,x2,y1,y2):
    x = point[0]
    y = point[1]
    if x1 <= x and x <= x2 and y1 <= y and y <= y2:
        return True
    else:
        return False

# 線分vec1とvec2が交差しているかどうかを判断
# 交差していたらtrue していなかったらfalse
def isClossLinesVec(vec1, vec2):
    p1 = vec1[0]
    p2 = vec1[1]
    p3 = vec2[0]
    p4 = vec2[1]
    # print(p1,p2,p3,p4)
    t1 = (p1 - p2) * (p3 - p1) + (p1 - p2) * (p1 - p3)
    t2 = (p1 - p2) * (p4 - p1) + (p1 - p2) * (p1 - p4)
    t3 = (p3 - p4) * (p1 - p3) + (p3 - p4) * (p3 - p1)
    t4 = (p3 - p4) * (p2 - p3) + (p3 - p4) * (p3 - p2)
    return t1*t2<=0 and t3*t4<=0

# 線分abと線分cdが交差するか判定
# 線分p1p2と線分p3p4が交差するか判定
def isClossLinesPoint(p1, p2, p3, p4):
    ax = p1[0]
    ay = p1[1]
    bx = p2[0]
    by = p2[1]
    cx = p3[0]
    cy = p3[1]
    dx = p4[0]
    dy = p4[1]
    ta = (cx - dx) * (ay - cy) + (cy - dy) * (cx - ax)
    tb = (cx - dx) * (by - cy) + (cy - dy) * (cx - bx)
    tc = (ax - bx) * (cy - ay) + (ay - by) * (ax - cx)
    td = (ax - bx) * (dy - ay) + (ay - by) * (ax - dx)

    return tc * td <= 0 and ta * tb <= 0
    #端点を含まない場合
    # return tc * td < 0 && ta * tb < 0
