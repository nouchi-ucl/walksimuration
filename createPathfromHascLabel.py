#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import pickle
import csv
import numpy as np
import sys

import configDef
conf = configDef.Config()
DATA_PATH = conf.dataPath

def run(fname, userNum):
    tmplist = []
    # with open(fname, "r") as f:
    #     print(f)
    with open(fname,"r") as f:
        labelData = csv.reader(f)
        header = next(labelData)
        for line in labelData:
            if line[-1].split(";")[-1] == "start":
                tmplist.append(["start",line[-1].split(";")[0]])
            elif not line[-1].split(";")[0] == "walk":
                tmplist.append(["pass",line[2].split(";")[0]])

    for line in tmplist:
        print(line)


    structureData = []
    structureDataPath = "/Users/nouchi/Documents/workspace/hask_workspace/HASC-IPSC/structure-with-nodeid.csv"
    with open(structureDataPath,"r") as f:
        lines = csv.reader(f)
        header = next(lines)
        for line in lines:
            structureData.append([line[0],line[1],line[2],line[3]])
            structureData.append([line[4],line[5],line[6],line[7]])

    passList = []
    for data in tmplist:
        for line in structureData:
            if line[0] == data[1]:
                # どうやらmapとxyが逆らしい
                # passList.append([float(line[2]),float(line[1]),data[0]])
                passList.append([float(line[1]),float(line[2]),data[0]])

                # passList.append([float(line[1]),float(line[2]),data[0]])

    for line in passList:
        print(line)

    if not os.path.isdir(os.path.join(DATA_PATH,"wms")):
        os.mkdir(os.path.join(DATA_PATH,"wms"))
    with open(os.path.join(DATA_PATH,"wms/pathList"+str(userNum)+".csv"), "w") as f:
        csvWriter = csv.writer(f)
        csvWriter.writerows(passList)
        print("pathList is saved as", os.path.join(DATA_PATH,"wms/pathList"+str(userNum)+".csv"))
        f.close()


if __name__ == '__main__':
    dataPath = os.path.join(DATA_PATH, "csv/HASC30088.csv")
    # dataPath = "/Users/nouchi/Documents/workspace/hask_workspace/HASC-30045/"
    # useBLEFlag = False
    userNum = 0
    run(dataPath, userNum)
