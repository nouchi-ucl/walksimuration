#!/usr/bin/env python
import rvo2
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os
import pickle

import searchShelf
import configDef
import fileIO

# path = None
conf = configDef.Config()
DATA_PATH = conf.dataPath

# simulation環境の設定
# float timeStep, float neighborDist, size_t maxNeighbors,float timeHorizon,
# float timeHorizonObst, float radius,float maxSpeed, tuple velocity=(0, 0)
# STEP_TIME = 1/10
# sim = rvo2.PyRVOSimulator(STEP_TIME, 0.3, 5, 1.5, 0.5, 0.2, 1.5)
STEP_TIME = conf.simStepTime
sim = rvo2.PyRVOSimulator(STEP_TIME, conf.neighborDist, conf.maxNeighbors,
                    conf.timeHorizon, conf.timeHorizonObst, conf.radius, conf.maxSpeed)

# TIME_TABLE_LIST = [ [], [1485239145.,1485240508.,1485252057.],
#                     [1485239148.,1485240513.,1485248722.],
#                     [1485239234.,1485240524.,1485252460.],
#                     [1485239243.,1485240545.,1485252313.],
#                     [1485239286.,1485240550.,1485252407.],
#                     [1485239291.,1485240555.,1485252344.],
#                     [1485239328.,1485240562.,1485252227.],
#                     [1485239334.,1485240568.,1485252085.]]

RUN_LIST = [4]
# userNumList = [2,3,4,5,6,7,8]


def setvPref(dataPath, agentNo, num, destination, pickList, step):
    # print(destination[num[0]][num[1]])
    # print(float(destination[num[0]][num[1]][2]))
    # print(float(destination[num[0]][num[1]][3]))

    shelfID = None

    if destination[num[0]][num[1]][-1] == "Pick":
        distanceThreshold = 0.25
        shelfID = destination[num[0]][num[1]][-2]
        if len(shelfID.split(" ")) > 1:
            currentDest = (float(shelfID.split(" ")[0][1:]),float(shelfID.split(" ")[1][:-1]))
        else:
            currentDest = searchShelf.run(shelfID, dataPath)
        # print("current destination is pickPoint ID :",shelfID, ", pos :", currentDest)
    elif destination[num[0]][num[1]][-1] == "Pass":
        distanceThreshold = 0.25
        shelfID = destination[num[0]][num[1]][-2]
        if len(shelfID.split(" ")) > 1:
            currentDest = (float(shelfID.split(" ")[0][1:]),float(shelfID.split(" ")[1][:-1]))
        else:
            currentDest = searchShelf.run(shelfID, dataPath)
        # print("current destination is pickPoint ID :",shelfID, ", pos :", currentDest)
    else:
        distanceThreshold = 0.2
        # distanceThreshold = 0.25
        currentDest = np.array([float(destination[num[0]][num[1]][2]),float(destination[num[0]][num[1]][3])])

    pos = np.array(sim.getAgentPosition(agentNo))
    distance = np.linalg.norm(currentDest - pos)
    dest = [num[0],num[1]]
    # print(pos,currentDest,distance)


    if distance <= distanceThreshold:
        if num[1] == len(destination[num[0]]) - 1:
            vPref = (0,0)
            if destination[num[0]][num[1]][-1] == "Pick":
                shelfID = destination[num[0]][num[1]][-2]
                if len(shelfID.split(" ")) > 1:
                    pickList[agentNo].append([step, shelfID, "Pass"])
                else:
                    pickList[agentNo].append([step, shelfID, "Pick"])
            if destination[num[0]][num[1]][-1] == "Pass":
                # shelfID = destination[num[0]][num[1]][-2]
                endPoint = (float(destination[num[0]][num[1]][2]), float(destination[num[0]][num[1]][3]))
                pickList[agentNo].append([step, endPoint, "Pass"])
            # print(num[1], len(destination[num[0]]), destination[0][num[1]], destination[0][len(destination[num[0]]) -1])
            print("つきました")
            return dest, vPref, None
        print(num[1], len(destination[num[0]]), destination[0][num[1]], destination[0][num[1]+1])
        currentDest = np.array([float(destination[num[0]][num[1]+1][2]),float(destination[num[0]][num[1]+1][3])])
        if destination[num[0]][num[1]+1][-1] == "Pick":
            shelfID = destination[num[0]][num[1]+1][-2]
            if len(shelfID.split(" ")) > 1:
                currentDest = (float(shelfID.split(" ")[0][1:]),float(shelfID.split(" ")[1][:-1]))
            else:
                currentDest = searchShelf.run(shelfID, dataPath)
            # print("next destination is pickPoint ID :", shelfID, ", pos :", currentDest)
        # if destination[num[0]][num[1]+1][-1] == "Pass":
            # currentDest = np.array([float(destination[num[0]][num[1]+1][2]),float(destination[num[0]][num[1]+1][3])])

        if destination[num[0]][num[1]][-1] == "Pick":
            shelfID = destination[num[0]][num[1]][-2]
            if len(shelfID.split(" ")) > 1:
                position = (float(shelfID.split(" ")[0][1:]), float(shelfID.split(" ")[1][:-1]))
                pickList[agentNo].append([step, position, "Pass"])
            else:
                pickList[agentNo].append([step, int(shelfID), "Pick"])
        if destination[num[0]][num[1]][-1] == "Pass":
            # shelfID = destination[num[0]][num[1]][-2]
            endPoint = (float(destination[num[0]][num[1]][2]), float(destination[num[0]][num[1]][3]))
            pickList[agentNo].append([step, endPoint, "Pass"])

        dest = [num[0],num[1]+1]
    direction = (currentDest - pos) / distance
    vPref = sim.getAgentMaxSpeed(agentNo) /2 *direction
    # vPref = (velocity[0] / np.linalg.norm(velocity), velocity[1] / np.linalg.norm(velocity))
    # print("renew vPref:",currentDest, pos, distance, direction, vPref)
    return dest, (vPref[0], vPref[1]), currentDest

def run(dataPath, drawFlag, runList):
    picklePath = os.path.join(dataPath, "pickle")

    # print(destination)
    # Pass either just the position (the other parameters then use
    # the default values passed to the PyRVOSimulator constructor),
    # or pass all available parameters.
    agents = []
    agentDest = []
    writeFileNameList = []
    userNumList = runList
    # for i in range(0,5):
    #     d = np.random.rand() * 5 - 3
    #     # d = d * 2 -1
    #     agents.append(sim.addAgent((0+d, 0+d)))
    #     agentDest.append([0,0])
    #     agents.append(sim.addAgent((100+d, 0+d)))
    #     agentDest.append([1,0])
    #     agents.append(sim.addAgent((100+d, 100+d)))
    #     agentDest.append([2,0])
    #     agents.append(sim.addAgent((0+d, 100+d)))
    #     agentDest.append([3,0])

    # # Obstacles are also supported.

    mapInfoList = []
    #  読み込み用リスト 棚
    shelfList = []
    #  読み込み用リスト 障害物
    obstacleList = []

    wayPointList = []
    destination = []

    # 表示用リスト 棚/障害物
    obstacles = []

    # pickleファイルからオブジェクトを復元
    with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)
    print (mapInfoList)
    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    startPoint = mapInfoList[0][2]


    with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'rb') as f:
        shelfList = pickle.load(f)

    with open(os.path.join(picklePath, 'obstacle.pickle'),mode = 'rb') as f:
        obstacleList = pickle.load(f)

    # wayPointList = [[]*len(userNumList)]
    writeFileNameList = [[] for i in range(0, len(userNumList))]
    destPos = [[] for i in range(0, len(userNumList))]
    pickleDumpArray = [[] for i in range(0, len(userNumList))]
    for (agentNo,userNum) in enumerate(userNumList):
        with open(os.path.join(dataPath, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'rb') as f:
        # with open(os.path.join(dataPath, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'rb') as f:
            wayPointList = pickle.load(f)
            writeFileNameList[agentNo].append(str(userNum))
        # print(wayPointList)
        distNo = 0
        agents.append(sim.addAgent(startPoint))
        agentDest.append([agentNo, distNo])
        destination.append(wayPointList)
        destPos[agentNo].append([[float(destination[agentNo][distNo][2]), float(destination[agentNo][distNo][3])]])
        pickleDumpArray[agentNo].append([0, 0.0, (startPoint[0], startPoint[1]), (float(destination[agentNo][distNo][2]), float(destination[agentNo][distNo][3])), ["Start", "None"]])


    if drawFlag:
        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
        ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])
        objs = []

        img = plt.imread(os.path.join(dataPath, "map/Map_image.png"))
        ax1.imshow(img, extent=[xlim[0], xlim[1], ylim[0], ylim[1]], aspect=1)


    # マップ外周を障害物として追加
    obstacles.append(sim.addObstacle([(xlim[0],ylim[0]),(xlim[1],ylim[0]),(xlim[1],ylim[1]),(xlim[0],ylim[1])]))
    # obstacleを障害物として追加
    obstacles.append([sim.addObstacle([(s[1],s[3]),(s[2],s[3]),(s[2],s[4]),(s[1],s[4])]) for s in shelfList])
    # shelfを障害物として追加
    obstacles.append([sim.addObstacle([(o[0],o[2]),(o[1],o[2]),(o[1],o[3]),(o[0],o[3])]) for o in obstacleList])

    # これ以降は障害物の追加をしても反映されないので注意
    sim.processObstacles()

    print('Simulation has %i agents and %i obstacle vertices in it.' %
          (sim.getNumAgents(), sim.getNumObstacleVertices()))

    print('Running simulation')

    # for step in range(200):
    step = 0
    stopCount = 0
    loopFlag = True
    prevPos = None
    pickList = [[[0, startPoint, "Pass"]] for i in range(0, len(userNumList))]
    print("length of destination ", len(destination[0]), destination[0][-1])
    while(loopFlag):

        # 目的地点に十分近い場合は目標の更新・そうでない場合は速度の更新を行う
        for (i,agent) in enumerate(agents):
            agentDest[i], vPref, destPos[i]= setvPref(dataPath, agent,agentDest[i],destination, pickList, step)

            # print("\ndestination : ", destination[agentDest[i][0]][agentDest[i][1]], "\nvPref : ", vPref)
            # print("\ndestination : ", destination[dest[0]][dest[1]], "vPref : ", vPref)
            sim.setAgentPrefVelocity(agent, vPref)
            # print(sim.getAgentVelocity(agent))

        # 全てのagentに対してdestPosがない(次の目標地点がない)場合はシミュレーションを終了
        flag = False
        for agentNo in agents:
            if destPos[agentNo] is not None:
                flag = True
        # if step > 1000:
        #     flag = False
        loopFlag = flag
        # print("DestPos : ", destPos[agentNo])

        positions = ['(%5.3f, %5.3f)' % sim.getAgentPosition(agentNo)
                     for agentNo in agents]
        # print('step=%2i  t=%.3f  Pos =%s' % (step, sim.getGlobalTime(), '  '.join(positions)))
        agentsPos = [(sim.getAgentPosition(agentNo)[0], sim.getAgentPosition(agentNo)[1]) for agentNo in agents]
        agentsPosx = [sim.getAgentPosition(agentNo)[0] for agentNo in agents]
        agentsPosy = [sim.getAgentPosition(agentNo)[1] for agentNo in agents]

        # agentsDestPos = [(destPos[agentNo][0], destPos[agentNo][1]) if destPos[agentNo] is not None else None for agentNo in agents]
        agentsDestPosx = [destPos[agentNo][0] if destPos[agentNo] is not None else float(destination[agentNo][-1][2]) for agentNo in agents]
        agentsDestPosy = [destPos[agentNo][1] if destPos[agentNo] is not None else float(destination[agentNo][-1][3]) for agentNo in agents]
        action = destination[agentDest[agentNo][0]][agentDest[agentNo][1]][-1]
        agentsDestAttribute = [["Pick", destination[agentDest[agentNo][0]][agentDest[agentNo][1]][-2]]
                                if destination[agentDest[agentNo][0]][agentDest[agentNo][1]][-1] == "Pick"
                                else [action, "None"] for agentNo in agents]
        # agentsDestPos = [(posx, posy) for (posx, posy) in zip(agentsDestPosx, agentsDestPosy)]
        agentsDestPos = [(posx, posy) for (posx, posy) in zip(agentsDestPosx, agentsDestPosy)]
        for agentNo in agents:
            # print([agentsPos[agentNo], agentsDestPos[agentNo]])
            pickleDumpArray[agentNo].append([step, sim.getGlobalTime(), agentsPos[agentNo], agentsDestPos[agentNo], agentsDestAttribute[agentNo]])

        if prevPos == agentsPos:
            stopCount += 1
            if stopCount > 10 / STEP_TIME:
                print("There was no movement for 10sec. Finish simulation")
                break

        print("\nIn %i step : " % step)
        for agentNo in agents:
            posx = agentsPos[agentNo][0]
            posy = agentsPos[agentNo][1]
            destx = agentsDestPos[agentNo][0]
            desty = agentsDestPos[agentNo][1]
            velx = sim.getAgentVelocity(agentNo)[0]
            vely = sim.getAgentVelocity(agentNo)[1]
            print("    Agent%i pos:(%.3f,%.3f) dest:(%.3f,%.3f) %s velocity:(%.3f,%.3f)"
                    %(userNumList[agentNo],posx,posy,destx,desty,agentsDestAttribute[agentNo][0],velx,vely))

        prevPos = agentsPos


        if drawFlag:
            # x1 = [float(data[2]) for data in destination[0]]
            # y1 = [float(data[3]) for data in destination[0]]
            pt1 = ax1.scatter(agentsPosx,agentsPosy,color = '#c92b29', s=8)
            pt2 = ax1.scatter(agentsDestPosx,agentsDestPosy,color = '#2ca9e1', s=5)

            objs.append([pt1,pt2])

        step += 1

        # RVO2の1ステップ更新
        sim.doStep()


    if not os.path.isdir(os.path.join(dataPath, "simulation")):
        os.mkdir(os.path.join(dataPath, "simulation"))
    for agentNo in agents:
        print(writeFileNameList)
        with open(os.path.join(dataPath, "simulation/"+ writeFileNameList[agentNo][0]+ "_simulatedTrajectory.pickle"), mode = 'wb') as f:
            pickle.dump(pickleDumpArray[agentNo], f)

    for agentNo in agents:
        for pick in pickList[agentNo]:
            print(pick)
        with open(os.path.join(picklePath, "pickStepList"+str(userNumList[agentNo])+".pickle"), "wb") as f:
            pickle.dump(pickList[agentNo],f)

    if drawFlag:
        interval = 100 * STEP_TIME
        ani = animation.ArtistAnimation(fig, objs, interval = 50, repeat = True)
        plt.show()

if __name__ == '__main__':
    drawFlag = False
    run(DATA_PATH, drawFlag, RUN_LIST)
