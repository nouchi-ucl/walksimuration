#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
from operator import attrgetter
import numpy as np

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef_v2
from classdef import pathDef

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

ALLOW_DIAGONAL_MOVE = True
ROUTE_COST_THRESHOLD = 20000
DIAGONAL_MOVE_COST = 1.4
REQUIRED_PATH_NUM = 50
TURN_COST = 2.0


# x,yの座標からその点が含まれるセルのアドレスを返す point -> gridMap[address[1]][address[0]]
# Nodeインスタンスそのものを返すわけではないので注意
def searchNodeFromPoint(gridMap, point, cellSize):
    address = None
    print("searchNodeFrom Point", point, cellSize)

    for line in gridMap:
        yDist = abs(line[0].posy - point[1])
        if yDist <= cellSize:
            for node in line:
                xDist = abs(node.posx - point[0])
                if xDist <= cellSize:
                    address = node.address
    if address is None:
        print("Target point ", point, " is out of map region")
        sys.exit()
    return address

# パスの更新:1マス進んだ先のノード(newNode)をpathに追加していく
# 端部のノードの更新, routeへ追加, path全体コストの更新
# コストは斜め方向に移動する場合は x1.4
# 前回の移動方向に対して回転している場合はコストを追加
def updatePath(currentNode, neighbor, direction):
    # newPath = copy.deepcopy(path)
    #
    # newPath.prevNode = newPath.edgeNode
    # newPath.edgeNode = newNode
    # # newPath.route.append(newNode)
    # newPath.routeCost += newNode.cost
    # turnFlag = False
    # if newPath.moveDirection != direction and newPath.moveDirection is not None:
    #     newPath.routeCost += 2.0
    #     turnFlag = True
    # newPath.moveDirection = direction
    # newPath.route.append([newNode.address, turnFlag])
    updateFlag = False
    moveCost = 0.
    if direction == "up" or direction == "down" or direction == "right" or direction == "left":
        directionCost = 1.
    else :
        directionCost = 1.4
    if currentNode.minCost is None:
        moveCost = neighbor.cost * directionCost
    else:
        moveCost = currentNode.minCost + neighbor.cost * directionCost
    # else:
    #     if currentNode.minCost is None:
    #         moveCost = neighbor.cost * 1.4
    #     else:
    #         moveCost = currentNode.minCost + neighbor.cost * 1.4
    # if neighbor.prevDirection is not None and neighbor.prevDirection != direction:
    #     moveCost += 5.0
    if moveCost < ROUTE_COST_THRESHOLD:
        if neighbor.minCost is None or neighbor.minCost > moveCost:
            neighbor.step = currentNode.step + 1
            neighbor.minCost = moveCost
            neighbor.minCostPrevNode = currentNode.address
            neighbor.prevDirection = direction
            updateFlag = True

    return updateFlag, neighbor


# pathの末端のセルから隣接するセル(neighborAdd)へ移動する際の計算を行う
#   移動先がgoal　-> pathをgoalPathに追加
#   移動先がobstacke/shelf/自身が以前通ったnode(ループが発生している) -> 移動できない(新しいpathとして追加しない)
#   移動先が既に他のpathが通過している
#       -> 他のpathがこのnodeを通った時のコストより大きい -> 移動しない(新しいpathとして追加しない)
#       -> ~~のコストより小さい -> 新しいpathとして追加
#   移動先はまだどのpathも通ったことのないnodeである -> 新しいpathとして追加
# 返り値は移動先がgoalの場合はTrue それ以外はFalse
def stepNewNode(gridMap, goal, currentNode, neighborAdd, direction, step):
    # print("neighbor ", neighborAdd[1],neighborAdd[0])
    neighbor = gridMap[neighborAdd[1]][neighborAdd[0]]
    # print("neighbor ", neighbor.address, "/pos ", neighbor.posx, neighbor.posy)

    if neighbor.step is None:
        return updatePath(currentNode, neighbor, direction)
        # return True, neighbor

    # 移動先がgoalの場合
    if neighbor.address == goal.address:
        # print("            goal!!")
        return updatePath(currentNode, neighbor, direction)
        # goalPath.append(newPath)

        # print("Find path! cost is ", neighbor.minCost, ":  step is ", neighbor.step)
        # return True, neighbor

    elif step > neighbor.step:
        return False, neighbor
    else:
        return updatePath(currentNode, neighbor, direction)
        # return True, neighbor

    # 移動先がobstacle/shelf/自身が以前通ったNode
    # elif neighbor.isObstacle or neighbor.isShelf or path.isPassedPoint(neighbor, gridMap):
    #     return False
    # # 既に他のpathが通過している
    # elif neighbor.isStepped:
    #     newCost = path.routeCost + neighbor.cost
    #     if newCost > ROUTE_COST_THRESHOLD:
    #         return False
    #     if newCost < neighbor.minStepCost:
    #         newPath = updatePath(path, neighbor, direction)
    #         addX = neighbor.address[0]
    #         addY = neighbor.address[1]
    #         gridMap[addY][addX].minStepCost = newCost
    #         gridMap[addY][addX].minPathFromStart = newPath
    #         currentPath.append(newPath)
    #     else :
    #         return False
    # else :
    #     newCost = path.routeCost + neighbor.cost
    #     if newCost > ROUTE_COST_THRESHOLD:
    #         return False
    #     newPath = updatePath(path, neighbor, direction)
    #     addX = neighbor.address[0]
    #     addY = neighbor.address[1]
    #     # print(addX, addY, len(gridMap[0]), len(gridMap))
    #     gridMap[addY][addX].minStepCost = newCost
    #     gridMap[addY][addX].minPathFromStart = newPath
    #     gridMap[addY][addX].isStepped = True
    #     currentPath.append(newPath)
    # return False

# 作成したマップを使って,2地点間の最小コスト経路を探索
# requiredGoalPathNum の本数分探索が完了したら終了
# 通過セルが1つでも違う場合は別の経路として扱う
# 計算上,最速でゴールに到着したpathが最小コストになるとは限らない
#   方向転換によるコスト,斜めへの移動があるため
def pathPlanning(gridMap, startNode, goalNode):
    print("Path Planning... ", startNode, " ", goalNode)
    requiredGoalPathNum = REQUIRED_PATH_NUM
    findStepThreshold = 30
    goalPath = []
    # currentPath = []
    neighborNodeList = []
    # currentPath.append(pathDef.Path(gridMap[startNode[1]][startNode[0]]))
    prevStepNode = []
    tmpList = []
    currentStepNode = []
    nextStepNode = []
    currentStepNode.append(startNode)
    goal = gridMap[goalNode[1]][goalNode[0]]
    goal.isObstacle = False
    prevGoalMinCost = goal.minCost
    # print("goal ", goal)
    lastFindCount = 0
    stepCount = 0
    breakCount = 0

    while(True):
        # 最後にgoalに到達したstepから一定以上新しいgoal pathがみつからない場合探索を終了する
        if lastFindCount != 0 and lastFindCount + findStepThreshold < stepCount:
            # print("goalPath is not enough ...", len(goalPath), " / ", requiredGoalPathNum, " but new path reach the goal has not found in last ", findStepThreshold, " steps.")
            return goal
        # goal pathが一定数見つかった場合　探索を終了する
        # if len(goalPath) >= requiredGoalPathNum:
        #     print("goalPath has reached required number", len(goalPath), " and stop search path")
        #     return goalPath
        # pathを探索しきった場合(探索中のpathがなくなった場合)　探索を終了する
        if breakCount > findStepThreshold or len(currentStepNode) == 0:
            print("ERROR : route could not find. Is the goal Node is in any obstacle ?")
            return goal
        tmpList = copy.deepcopy(currentStepNode)
        # currentStepNode.extend(prevStepNode)
        # prevPath = copy.deepcopy(currentPath)
        # currentPath.clear()
        # print("In ",stepCount," step, there are ",len(prevPath)," route. ", lastFindCount)
        # stepCount += 1
        # prevStepNode = copy.deepcopy(currentStepNode)

        # 探索部分
        # for path in prevPath:
            # print("currentNode ", path.edgeNode.address, "/pos ", path.edgeNode.posx, path.edgeNode.posy)
            # neighborNodeList.clear()
        for address in currentStepNode:
            edgeNode = gridMap[address[1]][address[0]]
            if edgeNode.neighborUp is not None:
                updateFlag, neighbor = stepNewNode(gridMap, goal, edgeNode, edgeNode.neighborUp, 'up',stepCount)
                if updateFlag:
                    nextStepNode.append(neighbor.address)
            if edgeNode.neighborDown is not None:
                updateFlag, neighbor = stepNewNode(gridMap, goal, edgeNode, edgeNode.neighborDown, 'down',stepCount)
                if updateFlag:
                    nextStepNode.append(neighbor.address)
            if edgeNode.neighborRight is not None:
                updateFlag, neighbor = stepNewNode(gridMap, goal, edgeNode, edgeNode.neighborRight, 'right',stepCount)
                if updateFlag:
                    nextStepNode.append(neighbor.address)
            if edgeNode.neighborLeft is not None:
                updateFlag, neighbor = stepNewNode(gridMap, goal, edgeNode, edgeNode.neighborLeft, 'left',stepCount)
                if updateFlag:
                    nextStepNode.append(neighbor.address)

            if ALLOW_DIAGONAL_MOVE:
                if edgeNode.neighborRightUp is not None:
                    updateFlag, neighbor = stepNewNode(gridMap, goal, edgeNode, edgeNode.neighborRightUp, 'rightUp',stepCount)
                    if updateFlag:
                        nextStepNode.append(neighbor.address)
                if edgeNode.neighborRightDown is not None:
                    updateFlag, neighbor = stepNewNode(gridMap, goal, edgeNode, edgeNode.neighborRightDown, 'rightDown', stepCount)
                    if updateFlag:
                        nextStepNode.append(neighbor.address)
                if edgeNode.neighborLeftUp is not None:
                    updateFlag, neighbor = stepNewNode(gridMap, goal, edgeNode, edgeNode.neighborLeftUp, 'leftUp',stepCount)
                    if updateFlag:
                        nextStepNode.append(neighbor.address)
                if edgeNode.neighborLeftDown is not None:
                    updateFlag, neighbor = stepNewNode(gridMap, goal, edgeNode, edgeNode.neighborLeftDown, 'leftDown',stepCount)
                    if updateFlag:
                        nextStepNode.append(neighbor.address)

        if len(tmpList) == len(nextStepNode):
            breakCount += 1
        prevStepNode = copy.deepcopy(tmpList)
        currentStepNode = copy.deepcopy(nextStepNode)
        print("In ", stepCount," step, there are ", len(currentStepNode))
        stepCount += 1
        if prevGoalMinCost != goal.minCost:
            prevGoalMinCost = goal.minCost
            lastFindCount = stepCount
    return goal


def crossObstacle(startNode, targetNode, gridMap, obstacleList):
    startPos = np.array((startNode.posx, startNode.posy))
    targetPos = np.array((targetNode.posx, targetNode.posy))
    targetLine = startPos - targetPos
    for (x1, x2, y1, y2) in obstacleList:
        point1 = np.array((x1,y1))
        point2 = np.array((x1,y2))
        point3 = np.array((x2,y2))
        point4 = np.array((x2,y1))
        line1 = point2 - point1
        line2 = point3 - point2
        line3 = point4 - point3
        line4 = point1 - point4

        if np.cross(targetLine, point1 - startPos) * np.cross(targetLine, point2 -startPos) <= 0:
            # print("1-true", startPos, targetPos, x1,x2,y1,y2)
            if np.cross(line1, startPos - point1) * np.cross(line1, targetPos - point1) <= 0:
                return True
        if np.cross(targetLine, point2 - startPos) * np.cross(targetLine, point3 -startPos) <= 0:
            # print("2-true", startPos, targetPos, x1,x2,y1,y2)
            if np.cross(line2, startPos - point2) * np.cross(line2, targetPos - point2) <= 0:
                return True
        if np.cross(targetLine, point3 - startPos) * np.cross(targetLine, point4 -startPos) <= 0:
            # print("3-true", startPos, targetPos, x1,x2,y1,y2)
            if np.cross(line3, startPos - point3) * np.cross(line3, targetPos - point3) <= 0:
                return True
        if np.cross(targetLine, point4 - startPos) * np.cross(targetLine, point1 -startPos) <= 0:
            # print("4-true", startPos, targetPos, x1,x2,y1,y2)
            if np.cross(line4, startPos - point4) * np.cross(line4, targetPos - point4) <= 0:
                return True

        # cross1 = np.cross(targetLine, point1)
        # cross2 = np.cross(targetLine, point2)
        # cross3 = np.cross(targetLine, point3)
        # cross4 = np.cross(targetLine, point4)
        # line1 = point1 - point2
        # line2 = point2 - point3
        # line3 = point3 - point4
        # line4 = point4 - point1
        #
        # if cross1*cross2 <=0:
        #     # print("1-true", startPos, targetPos, x1,x2,y1,y2)
        #     if np.cross(line1, startPos)*np.cross(line1, targetPos) <=0:
        #         # print("    1-1-true")
        #         return True
        # if cross2*cross3 <=0:
        #     # print("2-true", startPos, targetPos, x1,x2,y1,y2)
        #     if np.cross(line2, startPos)*np.cross(line2, targetPos) <=0:
        #         # print("    2-1-true")
        #         return True
        # if cross3*cross4 <=0:
        #     # print("3-true", startPos, targetPos, x1,x2,y1,y2)
        #     if np.cross(line3, startPos)*np.cross(line3, targetPos) <=0:
        #         # print("    3-1-true")
        #         return True
        # if cross4*cross1 <=0:
        #     # print("4-true", startPos, targetPos, x1,x2,y1,y2)
        #     if np.cross(line4, startPos)*np.cross(line4, targetPos) <=0:
        #         # print("    4-1-true")
        #         return True
        # if cross1*cross2 <= 0 or cross2*cross3 <= 0 or cross3*cross4 <= 0 or cross4*cross1 <= 0:
        #     line1 = point1 - point2
        #     line2 = point2 - point3
        #     line3 = point3 - point4
        #     line4 = point4 - point1
        #     print("hoge")
        #     if np.cross(line1, startPos)*np.cross(line1, targetPos) <= 0:
        #         return True
        #     if np.cross(line2, startPos)*np.cross(line2, targetPos) <= 0:
        #         return True
        #     if np.cross(line3, startPos)*np.cross(line3, targetPos) <= 0:
        #         return True
        #     if np.cross(line4, startPos)*np.cross(line4, targetPos) <= 0:
        #         return True
        #     print("hogehoge")
            # print("    crossed obstacles, ", startPos, targetPos, x1,x2,y1,y2,"\n        ",cross1, cross2, cross3, cross4)
    # print("false")
    return False

# minCostPath.append([currentNode.address, (currentNode.posx, currentNode.posy), currentNode.minCost, currentNode.prevDirection])

def smoothing(path, gridMap, obstacleList):
    print("Begin smoothing... length of path is ",len(path), ", number of obstacles is ", len(obstacleList))
    smoothPath = copy.deepcopy(path)
    goalNode = gridMap[smoothPath[-1][0][1]][smoothPath[-1][0][0]]
    startNode = gridMap[smoothPath[0][0][1]][smoothPath[0][0][0]]
    tmpPath = []
    tmpPath.append([startNode.address, (startNode.posx, startNode.posy), startNode.minCost, startNode.prevDirection])
    startNode = gridMap[tmpPath[0][0][1]][tmpPath[0][0][0]]
    loopFlag = True
    while(loopFlag):
        prevTargetNode = None
        for address in smoothPath:
            targetNode = gridMap[address[0][1]][address[0][0]]
            print(startNode.step, targetNode.step, goalNode.step)
            # print(startNode.step, targetNode.step)
            if targetNode.step <= startNode.step:
                # print("continue")
                continue
            if not crossObstacle(startNode, targetNode, gridMap, obstacleList):
                if targetNode.step == goalNode.step:
                    tmpPath.append([targetNode.address, (targetNode.posx, targetNode.posy), targetNode.minCost, targetNode.prevDirection])
                    loopFlag = False
                    print("targetNode is goal, break")
                    break
                else:
                    prevTargetNode = targetNode
                    print("prevNode is renewed and continue")
                    continue
            else:
                print("cross!!", startNode.step, targetNode.step)
                if targetNode.step == goalNode.step:
                    if prevTargetNode is not None:
                        tmpPath.append([prevTargetNode.address, (prevTargetNode.posx, prevTargetNode.posy), prevTargetNode.minCost, prevTargetNode.prevDirection])
                    tmpPath.append([targetNode.address, (targetNode.posx, targetNode.posy), targetNode.minCost, targetNode.prevDirection])
                    loopFlag = False
                    print("crossObstacle is True, and targetNode is goal, break")
                    break
                elif prevTargetNode is None:
                    tmpPath.append([targetNode.address, (targetNode.posx, targetNode.posy), targetNode.minCost, targetNode.prevDirection])
                else:
                    tmpPath.append([prevTargetNode.address, (prevTargetNode.posx, prevTargetNode.posy), prevTargetNode.minCost, prevTargetNode.prevDirection])
                startNode = copy.deepcopy(targetNode)
                print("startNode is renewed, break")
                break

    # loopFlag1 = True
    # while(loopFlag1):
    #     startNode = gridMap[smoothPath[0][0][1]][smoothPath[0][0][0]]
    #     tmpPath = []
    #     tmpPath.append([startNode.address, (startNode.posx, startNode.posy), startNode.minCost, startNode.prevDirection])
    #     loopFlag2 = True
    #     while(loopFlag2):
    #         prevTargetNode = None
    #         for address in smoothPath:
    #             targetNode = gridMap[address[0][1]][address[0][0]]
    #             # print(startNode.step, targetNode.step)
    #             if targetNode == startNode or targetNode.step < startNode.step:
    #                 # print("continue")
    #                 continue
    #             if not crossObstacle(startNode, targetNode, gridMap, obstacleList):
    #                 if targetNode == goalNode:
    #                     tmpPath.append([targetNode.address, (targetNode.posx, targetNode.posy), targetNode.minCost, targetNode.prevDirection])
    #                     loopFlag2 = False
    #                     print("targetNode is goal, break")
    #                     break
    #                 else:
    #                     prevTargetNode = targetNode
    #                     print("prevNode is renewed and continue")
    #                     continue
    #             else:
    #                 print("cross!!", startNode.step, targetNode.step)
    #                 if targetNode == goalNode:
    #                     if prevTargetNode is not None:
    #                         tmpPath.append([prevTargetNode.address, (prevTargetNode.posx, prevTargetNode.posy), prevTargetNode.minCost, prevTargetNode.prevDirection])
    #                     tmpPath.append([targetNode.address, (targetNode.posx, targetNode.posy), targetNode.minCost, targetNode.prevDirection])
    #                     loopFlag2 = False
    #                     print("crossObstacle is True, and targetNode is goal, break")
    #                     break
    #                 elif prevTargetNode is None:
    #                     tmpPath.append([targetNode.address, (targetNode.posx, targetNode.posy), targetNode.minCost, targetNode.prevDirection])
    #                 else:
    #                     tmpPath.append([prevTargetNode.address, (prevTargetNode.posx, prevTargetNode.posy), prevTargetNode.minCost, prevTargetNode.prevDirection])
    #                 startNode = targetNode
    #                 print("startNode is renewed, break")
    #                 break
    #     if len(smoothPath) == len(tmpPath):
    #         loopFlag1 = False
    smoothPath = copy.deepcopy(tmpPath)
    print("Finish smoothing ", len(path)," -> ", len(smoothPath))
    return smoothPath


def run(startPoint, goalPoint, startShelfID, endShelfID, displayFlag):
    mapInfoList = []
    obstacles = []
    shelf = []

    # # pickleファイルからオブジェクトを復元
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    print("mapSize...", xlim,ylim)

    # 別であらかじめ作成しておいたgridMapをロード
    # makeOccupancyGrid.py
    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
    # print(gridMap)
    cellSize = gridMap[0][1].posx - gridMap[0][0].posx
    print("cellSize", cellSize)

    # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
    obstacleList = []
    obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
    obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
    print("obstacles...",len(obstacleList))


    startNode = searchNodeFromPoint(gridMap, startPoint, cellSize)
    gridMap[startNode[1]][startNode[0]].step = 0
    goalNode = searchNodeFromPoint(gridMap, goalPoint, cellSize)
    # print(goalNode)

    minCostPath = []

    if startNode == goalNode:
        currentNode = gridMap[goalNode[1]][goalNode[0]]
        minCostPath.append([currentNode.address, (currentNode.posx, currentNode.posy), 0, currentNode.prevDirection])

    else:
        goal = pathPlanning(gridMap, startNode, goalNode)
        if goal.minCostPrevNode is None:
            print("Could not find path to ", goalPoint, " from ", startPoint, ". \nSo exit this program")
            sys.exit()

        currentNode = goal
        while(True):
            minCostPath.append([currentNode.address, (currentNode.posx, currentNode.posy), currentNode.minCost, currentNode.prevDirection])
            print(currentNode.address, currentNode.minCost, currentNode.step)
            if currentNode.minCostPrevNode is not None:
                print("     ", currentNode.minCostPrevNode, gridMap[currentNode.minCostPrevNode[1]][currentNode.minCostPrevNode[0]].address)
                currentNode = gridMap[currentNode.minCostPrevNode[1]][currentNode.minCostPrevNode[0]]
            else :
                break
        minCostPath.reverse()
        # minCostPath = smoothing(minCostPath, gridMap, obstacleList)



    if not os.path.isdir(os.path.join(DATA_PATH, "path")):
        os.mkdir(os.path.join(DATA_PATH, "path"))
    writeName = "path/path_"+ str(startPoint[0])+","+str(startPoint[1])+"_"+str(goalPoint[0])+","+str(goalPoint[1])+"_"+startShelfID+","+endShelfID
    with open(os.path.join(DATA_PATH, writeName + ".txt"), 'w') as f:
        prevDirection = None
        for node in minCostPath:
            if prevDirection is None or prevDirection == node[3]:
                turnFlag = "False"
            else :
                turnFlag = "True"
            prevDirection = node[3]
            f.write(str(node[0][0])+","+str(node[0][1])+","+str(node[1][0])+","+str(node[1][1])+","+str(node[2])+","+turnFlag+"\n")
        # totalCost = 0.
        # for node in path[0].route:
        #     totalCost += node.cost
        #     f.write("("+str(node.address[0])+","+str(node.address[1])+")  "+str(node.isObstacle)+"  ("+str(node.posx)+","+str(node.posy)+") "+str(totalCost)+"\n")
        #
        # for address in path[0].route:
        #     print(address)
        #     node = gridMap[address[0][1]][address[0][0]]
        #     totalCost += node.cost
        #     turnFlag = "True"if address[1]else"False"
        #     f.write(str(node.address[0])+","+str(node.address[1])+","+str(node.posx)+","+str(node.posy)+","+str(totalCost)+","+turnFlag+"\n")
            # f.write("("+str(node.address[0])+","+str(node.address[1])+")  "+str(node.isObstacle)+"  ("+str(node.posx)+","+str(node.posy)+") "+str(totalCost)+"\n")
    print("write min cost path to ", os.path.join(DATA_PATH, writeName + ".txt"))
    with open(os.path.join(DATA_PATH, writeName + ".pickle") ,mode = 'wb') as f:
    	pickle.dump(minCostPath, f)

    if displayFlag:
        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
        ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])


        for (x1, x2, y1, y2) in obstacleList:
            plt.plot([x1,x2],[y1,y1] , color = '#000000')
            plt.plot([x2,x2],[y1,y2] , color = '#000000')
            plt.plot([x2,x1],[y2,y2] , color = '#000000')
            plt.plot([x1,x1],[y2,y1] , color = '#000000')

        # コストの低いpath上位5ルートまでをマップに表示
        # colorList = ['#a1a900','#e4007f', '#2ca9e1', '#07d34f', '#c92b29']
        # for j in range(len(path) if len(path) < 5 else 5):
        for (i,address) in enumerate(minCostPath):
            node = gridMap[address[0][1]][address[0][0]]
            if i == 0:
                # print("Red path is ", path[j].routeCost, " in ", len(path[j].route) -1, " step\n")
                prevNode = node
                continue
            else:
                plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#a1a900')
                prevNode = node

        plt.show()

    return writeName + ".txt"

if __name__ == '__main__':
    startPoint = (43.3,46.6)
    goalPoint = (48.926,17.01)
    # goalPoint = (85.,30.)
    # cellSize = 0.5
    # 表示切替用のフラグ
    displayFlag = True
    startShelfID = "100"
    endShelfID = "200"
    run(startPoint, goalPoint, startShelfID, endShelfID, displayFlag)
