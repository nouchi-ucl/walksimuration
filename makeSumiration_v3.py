#!/usr/bin/env python
import rvo2
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os
import pickle
import sys

import searchShelf
import configDef
import fileIO
from classdef import compassModelDef


def run(conf, drawFlag, runList, animationFlag):
    dataPath = conf.simDataPath
    picklePath = os.path.join(dataPath, "pickle")
    STEP_TIME = conf.simStepTime

    # simulation環境の設定
    # float timeStep, float neighborDist, size_t maxNeighbors,float timeHorizon,
    # float timeHorizonObst, float radius,float maxSpeed, tuple velocity=(0, 0)
    # velocityは初期化しない(0,0)で初期化される
    # agent追加時にこれとは別に個別に初期化可能(設定しなければここで設定した値になる)
    sim = rvo2.PyRVOSimulator(STEP_TIME, conf.neighborDist, conf.maxNeighbors,
                        conf.timeHorizon, conf.timeHorizonObst, conf.radius, conf.maxSpeed)

    # Pass either just the position (the other parameters then use
    # the default values passed to the PyRVOSimulator constructor),
    # or pass all available parameters.
    agents = []
    agentDest = []
    writeFileNameList = []
    userNumList = runList

    # map情報のリスト
    mapInfoList = []
    #  読み込み用リスト 棚
    shelfList = []
    #  読み込み用リスト 障害物
    obstacleList = []

    wayPointList = []
    destination = []

    # agentの中心座標/左右の足の座標
    agentsPosx = []
    agentsPosy = []
    agentsRightPosx = []
    agentsRightPosy = []
    agentsLeftPosx = []
    agentsLeftPosy = []

    # シミュレーション上での経過ステップ時間
    step = 0
    # シミュレーション上での経過実時間
    simTime = 0
    # 一つ前のステップのシミュレーション上での経過実時間
    prevSimTime = 0

    # 表示用リスト 棚/障害物
    obstacles = []

    # pickleファイルからオブジェクトを復元
    with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)
    print (mapInfoList)
    xlim = mapInfoList[0]
    ylim = mapInfoList[1]

    with open(os.path.join(picklePath, 'obstacle.pickle'),mode = 'rb') as f:
        obstacleList = pickle.load(f)

    with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'rb') as f:
        shelfList = pickle.load(f)

    agentList = []
    for userNum in userNumList:
        with open(os.path.join(dataPath, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'rb') as f:
            wayPointList = pickle.load(f)
        memoryFlg = True

        # startPoint = conf.startPoint
        startPoint = (float(wayPointList[0][2][0]), float(wayPointList[0][2][1]))

        print(os.path.join(dataPath, "occupancyPath/wayPointList_"+str(userNum)+".pickle"))
        print("wayPointList\n")
        count = 0
        for item in wayPointList:
            if item[0][1] == 130:
                item[0] = (item[0][0], 131)
            if item[4] == "Pass":
                count += 1
            print(item)
        print (count)
        pathList = fileIO.loadPassList(os.path.join(dataPath,"wms/pathList"+str(userNum)+".csv"))
        print(os.path.join(dataPath,"wms/pathList"+str(userNum)+".csv"))
        print (len(pathList))
        # RVO2用のagentの追加
        # 返り値はsim上でのagentのID(int 0~)
        # agentNo = agents.append(sim.addAgent(startPoint))
        if conf.confType == "ipin":
            print(pathList)
            agentNo = sim.addAgent((float(pathList[0][0]),float(pathList[0][1])))
        else:
            agentNo = sim.addAgent(startPoint)
        # agentNo = sim.addAgent(startPoint)

        print("agentNo :",agentNo)
        print("agent velocity:", sim.getAgentVelocity(agentNo), type(sim.getAgentVelocity(agentNo)))
        # 自分で設定したモデル用のagentの追加
        # agentNo direction, 初期速度, 最大速度, position, stepTime, wayPointList, destPos, memoryFlg の初期設定
        # memoryFlg : trueでそのagentの移動軌跡を保存する falseではstepの更新のたびにそれ以前の状態は破棄される
        newAgent = compassModelDef.compass(agentNo, 0., conf.initVelocity, conf.maxSpeed, startPoint, conf.simStepTime, wayPointList, memoryFlg)
        newAgent.memoryCurrentStep(step, simTime, prevSimTime)
        agentList.append(newAgent)
        print(wayPointList)

    if drawFlag:
        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=((xlim[1]+20)/10,(ylim[1]+20)/10))
        ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])
        objs = []

        # 出力の背景に会場のマップを入れる
        # img = plt.imread(os.path.join(dataPath, conf.imgMap))
        # ax1.imshow(img, extent=[xlim[0], xlim[1], ylim[0], ylim[1]], aspect=1)


    # マップ外周を障害物として追加
    obstacles.append(sim.addObstacle([(xlim[0],ylim[0]),(xlim[1],ylim[0]),(xlim[1],ylim[1]),(xlim[0],ylim[1])]))
    # obstacleを障害物として追加
    obstacles.append([sim.addObstacle([(o[0],o[2]),(o[1],o[2]),(o[1],o[3]),(o[0],o[3])]) for o in obstacleList])
    obstacles.append([sim.addObstacle([(s[1],s[3]),(s[2],s[3]),(s[2],s[4]),(s[1],s[4])]) for s in shelfList])
    # これ以降は障害物の追加をしても反映されないので注意
    sim.processObstacles()

    print('Simulation has %i agents and %i obstacle vertices in it.' %
          (sim.getNumAgents(), sim.getNumObstacleVertices()))

    print('Running simulation')
    stopCount = 0
    loopFlag = True
    prevPos = None
    while(loopFlag):

        # 目的地点に十分近い場合は目標の更新・そうでない場合は速度の更新を行う
        for agent in agentList:
            agent.setvPref(step, conf)
            # print("agent :", agent.agentNo, type(agent.agentNo))
            # print("vPref :", agent.vPref, type(agent.vPref))
            sim.setAgentPrefVelocity(agent.agentNo, agent.vPref)

        # 全てのagentに対してdestPosがない(次の目標地点がない)場合はシミュレーションを終了
        loopFlag = False
        for agent in agentList:
            if not agent.arrivalFlg:
                loopFlag = True

        # 各agentの位置の格納
        # マップの描画のために一度xとyを分けている
        agentsPosx.extend([agent.centerPos[0] for agent in agentList])
        agentsPosy.extend([agent.centerPos[1] for agent in agentList])
        agentsPos  = [(posx, posy) for (posx, posy) in zip(agentsPosx, agentsPosy)]

        agentsRightPosx.extend([agent.rightFootPos[0] for agent in agentList])
        agentsRightPosy.extend([agent.rightFootPos[1] for agent in agentList])
        agentsLeftPosx.extend([agent.leftFootPos[0] for agent in agentList])
        agentsLeftPosy.extend([agent.leftFootPos[1] for agent in agentList])

        agentsDestPosx = [agent.destPos[0] for agent in agentList]
        agentsDestPosy = [agent.destPos[1] for agent in agentList]
        agentsDestPos  = [(posx, posy) for (posx, posy) in zip(agentsDestPosx, agentsDestPosy)]

        for agent in agentList:
            agent.memoryArrivalStep(step, sim.getGlobalTime())

        moveFlg = False
        # 全てのagentが動いているかどうかの確認
        # 一つでも動いていたらTrueに
        for agent in agentList:
            if not agent.isDontMove():
                moveFlg = True
                print("agent is moving")
                break

        # 小規模なテストを行う場合有効に，指定ステップ以上ば強制的に止まる
        # if step > 200:
        #     moveFlg = False

        # Flg = True : stopCountをリセット
        if moveFlg:
            stopCount = 0
        # Flg = False : stopCountを +1 ，その後一定時間止まったままならシミュレーションを終了
        else:
            stopCount += 1
            if stopCount > conf.allowStopTime / STEP_TIME:
                print("There was no movement for ", conf.allowStopTime, "sec. Finish simulation")
                break

        print("\nIn %i step : " % step)
        for agent in agentList:
            posx = agent.centerPos[0]
            posy = agent.centerPos[1]
            destx = agent.destPos[0]
            desty = agent.destPos[1]
            destState = agent.currentDest[-1]
            velx = agent.velocity[0]
            vely = agent.velocity[1]
            direction = agent.direction
            print("    Agent%i pos:(%.3f,%.3f) dest:(%.3f,%.3f) %s velocity:(%.3f,%.3f), direction:%.3f"
                    %(agent.agentNo,posx,posy,destx,desty,destState,velx,vely,direction))


        if drawFlag and animationFlag:
            pt1 = ax1.scatter(agentsPosx,agentsPosy,color = '#c92b29', s=5)
            # pt2 = ax1.scatter(agentsDestPosx,agentsDestPosy,color = '#2ca9e1', s=5)

            pt3 = ax1.scatter(agentsRightPosx,agentsRightPosy,color = '#a1a900', s=3)
            pt4 = ax1.scatter(agentsLeftPosx,agentsLeftPosy,color = '#07d34f', s=3)

            # if 1 < len(agentsPos):
            print("pos", agentsPosx[-1], len(agentsPosx))
            # pt5 = ax1.plot([agentsPosx[-1],agentsRightPosx[-1]], [agentsPosy[-1], agentsRightPosy[-1]], color = '#c92b29')
            # pt6 = ax1.plot([agentsPosx[-1],agentsLeftPosx[-1]], [agentsPosy[-1], agentsLeftPosy[-1]], color = '#c92b29')

            # agentsPosx[-1] + 2 * np.cos(agent.direction)
            # pt7 = ax1.plot([agentsPosx[-1], agentsPosx[-1] + 1/2 * np.cos(agent.direction)],[agentsPosy[-1], agentsPosy[-1] + 1/2 * np.sin(agent.direction)], color = '#2ca9e1')

            # pt8 = ax1.plot([agentList[-1].rightFootPos[0],agentList[-1].prevRightFootPos[0]],[agentList[-1].rightFootPos[1],agentList[-1].prevRightFootPos[1]], color = "#a1a900")
            # pt9 = ax1.plot([agentList[-1].leftFootPos[0],agentList[-1].prevLeftFootPos[0]],[agentList[-1].leftFootPos[1],agentList[-1].prevLeftFootPos[1]], color = "#07d34f")
            # pt10 = ax1.plot([agentList[-1].centerPos[0],agentList[-1].prevCenterPos[0]],[agentList[-1].centerPos[1],agentList[-1].prevCenterPos[1]], color = "#444444")

            objs.append([pt1,pt3,pt4])
            # objs.append([pt1,pt3,pt4,pt5,pt6,pt8,pt9,pt10])
            # print("draw... :", agentsPosx,agentsPosy)


        # RVO2の1ステップ更新
        sim.doStep()
        step += 1

        prevSimTime = simTime
        simTime = sim.getGlobalTime()

        # RVO2の中では1ステップ時間が進んでいるが，agentクラスの中にはまだ反映されていない
        # 各agent内を最新ステップの情報に更新すると同時に状態の記録も行う．
        for agent in agentList:
            # agentの向いている方向の再計算のためにここでもsetvPrefを行う
            agent.setvPref(step, conf)
            # agentの足の位置の計算
            agent.updateAgentState(sim, step, simTime, prevSimTime)
            # agentの中心位置をsimの方に反映し直す
            sim.setAgentPosition(agent.agentNo, agent.centerPos)

    if drawFlag:
        if animationFlag:
            interval = 100 * STEP_TIME
            ani = animation.ArtistAnimation(fig, objs, interval = interval, repeat = True)
        else :
            pt1 = ax1.scatter(agentsPosx,agentsPosy,color = '#c92b29', s=5)
            # pt2 = ax1.scatter(agentsDestPosx,agentsDestPosy,color = '#2ca9e1', s=5)

            pt3 = ax1.scatter(agentsRightPosx,agentsRightPosy,color = '#a1a900', s=3)
            pt4 = ax1.scatter(agentsLeftPosx,agentsLeftPosy,color = '#07d34f', s=3)

        plt.show()

    recordPath = os.path.join(dataPath, "record/")
    if not os.path.isdir(recordPath):
        os.mkdir(recordPath)
    for agent in agentList:
        writefname = os.path.join(dataPath, "txtresult/trajectory"+str(runList[agent.agentNo])+".txt")
        agent.writeTrajectory2File(writefname)
        with open(os.path.join(recordPath, "record_"+str(runList[agent.agentNo])+".pickle"), "wb") as f:
            # pickle.dump(agent)
            pickle.dump(agent.trajectory,f)
        print("save to %s" %writefname)

if __name__ == '__main__':
    conf = configDef.Config()
    drawFlag = False
    RUN_LIST = [0]
    run(conf, drawFlag, RUN_LIST, False)
