#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math
import copy
import numpy as np
import os
import matplotlib.pyplot as plt
import pickle
import itertools

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef_v2
from classdef import pathDef
from classdef import bleDef
from classdef import gridMapDef

import commonCalc
import configDef


# gridMapの生成全般
def makeMap(xlim, ylim, cellSize, obstacleList, shelfList, bleList):
    print("initialize map...\n")
    gridMap = gridMapDef.GridMap(xlim, ylim, cellSize)
    print("add obstacle on map...\n", len(obstacleList))
    gridMap.addObstacle(obstacleList)
    print("add shelf on map...\n", len(shelfList))
    gridMap.addShelf(shelfList)
    gridMap.addBLE(bleList)
    gridMap.addCost()

    return gridMap

def run(conf):
    dataPath = conf.dataPath
    obstacleCost = conf.obstacleCost
    # NEIGHBOR_obstacleCost = conf.neighborObstacleCost
    picklePath = os.path.join(dataPath, "pickle")
    cellSize = conf.cellSize

    mapInfoList = []
    obstacles = []
    shelf = []
    ble = []

    # pickleファイルからオブジェクトを復元
    # mapの外周の情報
    with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)
    # 障害物の情報
    with open(os.path.join(picklePath, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    # 棚の情報(ここでは障害物と同じ扱い)
    if conf.useShelf:
        with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'rb') as f:
            shelf = pickle.load(f)
    if conf.useBLE:
        with open(os.path.join(picklePath, 'ble.pickle'), mode = 'rb') as f:
            ble = pickle.load(f)

    xlim = mapInfoList[0]
    ylim = mapInfoList[1]
    print("mapSize...", xlim,ylim)

    # 棚と障害物を統合(位置情報しか使わないので統合できる)
    # 外周の壁も障害物として追加
    obstacleList = []
    obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
    obstacleList.extend([(o[1],o[2],o[3],o[4]) for o in shelf])

    shelfList = []
    shelfList.extend([(o[1],o[2],o[3],o[4]) for o in shelf])
    # 障害物情報の追加
    # 基本的に隙間ができてしまっている部分の手動での穴埋め
    obstacleList.extend(conf.extendObstList)

    # 外壁分のobstacleの追加
    # obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
    print("obstacles...",len(obstacleList))

    # マップの作成
    gridMap = makeMap(xlim, ylim, cellSize, obstacleList, shelfList, ble)

    # pickleファイルとして書き出す
    print("save map (pickle object file) as ", os.path.join(picklePath, "gridMap.pickle"),"\n")
    with open(os.path.join(picklePath, "gridMap.pickle"),  mode='wb') as f:
        pickle.dump(gridMap, f)

    # 表示用
    # gridMap.drawMap()

if __name__ == '__main__':
    conf = configDef.Config()
    run(conf)
