#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
import csv
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

def run(shelfID, dataPath):
    shelfList = []
    picklePath = os.path.join(dataPath, "pickle")
    with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'rb') as f:
        shelfList = pickle.load(f)

    # 上手くいかないものは決め打ちでやってしまった...
    if int(shelfID) == 1:
        return (54.5, 48.9)
    if int(shelfID) == 291:
        return (36.4, 22.9)
    if int(shelfID) == 318:
        return (65.8, 23.8)
    if int(shelfID) == 437:
        return (62.5, 21.5)
    if int(shelfID) == 451:
        return (60.4, 19.7)
    if int(shelfID) == 455:
        return (62.2, 19.9)
    if int(shelfID) == 474:
        return (41.8, 27.5)
    if int(shelfID) == 642:
        return (64.6, 11.5)
    if int(shelfID) == 646:
        return (62.8, 11.5)
    if int(shelfID) == 699:
        return (69.1, 11.5)
    if int(shelfID) == 715:
        return (51.4, 7.3)
    if int(shelfID) == 717:
        return (51.4, 9.7)
    if int(shelfID) == 723:
        return (51.4, 4.9)
    if int(shelfID) == 725:
        return (34.9, 9.4)
    if int(shelfID) == 735:
        return (34.9, 4.6)
    if int(shelfID) == 765:
        return (34.9, 2.2)
    if int(shelfID) == 1142:
        return (85., 4.6)
    if int(shelfID) == 1164:
        return (99.1, 4.6)
    # print(shelfID)
    for shelf in shelfList:
        if shelf[0] == int(shelfID):
            # print(shelf, shelfID)
            x1 = shelf[1]
            x2 = shelf[2]
            y1 = shelf[3]
            y2 = shelf[4]
            if shelf[5] == "up":
                x = (x1 + x2) /2
                y = y2 + 0.45
                # y = y2 + (y2-y1)/2
            elif shelf[5] == "down":
                x = (x1 + x2) /2
                y = y1 - 0.45
                # y = y1 - (y2-y1)/2
            elif shelf[5] == "right":
                # x = x2 + (x2-x1)/2
                x = x2 + 0.45
                y = (y1 + y2) /2
            elif shelf[5] == "left":
                # x = x1 - (x2-x1)/2
                x = x1 - 0.45
                y = (y1 + y2) /2
            # print("break search shelf")
            break
    print(x1,x2,y1,y2,"\n",x,y, "shelfid", shelfID)
    return (x,y)

if __name__ == '__main__':

    run(739, DATA_PATH)
