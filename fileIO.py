import os
import csv

def loadWms(fname, agentNo, startTime = 0):
    wmsList= []
    with open(fname, 'r') as f:

        wmsData = csv.reader(f)

        # header行を読み飛ばす
        header = next(wmsData)
        next(wmsData)

        for line in wmsData:
            terminalID = line[0]
            shelfNO = line[1]
            utime = int(line[2])

            if int(terminalID) == agentNo:
                if utime < startTime:
                    continue
                wmsList.append([utime, shelfNO, "pick"])
    return wmsList

def loadPassList(fname):
    passList = []
    with open(fname, 'r') as f:
        pList = csv.reader(f)
        # header = next(pList)
        for line in pList:
            posx = float(line[0])
            posy = float(line[1])
            action = line[2]
            if action == "Pass" or action == "Start":
                passList.append([(posx, posy), action])
    return passList

# 通過時間を考慮していた旧バージョン
# def loadPassList(fname):
#     passList = []
#     with open(fname, 'r') as f:
#         pList = csv.reader(f)
#         header = next(pList)
#         for line in pList:
#             utime = float(line[0])
#             posx = float(line[1])
#             posy = float(line[2])
#             action = line[3]
#             if action == "Pass":
#                 passList.append([utime, (posx, posy), action])
#     return passList
