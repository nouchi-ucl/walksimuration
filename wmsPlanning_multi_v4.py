#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
import csv
from multiprocessing import Pool
from multiprocessing import Process
from operator import attrgetter
import numpy as np

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef_v2
from classdef import pathDef
import pathPlanning_v4
import searchShelf

from classdef import gridMapDef
import configDef
import fileIO


# def getStartTime(dataPath, agentNo):
#     agentMoveList = []
#     with open(os.path.join(dataPath, 'labels/createdMoveLabel_mod'+str(agentNo)+'.csv'), mode = 'r') as f:
#         moveData = csv.reader(f)
#         header = next(moveData)
#         header = next(moveData)
#         for line in moveData:
#             # print(line)
#             startTime = float(line[0])
#             endTime = float(line[1])
#             action = line[2]
#             agentMoveList.append([startTime, endTime, action])
#     # リストを時間でソート
#     agentMoveList.sort(key=lambda x:x[0])
#     startTime = agentMoveList[0][0]
#     return startTime

# 実際に並列で処理を回したい部分
def planning(i, conf, prevPos, currentPos, action, listLength):
    writeName = pathPlanning_v4.run(conf, prevPos, currentPos, action, False)
    print("write to ", writeName, "\n     This is ", i, " of ", listLength)
    return (i, writeName)

# multiprocessingで複数の変数を引数として渡す際の処理
# 実はあまり理解できていない(multiprocessingでは直接複数の引数をとることができないらしい)
def wrapper(args):
    inputTapple = (args[0], args[1], args[2], args[3], args[4], args[5])
    return planning(*inputTapple)

def run(conf, userNum, drawFlag):
    dataPath = conf.dataPath
    picklePath = os.path.join(dataPath, "pickle")


    mapInfoList = []
    obstacle = []
    shelf = []

    # 各pickleファイルの読み込み
    with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(picklePath, 'obstacle.pickle'),mode = 'rb') as f:
        obstacle = pickle.load(f)

    if conf.useShelf:
        with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'rb') as f:
            shelf = pickle.load(f)

    xlim = mapInfoList[0]
    ylim = mapInfoList[1]

    with open(os.path.join(picklePath, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)

    cellSize = gridMap.cellSize
    passList = []
    # passList.append([conf.startPoint[0], conf.startPoint[1],"start"])


    # with open(os.path.join(dataPath,"wms/passList0.csv"),"r") as f:
    #     passData = csv.reader(f)
    #     for line in passData:
    #         passList.append([float(line[0]), float(line[1]),"pass"])

    # 通過点のリストを読み込み
    with open(os.path.join(dataPath,conf.passList),"r") as f:
        passData = csv.reader(f)
        if conf.confType =="ipin":
            header = next(passData)
            for line in passData:
                if int(line[0]) == userNum:
                    shelfID = line[1]
                    shelfPos = searchShelf.run(shelfID, dataPath)
                    passList.append([shelfPos[0], shelfPos[1],"pass"])
                    cell = gridMap.searchNodeFromPoint(shelfPos)
                    node = gridMap.gridmap[cell[1]][cell[0]]
                    print("add passList,",shelfID,shelfPos,node.posx,node.posy)
        if conf.confType =="hasc":
            for line in passData:
                passList.append([line[0], line[1],"pass"])
                cell = gridMap.searchNodeFromPoint((line[0], line[1]))
                node = gridMap.gridmap[cell[1]][cell[0]]
                print("add passList,",(line[0], line[1]),node.posx,node.posy)

    startPos = [passList[0][0], passList[0][1]]

    # startPath = [startPos, "pass"]
    prevPath = [startPos[0], startPos[1], "pass"]
    workList = []
    for (i, path) in enumerate(passList):
        # 何らかの理由でprevPathが設定されていない場合(基本的にFalse)
        if prevPath is None:
            prevPath = [path[0], path[1], path[-1]]
            continue
        # currentPosとprevPosの更新
        currentPos = gridMap.nearNonObstCell([path[0], path[1]])
        prevPos = gridMap.nearNonObstCell([prevPath[0], prevPath[1]])
        action = path[-1]
        # ループ処理用にリストを生成
        # i: 何番目のpathなのか(並列処理させるので後からソートする際に必要)
        # actionは現状passのみ，
        workList.append([i, conf, prevPos, currentPos, action, len(passList)])
        prevPath = path

    # 並列実行数はconfで設定されている
    pool = Pool(conf.poolNum)
    # multiprocessingで並列処理をさせる
    # 各実行結果はそれぞれ中間ファイルとして出力されている->ファイル名がwriteNameListに格納されている
    writeNameList = pool.map(wrapper, workList)
    pool.close()


    # リストを経路順で(元のworkListと同じ順になるように)ソート
    writeNameList.sort(key=lambda x:x[0])
    # writeNameList内のファイル名を絶対パスに変更
    fnameList = [os.path.join(dataPath, fname[1]) for fname in writeNameList]

    wayPointList = []
    for fname in fnameList:
        if len(fname.split(".")) < 2:
            continue

        with open(fname.split("\n")[0], 'r') as f:
            # ファイル名にstartPointとendPointが直書きされているので，ファイル名から取得
            startPoint = (os.path.basename(fname)[:-4].split("_")[1].split(","))
            endPoint = (os.path.basename(fname)[:-4].split("_")[2].split(","))
            pathLines = f.readlines()
            prevPos = None
            # ファイル内に入っている経路のうち，分岐点となる地点の情報だけ取り出したい
            for line in pathLines:
                data = line.split("\n")[0].split(",")
                # リストの先頭の際の処理
                if prevPos is None:
                    prevPos = (float(data[2]), float(data[3]))
                # data[-1]が一つ前の状態から進行方向が変化したかを見ている(True:変化した)
                # 進行方向が変化した点 = 分岐点　のみ保存しておけばルーティングは可能
                elif data[-1] == "True":
                    currentCell = (int(data[0]), int(data[1]))
                    currentPos = (float(data[2]), float(data[3]))
                    # currentPos - prevPos間は直進移動をしているはずなので単純なベクトル距離が移動距離
                    dist = np.linalg.norm(np.array(currentPos) - np.array(prevPos))
                    wayPointList.append([currentCell, dist, prevPos, currentPos, "Turn"])
                    prevPos = currentPos
            # 終着点だけは進行方向で判別できない可能性があるので手動入力
            endPoint = (float(endPoint[0]), float(endPoint[1]))
            endCell = gridMap.searchNodeFromPoint(endPoint)
            action = "Pass"
            dist = np.linalg.norm(np.array(endPoint) - np.array(prevPos))
            wayPointList.append([endCell, dist, prevPos, endPoint, action])

    if not os.path.isdir(os.path.join(dataPath, "occupancyPath")):
        os.mkdir(os.path.join(dataPath, "occupancyPath"))
    # txtとpickleで出力
    with open(os.path.join(dataPath, "occupancyPath/pathFileList_"+str(userNum)+".txt"), 'w') as f:
        for fname in fnameList:
            print("\n\nSave file as ",fname)
            f.write(fname+"\n")
    with open(os.path.join(dataPath, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'wb') as f:
        pickle.dump(wayPointList, f)

    print("path of user ", userNum, " is saved to ", os.path.join(dataPath, "occupancyPath/path_"+str(userNum)+".txt"))

    # 以下表示用
    if drawFlag:

        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=((gridMap.xMax-gridMap.xMin+20)/10,(gridMap.yMax-gridMap.yMin+20)/10))
        ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])

        # # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
        # # 単純にpathの探索をするだけなら必要ない
        obstacleList = []
        obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacle])
        obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
        # 外壁の追加
        obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
        print("obstacles...",len(obstacleList))

        for (x1, x2, y1, y2) in obstacleList:
            ax1.add_patch(plt.Rectangle((x1,y1),x2-x1,y2-y1, fc ='#303030', fill = False))

        turnPointX = []
        turnPointY = []
        passPointX = []
        passPointY = []
        pickPointX = []
        pickPointY = []

        # 各パスの描画と方向転換点，ピック点を描画
        for fName in fnameList:
            pickleName = fName[:-3]+"pickle"
            with open(pickleName, mode='rb') as f:
                path = pickle.load(f)

            for (i,address) in enumerate(path):
                node = gridMap.gridmap[address[0][1]][address[0][0]]
                if i == 0:
                    prevNode = node
                    if len(pickleName[:-4].split("_")[-1].split(",")[0].split(" ")) > 1:
                    # print("Red path is ", path[j].routeCost, " in ", len(path[j].route) -1, " step\n")
                        passPointX.append(node.posx)
                        passPointY.append(node.posy)
                    else:
                        pickPointX.append(node.posx)
                        pickPointY.append(node.posy)
                    continue
                else:
                    plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#a1a900')
                    prevNode = node
                    if address[1] == True:
                        turnPointX.append(node.posx)
                        turnPointY.append(node.posy)

        if startPos is not None:
            pt = ax1.scatter(startPos[0], startPos[1], color = '#2ca9e1')
        pt1 = ax1.scatter(turnPointX,turnPointY,color = '#07d34f')
        pt2 = ax1.scatter(pickPointX,pickPointY,color = '#c92b29')
        if len(passPointX) > 0:
            pt3 = ax1.scatter(passPointX, passPointY, color ="#ffd90d")

        plt.show()
        del turnPointX, turnPointY, pickPointX, pickPointY

    del mapInfoList, obstacle, gridMap, passList, workList, wayPointList

if __name__ == '__main__':
    conf = configDef.Config()
    userNum = 4
    # run(DATA_PATH, userNum, True)
    run(conf, userNum, True)
