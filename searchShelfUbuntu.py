#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
import csv
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef

DATA_PATH = os.path.join("/media/sf_Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")

# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

# pointがx1,x2,y1,y2に囲まれた矩型領域内に存在するかどうかの判定
# x1<x2, y1<y2でないといけないので引数に注意
def inRectangle(point,x1,x2,y1,y2):
    x = point[0]
    y = point[1]
    if x1 <= x and x <= x2 and y1 <= y and y <= y2:
        return True
    else:
        return False


def searchShelfFromCoordinate(pos):
    shelfList = []
    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelfList = pickle.load(f)
    shelfID = None
    for shelf in shelfList:
        x1 = shelf[1]
        x2 = shelf[2]
        y1 = shelf[3]
        y2 = shelf[4]
        if inRectangle(pos, x1,x2,y1,y2):
            shelfID = shelf[0]
            break
    print("searchShelf ",shelfID, pos)

    return shelfID


def searchShelfFromID(shelfID, dataPath):
    shelfList = []
    picklePath = os.path.join(dataPath, "pickle")
    with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'rb') as f:
        shelfList = pickle.load(f)

    # print(shelfID)
    for shelf in shelfList:
        # print(int(shelfID))
        if int(shelf[0]) == int(shelfID):
            # print(shelf)
            x1 = shelf[1]
            x2 = shelf[2]
            y1 = shelf[3]
            y2 = shelf[4]
            if shelf[5] == "up":
                x = (x1 + x2) /2
                y = y2 + 0.1
            elif shelf[5] == "down":
                x = (x1 + x2) /2
                y = y1 - 0.1
            elif shelf[5] == "right":
                x = x2 + 0.1
                y = (y1 + y2) /2
            elif shelf[5] == "left":
                x = x1 - 0.1
                y = (y1 + y2) /2
            break
    print("shelfID :", shelfID, "point :", x,y)
    return (x,y)

def run(shelfID):
    searchShelfFromID(shelfID)
