import os
import sys

import configDef

import loadmap
import loadPickingData
import makeOccupancyGrid_v3
import wmsPlanning_multi_v4
import makeSumiration_v3

def run(animationFlag = False):
    # コンフィグの読み込み
    conf = configDef.Config()
    # conf.userList = [1,2,3,4,5,6,7,8]
    conf.userList = [0]

    loadmap.run(conf)
    if conf.confType == "ipin":
        loadPickingData.run(conf)
    makeOccupancyGrid_v3.run(conf)
    print(conf.userList)
    for userNum in conf.userList:
        wmsPlanning_multi_v4.run(conf, conf.userList, False)
    makeSumiration_v3.run(conf, True, conf.userList, animationFlag)

if __name__ == '__main__':
    run()
