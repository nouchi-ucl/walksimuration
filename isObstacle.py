#!/usr/bin/python
# -*- coding: utf-8 -*-

# import wmsPlanning_multi_v2
import os
import csv
import pickle
import pathPlanning_v4

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")


def run(dataPath, gridMap, point, macAdd):
    picklePath = os.path.join(dataPath, "pickle")

    mapInfoList = []
    obstacles = []
    shelf = []

    # # pickleファイルからオブジェクトを復元
    with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    # with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'rb') as f:
    #     shelf = pickle.load(f)
    #
    # with open(os.path.join(picklePath, 'obstacle.pickle'),mode = 'rb') as f:
    #     obstacles = pickle.load(f)
    #
    # xlim = mapInfoList[0][0]
    # ylim = mapInfoList[0][1]
    # print("mapSize...", xlim,ylim)

    # print(os.path.join(picklePath, 'gridMap.pickle'))
    # print(gridMap)
    cellSize = gridMap[0][1].posx - gridMap[0][0].posx
    # print("cellSize", cellSize)

    targetNodeAdd = pathPlanning_v4.searchNodeFromPoint(gridMap, point, cellSize)
    targetNode = gridMap[targetNodeAdd[1]][targetNodeAdd[0]]
    print(targetNode.isObstacle, targetNode.isShelf, macAdd, (targetNode.posx,targetNode.posy))

if __name__ == '__main__':
    # drawFlag = False
        # 別であらかじめ作成しておいたgridMapをロード
        # makeOccupancyGrid.py
    dataPath = DATA_PATH
    picklePath = os.path.join(dataPath, "pickle")

    with open(os.path.join(picklePath, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)

    pointList = []
    with open(os.path.join(DATA_PATH, "csv/BLE_info_mod2.csv"), "r") as f:
        bleData = csv.reader(f)
        header = next(bleData)
        for ble in bleData:
            pointList.append([(float(ble[1])/1000, float(ble[2])/1000), ble[0]])

    # pointList = [(100,50),(50,10)]
    for point in pointList:
        run(DATA_PATH, gridMap, point[0], point[1])
