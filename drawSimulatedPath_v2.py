#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pickle
import itertools
import csv
import numpy as np
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import pathPlanning_v2
import searchShelf
import fileIO

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

# MIN MAX共に-1指定で全体を描画します
DRAW_THRESHOLD_MIN = -1
DRAW_THRESHOLD_MAX = -1

PICK_TIME = 0
DRAW_SIM_PATH = True
DRAW_OPT_PATH = False
DRAW_TURN_POINT = False


def run(agentNo, drawFlag, dataPath):
    # 各pickleファイルの読み込み
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0]
    ylim = mapInfoList[1]
    startPos = (43.3,46.6)


    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
    # print(gridMap)
    # cellSize = gridMap[0][1].posx - gridMap[0][0].posx
    cellSize = gridMap.cellSize

    # with open(os.path.join(DATA_PATH,"wms/WMS_comb_all.csv"), 'r') as f:
    #     wmsData = csv.reader(f)
    #
    #     # header行を読み飛ばす
    #     header = next(wmsData)
    #     next(wmsData)
    #
    #     wmsList = []
    #     for line in wmsData:
    #         # print(line)
    #         terminalID = line[0]
    #         shelfNO = int(line[1])
    #         utime = int(line[2])
    #
    #         if int(terminalID) == agentNo:
    #             # if utime < CUT_TIME[agentNo]:
    #                 # print(shelfNO, utime, "xxxx")
    #                 # continue
    #             wmsList.append([shelfNO, utime])
    #             print(shelfNO, utime)
    #
    # print("length of wmsList...", len(wmsList))
    # # リストを時間でソート
    # wmsList.sort(key=lambda x:x[1])
    #
    # passList = []
    # with open(os.path.join(dataPath,"wms/nearFromBLE"+str(agentNo)+".csv"), 'r') as f:
    #     pList = csv.reader(f)
    #     header = next(pList)
    #     for line in pList:
    #         utime = float(line[0])
    #         posx = float(line[1])
    #         posy = float(line[2])
    #         action = line[3]
    #         if action == "Pass":
    #             passList.append([utime, (posx, posy), action])

    eventList = fileIO.loadPassList(os.path.join(dataPath,"wms/pathList"+str(agentNo)+".csv"))


    # リストを時間でソート
    # wmsList.sort(key=lambda x:x[0])
    # eventList = []
    # eventList.extend(wmsList)
    # eventList.extend(passList)
    eventList.sort(key=lambda x:x[0])
    print("length of eventList...", len(eventList))


# pickleDumpArray[agentNo].append([step, sim.getGlobalTime(), agentsPos[agentNo], agentsDestPos[agentNo], agentsDestAttribute[agentNo]])
    with open(os.path.join(DATA_PATH, 'simulation/'+ str(agentNo) + '_simulatedTrajectory.pickle'), mode = 'rb') as f:
        agentTrajectory = pickle.load(f)

    result = []
    pathLength = 0.
    prevStep = None
    prevTime = 0.
    prevPos = None
    prevState = None
    prevPick = "start"
    prevRecord = None
    for record in agentTrajectory:
        # print(record)
        if prevPos is None:
            prevPos = np.array(record[2])
            prevStep = record[0]
            prevTime = record[1]
            currentState = None
            # prevWmsTime = 0
            prevRecord = record
            continue
        currentPos = np.array(record[2])
        distance = np.linalg.norm(currentPos - prevPos)
        pathLength += distance
        # print(currentPos, prevPos, distance)
        # print(record[-1])
        prevState = currentState
        currentState = record[-1][0]
        # print(currentState)

        if prevState == 'Pick' and currentState == 'Turn':
        # if record[-1][0] == 'Pick':
            print("pick", prevRecord[-1][1])
            step = prevRecord[0]
            time = prevRecord[1]
            diffStep = step - prevStep
            diffTime = time - prevTime
            pick = prevRecord[-1][1]
            # for wms in wmsList:
            #     # print(wms[0], prevRecord[-1][1])
            #     # print("hoge" if wms[0] == int(prevRecord[-1][1]) else "false")
            #     if wms[0] == float(prevRecord[-1][1]):
            #         wmsTime = wms[1]
            #         diffTimeFromStart = wmsTime - CUT_TIME[agentNo]
            #
            #         moveTime = 0
            #         for move in moveList:
            #             if prevWmsTime < int(move[0]) and int(move[0]) <= wmsTime:
            #                 if wmsTime < int(move[1]):
            #                     moveTime += wmsTime - int(move[0])
            #                 else:
            #                     moveTime += int(move[1]) - int(move[0])
            #         print("moveTime", moveTime)
            #         diffWmsTime = wmsTime - prevWmsTime - PICK_TIME
            #         if moveTime == 0:
            #             moveTime = 1
            #         # result.append([prevPick, pick, diffStep, diffWmsTime, pathLength, pathLength/diffWmsTime, diffWmsTime/pathLength, wmsTime, diffTimeFromStart])
            #         # print("prevPick :",prevPick, "pick :",pick, "step :",step, "time :",wmsTime, "\n    pathLength :", pathLength, "diffStep :",diffStep, "diffTime :",diffWmsTime, " length/time :", pathLength/diffWmsTime, "time/length :", diffWmsTime/pathLength)
            #         result.append([prevPick, pick, diffStep, diffWmsTime, moveTime, pathLength, pathLength/moveTime, moveTime/pathLength, wmsTime, diffTimeFromStart])
            #         # result.append([prevPick, pick, moveTime, pathLength, pathLength/moveTime])
            #         print("prevPick :",prevPick, "pick :",pick, "step :",step, "time :",wmsTime, "\n    pathLength :", pathLength, "diffStep :",diffStep, "diffTime :",diffWmsTime, " length/time :", pathLength/diffWmsTime, "time/length :", diffWmsTime/pathLength)
            #
            #         prevWmsTime = wmsTime
            pathLength = 0.
            prevTime = time
            prevStep = step
            prevPick = pick
        prevRecord = record
        prevPos = currentPos

    with open(os.path.join(DATA_PATH, "simulateResult_"+str(agentNo)+".txt"), "w") as f:
        f.write("Terminal ID:"+str(agentNo)+",pickTime:"+str(PICK_TIME)+"\n")
        f.write("prevPick, pick, diffStep, diffTime, moveTime, pathLength, length/time, time/length, time, diffTimeFromStart\n")
        for line in result:
            # print(line)
            f.write(",".join(map(str, line))+"\n")

        # f.write("\n\n\n???\nprevPick, pick, diffWmsTime, moveTime, pathLength, path/moveTime\n")
        # for line in result:
        #     if line[-1] < 0.4:
        #         array = [line[0],line[1],line[3],line[4],line[5],line[6]]
        #         f.write(",".join(map(str, array))+"\n")

    # drawFlag = False
    if drawFlag:


        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=(11,7.5))
        # ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        # ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        ax1.set_xlim(xlim[0]-3,xlim[1]+3)
        ax1.set_ylim(ylim[0]-3,ylim[1]+3)
        # ax1.set_xlim(25,80)
        # ax1.set_ylim(0, 60)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])

        # # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
        # # 単純にpathの探索をするだけなら必要ない
        obstacleList = []
        obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
        obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
        # obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
        print("obstacles...",len(obstacleList))

        for (x1, x2, y1, y2) in obstacleList:
            plt.plot([x1,x2],[y1,y1] , color = '#000000')
            plt.plot([x2,x2],[y1,y2] , color = '#000000')
            plt.plot([x2,x1],[y2,y2] , color = '#000000')
            plt.plot([x1,x1],[y2,y1] , color = '#000000')

        turnPointX = []
        turnPointY = []
        pickPointX = []
        pickPointY = []
        passedPointX = []
        passedPointY = []

        fname = os.path.join(DATA_PATH,"occupancyPath/pathFileList_"+ str(agentNo)+ ".txt")
        pathList = []
        with open(fname, 'r') as f:
            data = f.readlines()
            pathList = [line.split("\n")[0] for line in data]

        # 各パスの描画と方向転換点，ピック点を描画
        if DRAW_THRESHOLD_MIN == -1:
            drawMin = 0
        if DRAW_THRESHOLD_MAX == -1:
            drawMax = len(pathList)
        for fName in pathList[drawMin:drawMax]:
            pickleName = fName[:-3]+"pickle"
            pickleName = os.path.join(DATA_PATH, "path/" +pickleName.split("/")[-1])
            with open(pickleName, mode='rb') as f:
                path = pickle.load(f)

            prevDirection = None
            for (i,address) in enumerate(path):
                node = gridMap.gridmap[address[0][1]][address[0][0]]
                if i == 0:
                    # print("Red path is ", path.routeCost, " in ", len(path) -1, " step\n")
                    prevNode = node
                    prevDirection = address[-1]
                    # pickPointX.append(node.posx)
                    # pickPointY.append(node.posy)
                    continue
                if i == len(path) -1:
                    if len(fName[:-3].split(",")[-1].split(" ")) > 1:
                        passedPointX.append(node.posx)
                        passedPointY.append(node.posy)
                    else:
                        pickPointX.append(node.posx)
                        pickPointY.append(node.posy)
                else:
                    if DRAW_OPT_PATH:
                        plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#07d34f')
                    prevNode = node
                    # print(address)
                    if address[-1] != prevDirection:
                        turnPointX.append(node.posx)
                        turnPointY.append(node.posy)
                    prevDirection = address[-1]
        # if startPos is not None:
            # pt = ax1.scatter(startPos[0], startPos[1], color = '#2ca9e1')


        if DRAW_TURN_POINT:
            pt1 = ax1.scatter(turnPointX,turnPointY,color = '#07d34f',s = 20)
        pt2 = ax1.scatter(passedPointX,passedPointY,color = '#A15DC4', s = 30)
        pt3 = ax1.scatter(pickPointX,pickPointY,color = '#c92b29', s = 30)


        stepThresholdMin = 0
        stepThresholdMax = 0
        # for i in range(DRAW_THRESHOLD_MIN):
        #     # print(i, DRAW_THRESHOLD_MIN)
        #     stepThresholdMin += result[i][2]
        # for i in range(DRAW_THRESHOLD_MAX):
        #     # print(i, DRAW_THRESHOLD_MAX, len(result))
        #     # print(result[i])
        #     stepThresholdMax += result[i][2]
        # print(stepThresholdMin, stepThresholdMax, agentTrajectory[0], agentTrajectory[0][2][0])

# pickleDumpArray[agentNo].append([step, sim.getGlobalTime(), agentsPos[agentNo], agentsDestPos[agentNo], agentsDestAttribute[agentNo]])

        startDest = 0
        endDest = "767"
        # startDest = eventList[DRAW_THRESHOLD_MIN][1]
        # endDest = eventList[DRAW_THRESHOLD_MAX][1]
        startFlag = True
        endFlag = False

        startStep = 0
        endStep = len(agentTrajectory) - 1

        for record in agentTrajectory:
            if startFlag and record[-1][1] == startDest:
                startFlag = False
                startStep = record[0]
            if not endFlag and record[-1][1] == endDest:
                endFlag = True
            if endFlag and not record[-1][1] == endDest:
                endStep = record[0]
                break
        print(startDest, endDest)
        print(pathList[DRAW_THRESHOLD_MIN])
        print(pathList[DRAW_THRESHOLD_MAX])
        print(startStep,endStep,len(agentTrajectory))


        # agentPosx = [pos[2][0] for pos in agentTrajectory[startStep :endStep]]
        # agentPosy = [pos[2][1] for pos in agentTrajectory[startStep :endStep]]
        # timeCount = [float(pos[1]) for pos in agentTrajectory[startStep :endStep]]
        agentPosx = [pos[2][0] for pos in agentTrajectory]
        agentPosy = [pos[2][1] for pos in agentTrajectory]
        timeCount = [float(pos[1]) for pos in agentTrajectory]

        prevPosx = agentPosx[0]
        prevPosy = agentPosy[0]
        for i in range(1, len(agentPosx)):
            posx = agentPosx[i]
            posy = agentPosy[i]
            # print(posx, posy)
            if DRAW_SIM_PATH:
                plt.plot([posx, prevPosx], [posy, prevPosy], color = '#2ca9e1')
            prevPosx = posx
            prevPosy = posy

        plt.show()


if __name__ == "__main__":
    agentNo = 4
    drawFlag = True
    run(agentNo, drawFlag, DATA_PATH)
