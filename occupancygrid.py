#!/usr/bin/python
# -*- coding: utf-8 -*-





# makeOccupancyGrid と pathPlanning_v2に分割しました







import random
import math
import copy
import numpy as np
import os
import matplotlib.pyplot as plt
import pickle

from classdef import nodeDef
from classdef import pathDef


# MAP_Path = os.path.join("/media/sf_Ubuntu-simpy/workspace/sample_dataset_20170602/WarehouseB_30min/map")
# MAP_Path = os.path.join("/Users/nouchi/Desktop/ipin_comp/map")
# MAP_Path = os.path.join("/Users/nouchi/Desktop/map")
DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp/pickle")


# class NODE():
#
#     def __init__(self,x,y,addX,addY, mapSize):
#         self.posx = x
#         self.posy = y
#         self.address = (addX,addY)
#         # self.neighborNode = []
#         self.cost = 1.0
#         self.isObstacle = False
#         self.isStepped = False
#         self.minStepCost = 0.
#         self.minPathFromStart = None
#
#         self.neighborUP = None
#         self.neighborDown = None
#         self.neighborRight = None
#         self.neighborLeft = None
#
#         if not addY == mapSize[1] -1:
#             self.neighborUP = (self.address[0], self.address[1] + 1)
#         if not addY  == 0:
#             self.neighborDown = (self.address[0], self.address[1] - 1)
#         if not addX == mapSize[0] -1:
#             self.neighborRight = (self.address[0] +1, self.address[1])
#         if not addX == 0:
#             self.neighborLeft = (self.address[0] -1, self.address[1])
#         # print("create Node...", addX,addY, self.address, x, y)
#         # print("     neighborNode...", self.neighborNode)
#
#
# class Path():
#
#     def __init__(self, node):
#         self.edgeNode = node
#         self.route = []
#         self.route.append(node)
#         self.routeCost = node.cost
#
#     def isPassedPoint(self, target):
#         for node in self.route:
#             if node.address == target.address:
#                 return True
#             else:
#                 return False


def initializeMap(xlim,ylim,startPoint, cellSize):
    xMin = xlim[0]
    xMax = xlim[1]
    yMin = ylim[0]
    yMax = ylim[1]
    startx = startPoint[0]
    starty = startPoint[1]

    sizeX = (-1 * round((startx - xMin)/cellSize) -1, round((xMax - startx)/cellSize) +1)
    sizeY = (-1 * round((starty - yMin)/cellSize) -1, round((yMax - starty)/cellSize) +1)
    centerCell = (round((startx - xMin)/cellSize)+2, round((starty - yMin)/cellSize)+2)
    mapSize = (sizeX[1] - sizeX[0] +1, sizeY[1] - sizeY[0] +1)
    print("mapGridSize ", mapSize)
    print("sizeX",sizeX, "  sizeY",sizeY)
    # print([[(cellSize * x + startx,cellSize * y + starty, (x - sizeX[0], y - sizeY[0])) for x in range(sizeX[0] ,sizeX[0] +2)] for y in range(sizeY[0], sizeY[0] +2)])
    # print("\n\n\n")
    # print([[NODE(cellSize * x + startx, cellSize * y + starty, x - sizeX[0], y - sizeY[0], mapSize).address for x in range(sizeX[0],sizeX[0] +2)] for y in range(sizeY[0], sizeY[0] +2)])
    gridMap = [[NODE(cellSize * x + startx, cellSize * y + starty, x - sizeX[0], y - sizeY[0], mapSize) for x in range(sizeX[0] ,sizeX[1] +1)] for y in range(sizeY[0], sizeY[1] +1)]

    return gridMap

def inObstacle(point,x1,x2,y1,y2):
    x = point[0]
    y = point[1]
    if x1 <= x and x <= x2 and y1 <= y and y <= y2:
        return True
    else:
        return False

def addObstacle(map, cellSize, obstacleList):
    OBSTACLE_COST = 1000000
    cellSizeHarf = cellSize/2
    xMax = len(map[0])
    yMax = len(map)
    testCount = 0
    print("addObstacle",xMax, yMax)
    for (i,mapLine) in enumerate(map):
        for (j,node) in enumerate(mapLine):
            if j == 0 or j == xMax -1 or i == 0 or i == yMax -1:
                node.cost = OBSTACLE_COST
                node.isObstacle = True
                testCount +=1
                continue
            x = node.posx
            y = node.posy
            center = (x,y)
            ru = (x + cellSizeHarf, y + cellSizeHarf)
            rd = (x + cellSizeHarf, y - cellSizeHarf)
            lu = (x - cellSizeHarf, y + cellSizeHarf)
            ld = (x - cellSizeHarf, y - cellSizeHarf)

            for (x1,x2,y1,y2) in obstacleList:
                if node.isObstacle :
                    testCount +=1
                    break
                if inObstacle(center,x1,x2,y1,y2):
                    node.cost = OBSTACLE_COST
                    node.isObstacle = True
                else:
                    if inObstacle(ru,x1,x2,y1,y2) and inObstacle(rd,x1,x2,y1,y2):
                        node.neighborRight = None
                    if inObstacle(rd,x1,x2,y1,y2) and inObstacle(ld,x1,x2,y1,y2):
                        node.neighborDown = None
                    if inObstacle(ld,x1,x2,y1,y2) and inObstacle(lu,x1,x2,y1,y2):
                        node.neighborLeft = None
                    if inObstacle(lu,x1,x2,y1,y2) and inObstacle(ru,x1,x2,y1,y2):
                        node.neighborUP = None

    print ("obstacleCount...",testCount)
    return map


def makeMap(xlim, ylim, startPoint, cellSize, obstacleList):
    initMap = initializeMap(xlim, ylim, startPoint, cellSize)
    obstacleMap = addObstacle(initMap, cellSize, obstacleList)
    return obstacleMap


def searchNodeFromPoint(gridMap, point, cellSize):
    address = None
    for line in gridMap:
        yDist = abs(line[0].posy - point[1])
        if yDist <= cellSize:
            for node in line:
                xDist = abs(node.posx - point[0])
                if xDist <= cellSize:
                    address = node.address
    if address is None:
        print("Target point ", point, " is out of map region")
        sys.exit()
    return address


def updatePath(path, newNode):
    newPath = copy.deepcopy(path)

    newPath.edgeNode = newNode
    newPath.route.append(newNode)
    newPath.routeCost += newNode.cost

    return newPath


def pathPlanning(gridMap, startNode, goalNode):
    print("Path Planning...")
    requiredGoalPathNum = 3
    goalPath = []
    currentPath = []
    neighborNodeList = []
    currentPath.append(Path(gridMap[startNode[1]][startNode[0]]))
    goal = gridMap[goalNode[1]][goalNode[0]]
    # print("goal ", goal)
    testCount = 0
    while(True):
        prevPath = copy.deepcopy(currentPath)
        currentPath.clear()
        print("In ",testCount," step, there are ",len(prevPath)," route.")
        testCount += 1
        for path in prevPath:
            # print("currentNode ", path.edgeNode.address, "/pos ", path.edgeNode.posx, path.edgeNode.posy)
            neighborNodeList.clear()
            if path.edgeNode.neighborUP is not None:
                neighborNodeList.append(path.edgeNode.neighborUP)
            if path.edgeNode.neighborDown is not None:
                neighborNodeList.append(path.edgeNode.neighborDown)
            if path.edgeNode.neighborRight is not None:
                neighborNodeList.append(path.edgeNode.neighborRight)
            if path.edgeNode.neighborLeft is not None:
                neighborNodeList.append(path.edgeNode.neighborLeft)
            # for neighborAdd in path.edgeNode.neighborNode:
            for neighborAdd in neighborNodeList:
                neighbor = gridMap[neighborAdd[1]][neighborAdd[0]]
                # print("neighbor ", neighbor.address, "/pos ", neighbor.posx, neighbor.posy)
                if neighbor.address == goal.address:
                    newPath = updatePath(path, neighbor)
                    goalPath.append(newPath)
                    print("Find path! cost is ", newPath.routeCost, ":  step is ", len(newPath.route))
                    if len(goalPath) >= requiredGoalPathNum:
                        return goalPath
                    else:
                        continue

                elif neighbor.isObstacle :
                    continue
                elif path.isPassedPoint(neighbor):
                    continue
                elif neighbor.isStepped:
                    newCost = path.routeCost + neighbor.cost
                    if newCost < neighbor.minStepCost:
                        newPath = updatePath(path, neighbor)
                        addX = neighbor.address[0]
                        addY = neighbor.address[1]
                        gridMap[addY][addX].minStepCost = newCost
                        gridMap[addY][addX].minPathFromStart = newPath
                        currentPath.append(newPath)
                    else :
                        continue
                else :
                    newCost = path.routeCost + neighbor.cost
                    newPath = updatePath(path, neighbor)
                    addX = neighbor.address[0]
                    addY = neighbor.address[1]
                    # print(addX, addY, len(gridMap[0]), len(gridMap))
                    gridMap[addY][addX].minStepCost = newCost
                    gridMap[addY][addX].minPathFromStart = newPath
                    gridMap[addY][addX].isStepped = True
                    currentPath.append(newPath)

    if len(goalPath) < requiredGoalPathNum:
        print("goalPath is not enough ...", len(goalPath), " / ", requiredGoalPathNum)
    return goalPath

def run(startPoint, goalPoint, cellSize):
    mapInfoList = []
    obstacles = []
    shelf = []

    # pickleファイルからオブジェクトを復元
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    obstacleList = []
    obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
    print("obstacles...",len(obstacleList))

    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    print("mapSize...", xlim,ylim)

    # マップの初期作成
    gridMap = makeMap(xlim,ylim,startPoint, cellSize, obstacleList)
    #確認用
    with open(os.path.join(DATA_PATH, 'map/textMap.txt'), mode = 'w') as f:
        for line in gridMap:
            f.writelines(["x" if node.isObstacle else "o" for node in line])
            f.write("\n")

    startNode = searchNodeFromPoint(gridMap, startPoint, cellSize)
    gridMap[startNode[1]][startNode[0]].isStepped = True
    goalNode = searchNodeFromPoint(gridMap, goalPoint, cellSize)
    path = pathPlanning(gridMap, startNode, goalNode)
    if len(path) == 0:
        print("Could not find path to ", goalPoint, " from ", startPoint, ". \nSo exit this program")
        sys.exit()

    print("Find path, cost is ", path[0].routeCost)
    # for node in path[0].route:
        # print(node.posx, node.posy, "\n")

    with open(os.path.join(DATA_PATH, "goal_path.txt"), 'w') as f:
        totalCost = 0.
        for node in path[0].route:
            totalCost += node.cost
            f.write("("+str(node.address[0])+","+str(node.address[1])+")  "+str(node.isObstacle)+"  ("+str(node.posx)+","+str(node.posy)+") "+str(totalCost)+"\n")

    # 表示用pltの設定
    fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
    ax1.set_xlim(xlim[0]-10,xlim[1]+10)
    ax1.set_ylim(ylim[0]-10,ylim[1]+10)
    print("xlim", xlim[0],"  ",xlim[1])
    print("ylim", ylim[0],"  ",ylim[1])

    for (x1, x2, y1, y2) in obstacleList:
        plt.plot([x1,x2],[y1,y1] , color = '#000000')
        plt.plot([x2,x2],[y1,y2] , color = '#000000')
        plt.plot([x2,x1],[y2,y2] , color = '#000000')
        plt.plot([x1,x1],[y2,y1] , color = '#000000')

    for (i,node) in enumerate(path[0].route):
        if i == 0:
            prevNode = node
            continue
        else:
            plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#c92b29')
            prevNode = node

    for (i,node) in enumerate(path[1].route):
        if i == 0:
            prevNode = node
            continue
        else:
            plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#2ca9e1')
            prevNode = node

    for (i,node) in enumerate(path[2].route):
        if i == 0:
            prevNode = node
            continue
        else:
            plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#00a1e9')
            prevNode = node

    plt.show()
    # plt.savefig()
if __name__ == '__main__':
    startPoint = (43.3,46.6)
    goalPoint = (85.,30.)
    cellSize = 0.5

    run(startPoint, goalPoint, cellSize)
