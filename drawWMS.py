#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
import csv
import numpy as np
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import pathPlanning_v2
import searchShelf

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_hard")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

DRAW_THRESHOLD = 10
PICK_TIME = 10

START_TIME = 1485239148
CUT_TIME = [START_TIME] * 9
CUT_TIME[2] = START_TIME + 1490
CUT_TIME[3] = START_TIME + 1350
CUT_TIME[4] = START_TIME + 1350
CUT_TIME[5] = START_TIME + 1330
CUT_TIME[6] = START_TIME + 1330
CUT_TIME[7] = START_TIME + 1250
CUT_TIME[8] = START_TIME + 1345

for userNum in (2,3,4,5,6,7,8):

    with open(os.path.join(DATA_PATH,"wms/WMS.csv"), 'r') as f:
        wmsData = csv.reader(f)

        # header行を読み飛ばす
        header = next(wmsData)
        next(wmsData)

        wmsList = []
        for line in wmsData:
            # print(line)
            terminalID = line[0]
            shelfNO = int(line[1])
            utime = int(line[2])

            if int(terminalID) == userNum:
                if utime < CUT_TIME[userNum]:
                    # print(shelfNO, utime, "xxxx")
                    continue
                wmsList.append([shelfNO, utime])
                print(shelfNO, utime)
            # 邪魔な空行を読み飛ばす
            # next(wmsData)
    print("length of wmsList...", len(wmsList))
    # リストを時間でソート
    wmsList.sort(key=lambda x:x[1])

    result = []
    prevPick = "start"
    prevWmsTime = CUT_TIME[userNum]
    for wms in wmsList:
        # print(wms[0], prevRecord[-1][1])
        # print("hoge" if wms[0] == int(prevRecord[-1][1]) else "false")
        wmsTime = wms[1]
        pick = wms[0]
        diffTimeFromStart = wmsTime - CUT_TIME[userNum]


        diffWmsTime = wmsTime - prevWmsTime - PICK_TIME
        result.append([prevPick, pick, diffWmsTime, diffTimeFromStart, wmsTime])
        # print("prevPick :",prevPick, "pick :",pick, "step :",step, "time :",wmsTime, "\n    pathLength :", pathLength, "diffStep :",diffStep, "diffTime :",diffWmsTime, " length/time :", pathLength/diffWmsTime, "time/length :", diffWmsTime/pathLength)
        prevWmsTime = wmsTime
        prevPick = pick

        with open(os.path.join(DATA_PATH, "simulateResult_"+str(userNum)+".txt"), "w") as f:
            f.write("Terminal ID:"+str(userNum)+",pickTime:"+str(PICK_TIME)+"\n")
            f.write("prevPick, pick, diffTime, diffTimeFromStart, time\n")
            for line in result:
                print(line)
                f.write(",".join(map(str, line))+"\n")
