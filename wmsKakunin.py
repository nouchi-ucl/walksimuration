#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
import csv
from multiprocessing import Pool
from multiprocessing import Process
from operator import attrgetter
import numpy as np

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef_v2
from classdef import pathDef
import pathPlanningKakunin
import searchShelf

from classdef import gridMapDef
import configDef
import fileIO

conf = configDef.Config()
DATA_PATH = conf.dataPath
POOL_NUM = conf.poolNum

# def getStartTime(dataPath, agentNo):
#     agentMoveList = []
#     with open(os.path.join(dataPath, 'labels/createdMoveLabel_mod'+str(agentNo)+'.csv'), mode = 'r') as f:
#         moveData = csv.reader(f)
#         header = next(moveData)
#         header = next(moveData)
#         for line in moveData:
#             # print(line)
#             startTime = float(line[0])
#             endTime = float(line[1])
#             action = line[2]
#             agentMoveList.append([startTime, endTime, action])
#     # リストを時間でソート
#     agentMoveList.sort(key=lambda x:x[0])
#     startTime = agentMoveList[0][0]
#     return startTime

# 実際に並列で処理を回したい部分
def planning(i, prevPos, currentPos, action):
    writeName = pathPlanningKakunin.run(prevPos, currentPos, action, False)
    print("write to ", writeName, "\n     This is ", i)
    return (i, writeName)

# multiprocessingで複数の変数を引数として渡す際の処理
# 実はあまり理解できていない(multiprocessingでは直接複数の引数をとることができないらしい)
def wrapper(args):
    inputTapple = (args[0], args[1], args[2], args[3])
    return planning(*inputTapple)

def run(dataPath, userNum, drawFlag):
    picklePath = os.path.join(dataPath, "pickle")

    # 各pickleファイルの読み込み
    with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(picklePath, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0]
    ylim = mapInfoList[1]

    with open(os.path.join(picklePath, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)

    cellSize = gridMap.cellSize
    # startTime = getStartTime(dataPath, userNum)
    # wmsList = fileIO.loadWms(os.path.join(dataPath,"wms/WMS_comb.csv"), userNum, startTime)

    # (x,y) action
    # pathList = fileIO.loadPassList(os.path.join(dataPath,"wms/pathList"+str(userNum)+".csv"))
    pathList = []
    # pathList.append([conf.startPoint[0], conf.startPoint[1],"start"])

    # with open(os.path.join(dataPath,"wms/pathList1.csv"),"r") as f:
    #     wmsAllData = csv.reader(f)
    #     header = next(wmsAllData)
    #     for line in wmsAllData:
    #         if int(line[0]) == userNum:
    #             shelfID = line[1]
    #             shelfPos = searchShelf.run(shelfID, dataPath)
    #             pathList.append([shelfPos[0], shelfPos[1],"pass"])
    #             cell = gridMap.searchNodeFromPoint(shelfPos)
    #             node = gridMap.gridmap[cell[1]][cell[0]]
    #             print("add pathList,",shelfID,shelfPos,node.posx,node.posy)





    with open(os.path.join(dataPath,"wms/pathList0.csv"),"r") as f:
        wmsAllData = csv.reader(f)
        # header = next(wmsAllData)
        for line in wmsAllData:
            # if int(line[0]) == userNum:
            # shelfID = line[1]
            # shelfPos = searchShelf.run(shelfID, dataPath)
            pathList.append([float(line[0]), float(line[1]),"pass"])
            # print("add pathList,",shelfID,shelfPos)
    print(pathList)




    # print("length of wmsList...", len(wmsList))
    # リストを時間でソート
    # wmsList.sort(key=lambda x:x[0])
    # actionList = []
    # actionList.extend(wmsList)
    # actionList.extend(pathList)
    # startPos = None
    # if pathList[0][-1] == "start":
    #     startPos = [pathList[0][0], pathList[0][1]]
    #     pathList = pathList[1:]
    #     print("startPos is redefined")
    # else:
    #     startPos = conf.startPoint
    #     print("startPos is default")
    # startTime = getStartTime(dataPath, userNum)

    # print("hoge\nhoge\nhoge\n", startPos,"\n",pathList,"\n\n\n")

    startPos = [pathList[0][0], pathList[0][1]]
    startPath = [startPos[0], startPos[1], "pass"]
    prevPath = [startPos[0], startPos[1], "pass"]
    workList = []
    for (i, path) in enumerate(pathList):
        # print("wmsData...", wms)
        # if i >= len(wmsList) -1:
        # if i > 1:
            # break
        # スタート地点が指定されていない,かつリストの先頭の場合
        if prevPath is None:
            # prevPos = searchShelf.run(int(wms[1]), dataPath) if wms[-1] == "pick" else wms[1]
            # prevShelfID = int(wms[1]) if wms[-1] == "pick" else None
            # prevTime = float(wms[0])
            pos = [path[0], path[1]]
            action = path[-1]
            prevPath = [path[0], path[1], action]
            # print("continue")
            continue
        # print(prevWms, "\n", wms)
        # print(path, prevPath)
        currentPos = gridMap.nearNonObstCell([path[0], path[1]])
        prevPos = gridMap.nearNonObstCell([prevPath[0], prevPath[1]])
        # currentTime = float(wms[0])
        # prevTime = float(prevWms[0])
        # ShelfID = int(wms[1]) if wms[-1] == "pick" else "("+str(currentPos[0])+" "+str(currentPos[1])+")"
        # prevShelfID = int(prevWms[1]) if prevWms[-1] == "pick" else "("+str(prevPos[0])+" "+str(prevPos[1])+")"
        action = path[-1]
        workList.append([i, prevPos, currentPos, action])
        prevPath = path

    print("workList",workList)
    writeNameList = []
    for work in workList:
        writeNameList.append(pathPlanningKakunin.run(work[1], work[2], work[3], True))
    print(writeNameList)
    # リストを処理順でソート
    # workListの順番と一致するようにならべかえる
    # writeNameList.sort(key=lambda x:x[0])
    fnameList = [os.path.join(dataPath, fname) for fname in writeNameList]

    wayPointList = []
    for fname in fnameList:
        # print(fname)
        if len(fname.split(".")) < 2:
            continue

        with open(fname.split("\n")[0], 'r') as f:
            startPoint = (os.path.basename(fname)[:-4].split("_")[1].split(","))
            endPoint = (os.path.basename(fname)[:-4].split("_")[2].split(","))
            # print(endPoint)
            pathLines = f.readlines()
            prevPos = None
            for line in pathLines:
                data = line.split("\n")[0].split(",")
                # print("data   ", data, len(data))
                if prevPos is None:
                    prevPos = (float(data[2]), float(data[3]))
                    # prevPos = np.array((float(data[2]), float(data[3])))
                elif data[-1] == "True":
                    currentCell = (int(data[0]), int(data[1]))
                    currentPos = (float(data[2]), float(data[3]))
                    # currentPos = np.array((float(data[2]), float(data[3])))
                    dist = np.linalg.norm(np.array(currentPos) - np.array(prevPos))
                    prevPos = currentPos
                    wayPointList.append([currentCell, dist, prevPos, currentPos, "Turn"])
                    # print(currentCell, dist, prevPos, currentPos, "Turn")

                # else:
                #     currentPos = np.array((float(data[2]), float(data[3])))
                #     dist += np.linalg.norm(currentPos - prevPos)
                #     prevPos = currentPos
                # if data[-1] == "True":
                #     # data[-1] = "turn"
                #     # writeData = [data[0], data[1], data[2], data[3], "Turn"]
                #     writeData = [(data[0], data[1]), dist, data[2], data[3], "Turn"]
                #     wayPointList.append(writeData)
                #     # wayPointList.append(data)
            endPoint = (float(endPoint[0]), float(endPoint[1]))
            # endPoint = (float(endPoint[0].split(".")[0]), float(endPoint[1].split(".")[0]))
            endCell = gridMap.searchNodeFromPoint(endPoint)
            # endCell = pathPlanning_v3.searchNodeFromPoint(gridMap, endPoint, cellSize)
            action = "Pass"
            dist = np.linalg.norm(np.array(endPoint) - np.array(prevPos))
            wayPointList.append([endCell, dist, prevPos, endPoint, action])
            # wayPointList.append([endCell, dist, startPoint, endPoint, action])
            # wayPointList.append([endCell[0], endCell[1], endPoint[0], endPoint[1], dist, startShelfID, endShelfID ,action])

# 確認用だし保存はしなくても良いかな
    # userNum = 999
    if not os.path.isdir(os.path.join(dataPath, "occupancyPath")):
        os.mkdir(os.path.join(dataPath, "occupancyPath"))
    # txtとpickleで出力
    with open(os.path.join(dataPath, "occupancyPath/pathFileList_"+str(userNum)+".txt"), 'w') as f:
        # f.write([fname+"\n" for fname in pathList])
        for fname in fnameList:
            print("\n\nfname\n\n",fname,"\nここまで\n\n")
            f.write(fname+"\n")
    with open(os.path.join(dataPath, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'wb') as f:
        pickle.dump(wayPointList, f)

    print("path of user ", userNum, " is saved to ", os.path.join(dataPath, "occupancyPath/path_"+str(userNum)+".txt"))

    # 以下表示用
    if drawFlag:

        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=((xlim[1]+20)/10,(ylim[1]+20)/10))
        ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])

        # # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
        # # 単純にpathの探索をするだけなら必要ない
        obstacleList = []
        obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
        obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
        print("obstacles...",len(obstacleList))

        for (x1, x2, y1, y2) in obstacleList:
            plt.plot([x1,x2],[y1,y1] , color = '#000000')
            plt.plot([x2,x2],[y1,y2] , color = '#000000')
            plt.plot([x2,x1],[y2,y2] , color = '#000000')
            plt.plot([x1,x1],[y2,y1] , color = '#000000')

        turnPointX = []
        turnPointY = []
        passPointX = []
        passPointY = []
        pickPointX = []
        pickPointY = []

        # 各パスの描画と方向転換点，ピック点を描画
        for fName in fnameList:
            pickleName = fName[:-3]+"pickle"
            with open(pickleName, mode='rb') as f:
                path = pickle.load(f)

            for (i,address) in enumerate(path):
                node = gridMap.gridmap[address[0][1]][address[0][0]]
                if i == 0:
                    prevNode = node
                    if len(pickleName[:-4].split("_")[-1].split(",")[0].split(" ")) > 1:
                    # print("Red path is ", path[j].routeCost, " in ", len(path[j].route) -1, " step\n")
                        passPointX.append(node.posx)
                        passPointY.append(node.posy)
                    else:
                        pickPointX.append(node.posx)
                        pickPointY.append(node.posy)
                    continue
                else:
                    plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#a1a900')
                    prevNode = node
                    if address[1] == True:
                        turnPointX.append(node.posx)
                        turnPointY.append(node.posy)

        if startPos is not None:
            pt = ax1.scatter(startPos[0], startPos[1], color = '#2ca9e1')
        pt1 = ax1.scatter(turnPointX,turnPointY,color = '#07d34f')
        pt2 = ax1.scatter(pickPointX,pickPointY,color = '#c92b29')
        if len(passPointX) > 0:
            pt3 = ax1.scatter(passPointX, passPointY, color ="#ffd90d")

        plt.show()
        del turnPointX, turnPointY, pickPointX, pickPointY

    del mapInfoList, obstacles, gridMap, pathList, workList, wayPointList

if __name__ == '__main__':
    userNum = 0
    run(DATA_PATH, userNum, False)
