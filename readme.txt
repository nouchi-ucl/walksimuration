必要なPython環境
Python 3.5.3で動作を確認(nouchi)

必要なライブラリ

DATA_PATH : configで設定



実行に必要なもの
(DATA_PATH/map/)Map_info.csv
(DATA_PATH/map/)Obstacle_data.csv

*optional
(DATA_PATH/map/)Shelf_data.csv
(DATA_PATH/map/)BLE_info.csv


実行に必要なPythonファイル
configDef.py
loadmap.py
makeOccupancyGrid_v3.py
wmsPlanning_multi_v4.py

classdef/
    compassModel.py
    gridMapDef.py
    nodeDef_v2.py
    pathDef.py
    recordDef.py
    sensorDataDef.py
    
実行順メモ

loadmap.py
loadPickingData.py
makeOccupancyGrid_v3.py
wmsPlanning_multi_v4.py

//RVO2が動く環境(nouchiはUbuntu上)
makeSumilation_multi_v3.py

最終的な結果出力
makeMove.py

出力ファイルの描画(経路
drawResult.py
