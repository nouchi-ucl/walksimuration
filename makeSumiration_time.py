#!/usr/bin/env python
import rvo2
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os
import pickle
import csv

import searchShelf

# MAP_Path = os.path.join("/media/sf_Ubuntu-simpy/workspace/ipin/sampledata/WarehouseB_30min/map")
# MAP_Path = os.path.join("/media/sf_Ubuntu-simpy/workspace/sample_dataset_20170602/WarehouseB_30min/map")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
# DATA_PATH = os.path.join("/media/sf_Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min/")
DATA_PATH = os.path.join("/media/sf_Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

# シミュレーション1ステップあたりに進める時間
STEP_TIME = 1/10
# 計算を終了させるステップ数 STEP_THRESHOLD * STEP_TIMEまでのシミュレーションを行う
# STEP_THRESHOLD = 0で指定なし(infinit)
STEP_THRESHOLD = 6000
# simulation環境の設定
# float timeStep, float neighborDist, size_t maxNeighbors,float timeHorizon,
# float timeHorizonObst, float radius,float maxSpeed, tuple velocity=(0, 0)
sim = rvo2.PyRVOSimulator(STEP_TIME, 0.3, 5, 1.5, 0.5, 0.2, 1.5)

# userNumList = [6]
userNumList = [2,3,4,5,6,7,8]

def setvAvefromMoveList(agent, time, moveList, wmsList, wayPointList):
    # print("setvAvefromMoveList", agent, time)
    action = None
    pickCount = 0
    prevWms = None
    startShelfID = None
    endShelfID = None
    for wms in wmsList:
        if prevWms is None:
            prevWms = ["start", 0]
        if int(prevWms[1]) < time and time < int(wms[1]):
            startTime = int(prevWms[1])
            endTime = int(wms[1])
            startShelfID = prevWms[0]
            endShelfID = wms[0]
            break
        prevWms = wms
    # print(startTime, endTime, startShelfID, endShelfID)
    for point in wayPointList:
        if point[-1] != "Pick":
            continue
        if int(point[-2]) == int(endShelfID):
            distance = int(point[-4])
    countTime = 0
    for line in moveList:
        if line[0] < time and time < line[1]:
            action = line[2]
        if line[2] == "move" or line[2] == "Rturn_m" or line[2] == "Lturn_m" or line[2] == "Uturn":
            if startTime < line[1] and line[1] < endTime:
                countTime += line[1] - startTime if line[0] < startTime else line[1] - line[0]
            elif startTime < line[0] and line[0] < endTime:
                countTime += endTime - line[0] if endTime < line[1] else line[1] - line[0]
            elif line[0] < startTime and endTime < line[1]:
                countTime += endTime - startTime
            # print(countTime)
    # print("countTime", countTime, "   distance", distance)
    if countTime == 0:
        vAve = 9999
    else:
        vAve = distance / countTime
    if action != "move" and action != "Rturn_m" and action != "Lturn_m" and action != "Uturn":
        vAve = 0
        action = "stop"
    return vAve, action, startShelfID, endShelfID

def setvPref(agent, num, destination, vPrefAve):
    # print(destination[num[0]][num[1]])
    # print(float(destination[num[0]][num[1]][2]))
    # print(float(destination[num[0]][num[1]][3]))

    shelfID = None

    if destination[num[0]][num[1]][-1] == "Pick":
        distanceThreshold = 0.25
        shelfID = destination[num[0]][num[1]][-2]
        currentDest = searchShelf.run(shelfID, DATA_PATH)
        # print("current destination is pickPoint ID :",shelfID, ", pos :", currentDest)
    else:
        distanceThreshold = 0.2
        currentDest = np.array([float(destination[num[0]][num[1]][2]),float(destination[num[0]][num[1]][3])])

    pos = np.array(sim.getAgentPosition(agent))
    distance = np.linalg.norm(currentDest - pos)
    dest = [num[0],num[1]]
    # print(pos,currentDest,distance)


    if distance <= distanceThreshold:
        if num[1] >= len(destination[num[0]]) - 1:
            vPref = (0,0)
            # print("つきました")
            return dest, vPref, None
        currentDest = np.array([float(destination[num[0]][num[1]+1][2]),float(destination[num[0]][num[1]+1][3])])
        if destination[num[0]][num[1]+1][-1] == "Pick":
            startShelfID = destination[num[0]][num[1]+1][-3]
            endShelfID = destination[num[0]][num[1]+1][-2]
            currentDest = searchShelf.run(endShelfID, DATA_PATH)
            # print("next destination is pickPoint ID :", endShelfID, ", pos :", currentDest)

        dest = [num[0],num[1]+1]
    direction = (currentDest - pos) / distance
    vPref = vPrefAve * direction
    # vPref = sim.getAgentMaxSpeed(agent) /2 *direction
    # vPref = (velocity[0] / np.linalg.norm(velocity), velocity[1] / np.linalg.norm(velocity))
    # print("renew vPref:",currentDest, pos, distance, direction, vPref)
    return dest, (vPref[0], vPref[1]), currentDest

def run(drawFlag):
    # print(destination)
    # Pass either just the position (the other parameters then use
    # the default values passed to the PyRVOSimulator constructor),
    # or pass all available parameters.
    agents = []
    agentDest = []
    writeFileNameList = []

    mapInfoList = []
    #  読み込み用リスト 棚
    shelfList = []
    #  読み込み用リスト 障害物
    obstacleList = []

    wayPointList = []
    destination = []

    # 表示用リスト 棚/障害物
    obstacles = []

    # pickleファイルからオブジェクトを復元
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)
    print (mapInfoList)
    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    startPoint = mapInfoList[0][2]


    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelfList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacleList = pickle.load(f)

    # wayPointList = [[]*len(userNumList)]
    writeFileNameList = [[] for i in range(0, len(userNumList))]
    destPos = [[] for i in range(0, len(userNumList))]
    pickleDumpArray = [[] for i in range(0, len(userNumList))]
    agentMoveList = [[] for i in range(0, len(userNumList))]
    wmsList = [[] for i in range(0, len(userNumList))]
    agentStartTime = [ 0 for i in range(0, len(userNumList))]
    startTime = 99999999999999
    for (agentNo,userNum) in enumerate(userNumList):
        with open(os.path.join(DATA_PATH, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'rb') as f:
        # with open(os.path.join(DATA_PATH, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'rb') as f:
            wayPointList = pickle.load(f)
            writeFileNameList[agentNo].append(str(userNum))
        distNo = 0
        agents.append(sim.addAgent(startPoint))
        agentDest.append([agentNo, distNo])
        destination.append(wayPointList)
        destPos[agentNo].append([[float(destination[agentNo][distNo][2]), float(destination[agentNo][distNo][3])]])
        pickleDumpArray[agentNo].append([0, 0.0, (startPoint[0], startPoint[1]), (float(destination[agentNo][distNo][2]), float(destination[agentNo][distNo][3])), ["Start", "None"]])

        with open(os.path.join(DATA_PATH, 'labels/allLabel'+str(userNum)+'.csv'), mode = 'r') as f:
            moveData = csv.reader(f)
            header = next(moveData)
            header = next(moveData)
            for line in moveData:
                # print(line)
                startTime = float(line[0])
                endTime = float(line[1])
                action = line[2]
                agentMoveList[agentNo].append([startTime, endTime, action])
        # リストを時間でソート
        agentMoveList[agentNo].sort(key=lambda x:x[0])
        agentStartTime[agentNo] = agentMoveList[agentNo][0][0]
        if agentMoveList[agentNo][0][0] < startTime:
            startTime = agentMoveList[agentNo][0][0]

        with open(os.path.join(DATA_PATH,"wms/WMS_comb.csv"), 'r') as f:
            wmsData = csv.reader(f)
            # header行を読み飛ばす
            header = next(wmsData)
            next(wmsData)
            for line in wmsData:
                terminalID = line[0]
                shelfNO = line[1]
                utime = int(line[2])
                if int(terminalID) == userNum:
                    if utime < agentStartTime[agentNo]:
                        # print(shelfNO, utime, "xxxx")
                        continue
                    wmsList[agentNo].append([shelfNO, utime])
                    # print(shelfNO, utime)
                # 邪魔な空行を読み飛ばす
                # next(wmsData)

        print("length of wmsList of agent", userNumList[agentNo], "...", len(wmsList[agentNo]))
        # リストを時間でソート
        wmsList[agentNo].sort(key=lambda x:x[1])


    if drawFlag:
        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
        ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])
        objs = []

        img = plt.imread(os.path.join(DATA_PATH, "map/Map_image.png"))
        ax1.imshow(img, extent=[xlim[0], xlim[1], ylim[0], ylim[1]], aspect=1)


    # マップ外周を障害物として追加
    obstacles.append(sim.addObstacle([(xlim[0],ylim[0]),(xlim[1],ylim[0]),(xlim[1],ylim[1]),(xlim[0],ylim[1])]))
    # obstacleを障害物として追加
    obstacles.append([sim.addObstacle([(s[1],s[3]),(s[2],s[3]),(s[2],s[4]),(s[1],s[4])]) for s in shelfList])
    # shelfを障害物として追加
    obstacles.append([sim.addObstacle([(o[0],o[2]),(o[1],o[2]),(o[1],o[3]),(o[0],o[3])]) for o in obstacleList])

    # これ以降は障害物の追加をしても反映されないので注意
    sim.processObstacles()

    print('Simulation has %i agents and %i obstacle vertices in it.' %
          (sim.getNumAgents(), sim.getNumObstacleVertices()))

    print('Running simulation')

    # for step in range(200):
    step = 0
    stopCount = 0
    loopFlag = True
    prevPos = None
    prevTime = startTime
    moveFlag = False
    csvOutputArray = [[["x","y","unixtime"]] for i in range(0,len(userNumList))]
    actionList = [[] for i in range(0, len(userNumList))]
    while(loopFlag):
        moveFlag = False
        time = prevTime + STEP_TIME
        # 目的地点に十分近い場合は目標,速度の更新・そうでない場合は速度の更新を行う
        for (agentNo,agent) in enumerate(agents):
            vPrefAve, action, startID, endID  = setvAvefromMoveList(agent, time, agentMoveList[agentNo], wmsList[agentNo], destination[agentNo])
            actionList[agentNo] = action
            if action != "stop":
                moveFlag = True
            agentDest[agentNo], vPref, destPos[agentNo]= setvPref(agent,agentDest[agentNo],destination, vPrefAve)

            # print("\ndestination : ", destination[agentDest[agentNo][0]][agentDest[agentNo][1]], "\nvPref : ", vPref)
            # print("\ndestination : ", destination[dest[0]][dest[1]], "vPref : ", vPref)
            sim.setAgentPrefVelocity(agent, vPref)
            sim.setAgentMaxSpeed(agent, vPrefAve * 2 if vPrefAve < 1.5 else 1.5)
            # print(sim.getAgentVelocity(agent))

        # 全てのagentに対してdestPosがない(次の目標地点がない)場合はシミュレーションを終了
        flag = False
        for agentNo in agents:
            if destPos[agentNo] is not None:
                flag = True
        loopFlag = flag

        if STEP_THRESHOLD != 0 and STEP_THRESHOLD < step:
            loopFlag = False
        # print("DestPos : ", destPos[agentNo])

        positions = ['(%5.3f, %5.3f)' % sim.getAgentPosition(agentNo)
                     for agentNo in agents]
        # print('step=%2i  t=%.3f  Pos =%s' % (step, sim.getGlobalTime(), '  '.join(positions)))
        agentsPos = [(sim.getAgentPosition(agentNo)[0], sim.getAgentPosition(agentNo)[1]) for agentNo in agents]
        agentsPosx = [sim.getAgentPosition(agentNo)[0] for agentNo in agents]
        agentsPosy = [sim.getAgentPosition(agentNo)[1] for agentNo in agents]

        # agentsDestPos = [(destPos[agentNo][0], destPos[agentNo][1]) if destPos[agentNo] is not None else None for agentNo in agents]
        agentsDestPosx = [destPos[agentNo][0] if destPos[agentNo] is not None else float(destination[agentNo][-1][2]) for agentNo in agents]
        agentsDestPosy = [destPos[agentNo][1] if destPos[agentNo] is not None else float(destination[agentNo][-1][3]) for agentNo in agents]
        agentsDestAttribute = [["Pick", destination[agentDest[agentNo][0]][agentDest[agentNo][1]][-2]]
                                if destination[agentDest[agentNo][0]][agentDest[agentNo][1]][-1] == "Pick"
                                else ["Turn", "None"] for agentNo in agents]
        # agentsDestPos = [(posx, posy) for (posx, posy) in zip(agentsDestPosx, agentsDestPosy)]
        agentsDestPos = [(posx, posy) for (posx, posy) in zip(agentsDestPosx, agentsDestPosy)]
        for agentNo in agents:
            # print([agentsPos[agentNo], agentsDestPos[agentNo]])
            pickleDumpArray[agentNo].append([step, sim.getGlobalTime(), agentsPos[agentNo], agentsDestPos[agentNo], agentsDestAttribute[agentNo]])
            if agentStartTime[agentNo] <= time:
                csvOutputArray[agentNo].append([agentsPosx[agentNo], agentsPosy[agentNo], time])

        print("\nIn %i step ( %.3f  //  %.5f): " %(step, time - startTime, time))
        for agentNo in agents:
            posx = agentsPos[agentNo][0]
            posy = agentsPos[agentNo][1]
            destx = agentsDestPos[agentNo][0]
            desty = agentsDestPos[agentNo][1]
            velx = sim.getAgentVelocity(agentNo)[0]
            vely = sim.getAgentVelocity(agentNo)[1]
            print("    Agent%i pos:(%.3f,%.3f) dest:(%.3f,%.3f) %s %s velocity:(%.3f,%.3f)"
                    %(userNumList[agentNo],posx,posy,destx,desty,agentsDestAttribute[agentNo][0],actionList[agentNo],velx,vely))

        if prevPos == agentsPos and moveFlag == True:
            stopCount += 1
            if stopCount > 10 / STEP_TIME:
                print("There was no movement for 10sec. Finish simulation")
        prevPos = agentsPos
        prevTime = time

        if drawFlag:
            # x1 = [float(data[2]) for data in destination[0]]
            # y1 = [float(data[3]) for data in destination[0]]
            movePosx = []
            movePosy = []
            stopPosx = []
            stopPosy = []
            for agentNo in agents:
                if actionList[agentNo] == "move":
                    movePosx.append(agentsPosx[agentNo])
                    movePosy.append(agentsPosy[agentNo])
                else :
                    stopPosx.append(agentsPosx[agentNo])
                    stopPosy.append(agentsPosy[agentNo])
            # if len(movePosx) != 0:
            move = ax1.scatter(movePosx,movePosy,color = '#c92b29', s=8)
            # if len(stopPosx) != 0:
            stop = ax1.scatter(stopPosx,stopPosy,color = '#2ca9e1', s=8)
            # pt2 = ax1.scatter(agentsDestPosx,agentsDestPosy,color = '#2ca9e1', s=5)
            shelfPosx = []
            shelfPosy = []
            if startID is not None:
                if startID == "start":
                    shelfPosx.append(startPoint[0])
                    shelfPosy.append(startPoint[1])
                else:
                    shelfPosx.append(searchShelf.run(startID, DATA_PATH)[0])
                    shelfPosy.append(searchShelf.run(startID, DATA_PATH)[1])
            if endID is not None:
                shelfPosx.append(searchShelf.run(endID, DATA_PATH)[0])
                shelfPosy.append(searchShelf.run(endID, DATA_PATH)[1])
            if len(shelfPosx) != 0:
                pt3 = ax1.scatter(shelfPosx, shelfPosy,color = '#FF1493', s=10)
                objs.append([move,stop,pt3])
            else:
                objs.append([move,stop])

        step += 1

        # RVO2の1ステップ更新
        sim.doStep()


    if not os.path.isdir(os.path.join(DATA_PATH, "simulation")):
        os.mkdir(os.path.join(DATA_PATH, "simulation"))
    if not os.path.isdir(os.path.join(DATA_PATH, "result")):
        os.mkdir(os.path.join(DATA_PATH, "result"))

    for agentNo in agents:
        print(writeFileNameList)
        with open(os.path.join(DATA_PATH, "simulation/"+ writeFileNameList[agentNo][0]+ "_simulatedTrajectory_time.pickle"), mode = 'wb') as f:
            pickle.dump(pickleDumpArray[agentNo], f)
        with open(os.path.join(DATA_PATH, "result_time/Terminal"+str(userNumList[agentNo])+".csv"), "w") as f:
            dataWriter = csv.writer(f)
            dataWriter.writerows(csvOutputArray[agentNo])


    if drawFlag:
        interval = 100 * STEP_TIME
        ani = animation.ArtistAnimation(fig, objs, interval = 50, repeat = True)
        plt.show()

if __name__ == '__main__':
    drawFlag = True
    run(drawFlag)
