#!/usr/bin/env python
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os
import sys
import pickle
import csv

from classdef import bleDef
import configDef




# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# dataPath = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# dataPath = os.path.join("/Users/nouchi/Desktop/ipin_comp")

def loadMapInfo(path):
	xlim = [0, 0]
	ylim = [0, 0]
	startPoint = None
	with open(path) as f:
		lines = f.readlines()
		for line in lines[1:]:
			data = line.split("\n")[0].split(",")
			if len(data) > 1:
				# print(data)
				if data[2] == 'vertex':
					x = float(data[0])
					y = float(data[1])
					# mapInfoList.append([x, y, data[2]])

					if(x < xlim[0]):
						xlim[0] = x
					if(x > xlim[1]):
						xlim[1] = x
					if(y < ylim[0]):
						ylim[0] = y
					if(y > ylim[1]):
						ylim[1] = y

				if data[2] == 'stert_point':
					startPoint = (float(data[0]),float(data[1]))
	# マップの境界情報を出力用配列に格納
	mapInfoArray = [xlim,ylim, startPoint]
	print (mapInfoArray)
	return mapInfoArray

def loadShelf(path):
	shelfArray = []
	with open(path) as f:
		lines = f.readlines()
		print(len(lines))
		for line in lines[1:]:
			# line = lines[i]
			data = line.split("\n")[0].split(",")
			if len(data) > 1:
				shelfid = int(data[0])
				x = float(data[1])
				y = float(data[2])
				width = float(data[3])
				depth = float(data[4])
				if data[5] == 'up' or data[5] == 'up2' :
					data[5] = 'up'
				elif data[5] == 'down' or data[5] == 'down2':
					data[5] = 'down'
				pickForm = data[5]
				if pickForm == 'up' or pickForm == 'down':
					xLength = width /2
					yLength = depth /2
				else:
					xLength = depth /2
					yLength = width /2
				# else :
					# continue
				x1 = x - xLength
				x2 = x + xLength
				y1 = y - yLength
				y2 = y + yLength

				shelfArray.append([shelfid,x1,x2,y1,y2,pickForm])
	return shelfArray

def loadObstacle(path):
	obstacleArray = []
	with open(path) as f:
		lines = f.readlines()
		print(len(lines))
		for line in lines[1:]:
			data = line.split("\n")[0].split(",")
			if len(data) > 1:
				x = float(data[0])
				y = float(data[1])
				xLength = float(data[2]) /2
				yLength = float(data[3]) /2

				x1 = x - xLength
				x2 = x + xLength
				y1 = y - yLength
				y2 = y + yLength

				# obstacles.append(sim.addObstacle([(x1,y1),(x1,y2),(x2,y2),(x2,y1)]))
				obstacleArray.append([x1,x2,y1,y2,data[4]])
	return obstacleArray

def loadBLE(path):
	bleArray = []
	with open(path) as f:
		lines = f.readlines()
		print("BLE_info.csv", len(lines))
		for line in lines[1:]:
			data = line.split("\n")[0].split(",")
			if len(data) > 1:
				macAdd = data[0]
				x = float(data[1]) / 1000
				y = float(data[2]) / 1000
				z = float(data[3]) / 1000
				# print(macAdd,"\n     ",x,y,z)
				bleArray.append(bleDef.Ble(macAdd,x,y,z))
	return bleArray

def run(conf):
	dataPath = conf.dataPath
	# 中間ファイルをpickle形式で保存する，保存場所は "dataParh"/pickle/ 以下
	# ディレクトリがない場合は作成
	picklePath = os.path.join(dataPath, "pickle")
	if not os.path.isdir(picklePath):
		os.mkdir(picklePath)

	# 表示用リスト　棚/障害物
	obstacles = []
	partitionList = []

	# 出力用リスト　マップ外周
	mapInfoPickle = []
	# 出力用リスト 棚情報
	shelfPickle = []
	# 出力用リスt　障害物情報
	obstaclePickle = []
	# 出力用リスト BLEビーコン情報
	blePickle = []


	# 各入力ファイル(csv)を読み込み　その後pickleファイルとして保存
	mapInfoPickle = loadMapInfo(os.path.join(dataPath, "map/Map_info.csv"))
	with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'wb') as f:
		pickle.dump(mapInfoPickle, f)

	obstaclePickle = loadObstacle(os.path.join(dataPath, "map/Obstacle_data.csv"))
	with open(os.path.join(picklePath, 'obstacle.pickle'),mode = 'wb') as f:
		pickle.dump(obstaclePickle, f)

    # shelfとBLEについては必要がなくてもダミーで空ファイルを生成
	# if conf.useShelf:
	shelfPickle = loadShelf(os.path.join(dataPath,"map/Shelf_data.csv"))
	with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'wb') as f:
		pickle.dump(shelfPickle, f)

	# if conf.useBLE:
	blePickle = loadBLE(os.path.join(dataPath, "map/BLE_info.csv"))
	with open(os.path.join(picklePath, 'ble.pickle'),mode = 'wb') as f:
		pickle.dump(blePickle, f)

    # obstacleとshelfをまとめたものをpartitionとして保存しておく
	# if not os.path.isdir(os.path.join(dataPath, 'csv')):
	# 	os.mkdir(os.path.join(dataPath, 'csv'))
	# with open(os.path.join(dataPath, 'csv/partition.csv'), 'w') as f:
	with open(os.path.join(dataPath, 'map/partition.csv'), 'w') as f:
	    writer = csv.writer(f, lineterminator='\n') # 改行コード（\n）を指定しておく
	    writer.writerows([["Partition", o[0], o[2], o[1], o[3]]for o in obstaclePickle])
	    writer.writerows([["Partition", s[1], s[3], s[2], s[4]]for s in shelfPickle])


if __name__ == '__main__':
	conf = configDef.Config()
	run(conf)
