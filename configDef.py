import os

# CONF_TYPE = "ipin"
CONF_TYPE = "hasc"

class Config(object):
    def __init__(self):
        # Configのタイプを切り替えられるように追加
        self.confType = CONF_TYPE


        # obstacleと別にshelf情報を利用するかどうか
        # True ->使用 False->使用しない
        # デフォルトはFalse
        self.useShelf = False
        # BLEビーコンのセンサ情報を利用するかどうか
        # True ->使用 False->使用しない
        # デフォルトはFalse
        self.useBLE = False

        if CONF_TYPE == "ipin":
            self.dataPath = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
            self.simDataPath = os.path.join("/media/sf_Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
            # 生成した仮想センサデータの出力先
            self.virtualLogPath = os.path.join("/Users/nouchi/Documents/workspace/runtime-hascToolConf/testProject")
            # 通過点リストの場所(self.dataPathからの相対パス)
            self.passList = "wms/WMS_comb_all.csv"

            self.useShelf = True
            self.useBLE = True

        # HASC用
        if CONF_TYPE == "hasc":
            # self.dataPath = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/hasc/HASC-30088")
            self.dataPath = os.path.join("/media/sf_Ubuntu-simpy/workspace/test_dataset/hasc/HASC-30088")
            self.simDataPath = os.path.join("/media/sf_Ubuntu-simpy/workspace/test_dataset/hasc/HASC-30088")
            # 生成した仮想センサデータの出力先
            self.virtualLogPath = os.path.join("/Users/nouchi/Documents/workspace/runtime-hascToolConf/testProject")
            # 通過点リストの場所(self.dataPathからの相対パス)
            self.passList = "wms/pathList0.csv"

        self.userList = []
        #
        self.sensorPath = os.path.join(self.virtualLogPath, "virtualLogData/sensorMaterial")
        # 環境の地図画像へのパス(self.simDataPathからの相対パスにしておく)
        self.imgMap = "map/Map_image.png"

        # makeOccupancyGridで使用
        # 障害物内に進入する際の移動コスト(通常移動コストは1)
        self.obstacleCost = 10000
        # 障害物の隣のセルへの移動にかかるコスト(通常移動コストは1)
        self.neighborObstacleCost = 5.
        # 障害物情報の追加用
        # 基本的に隙間ができてしまっている部分の手動での穴埋め
        # これはIPIN用
        if CONF_TYPE == "ipin":
            self.extendObstList = [(43.5,45.,31.5,33),
                                    (53.5,73.0,5.3,5.7),
                                    (64.8,67.1,20.7,21.3)]
        if CONF_TYPE == "hasc":
            self.extendObstList = []

        # makeOccupancyGridでgridmapを作る時のcellサイズ
        self.cellSize = 0.3

        # wmsPlanningを実行する際の同時並列実行数
        self.poolNum = 2

        # 斜め方向への移動を許可するかのフラグ
        # True -> 8方向の移動 False ->上下左右の4方向のみの移動に限定
        self.allowDiagonalMove = True
        # ダイクストラ実行時のコスト上限
        # 移動コストがこの値を超えたときそのルートの探索を終了する
        self.routeCostThreshold = 25000
        # 斜め方向への移動にかかるコスト倍率
        # 基本的に十字方向との距離の比を設定
        self.diagonalMoveCost = 1.4
        # goalPointに到着したrouteの数が以下の値を超えたら全体の探索を終了する
        # 探索が終了した順N件の中で最もコストが低いルートが選ばれる
        # 早く探索が終了した = コストが最も低い　ではないことに注意
        self.requiredPathNum = 10
        # 一回の方向転換で増加するコスト(回転の角度は考慮していない)
        self.turnCost = 2.0
        # 以下のstep数の間新しくgoalにたどり着いたrouteがなければ全体の探索を終了する(1本目が見つかるまでは無視される)
        self.findStepThreshold = 30

        # スタート地点は廃止->passListの先頭をスタート地点に設定するように
        # # agentのスタート地点
        # if CONF_TYPE == "ipin":
        #     self.startPoint = (43.3, 46.6)
        # if CONF_TYPE == "hasc":
        #     self.startPoint = (-15.5,20.9)
        # self.goalPoint = (43.3, 46.6)

        # simulation環境の設定
        # float timeStep, float neighborDist, size_t maxNeighbors,float timeHorizon,
        # float timeHorizonObst, float radius,float maxSpeed, tuple velocity=(0, 0)

        # シミュレーションでの1ステップで経過させる時間(sec)
        # 1step = 歩行者の1歩
        self.simStepTime = 1/2
        # agentが人や障害物を認識する半径(m)
        self.neighborDist = 10
        # agentが考慮する最大人数
        self.maxNeighbors = 100
        # agentが何秒先まで見て人との回避行動をとるか
        self.timeHorizon = 3.
        # agentが何秒先まで見て障害物との回避行動をとるか
        self.timeHorizonObst = 5.
        # agentの初期半径(体の大きさ)
        self.radius = 0.3
        # agentの出せる最大速度
        self.maxSpeed = 1.5
        # agentの初期速度
        self.initVelocity = (0,0)

        # 方向転換とみなされる最小回転角 (digree)
        self.directionMinThreshold = 15
        # 1stepで許容される最大回転角 (digree)
        self.directionMaxThreshold = 45

        # 目的地に到着したとみなされる距離
        self.destinationDist = self.maxSpeed * self.simStepTime
        # 中間点に到着したとみなされる距離
        self.passPointDist = self.maxSpeed * self.simStepTime

        # 全てのagentがx秒以上動かないのならシミュレーションを終了
        self.allowStopTime = 10
        # 作成するPDR波形の周波数
        self.virtualWaveFrequency = 50
