#!/usr/bin/env python
import rvo2
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os
import pickle

import searchShelfUbuntu

# MAP_Path = os.path.join("/media/sf_Ubuntu-simpy/workspace/ipin/sampledata/WarehouseB_30min/map")
# MAP_Path = os.path.join("/media/sf_Ubuntu-simpy/workspace/sample_dataset_20170602/WarehouseB_30min/map")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
# DATA_PATH = os.path.join("/media/sf_Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min/")
DATA_PATH = os.path.join("/media/sf_Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

# simulation環境の設定
# float timeStep, float neighborDist, size_t maxNeighbors,float timeHorizon,
# float timeHorizonObst, float radius,float maxSpeed, tuple velocity=(0, 0)
STEP_TIME = 1/10
sim = rvo2.PyRVOSimulator(STEP_TIME, 1., 5, 1.5, 2, 0.4, 2)

userNum = 2


def setvPref(agent, num, destination):
    print(destination[num[0]][num[1]])
    print(float(destination[num[0]][num[1]][2]))
    print(float(destination[num[0]][num[1]][3]))

    shelfID = None

    if destination[num[0]][num[1]][-1] == "Pick":
        distanceThreshold = 0.31
        shelfID = destination[num[0]][num[1]][-2][0].split(".")[0]
        currentDest = searchShelfUbuntu.searchShelfFromID(shelfID)
        print("current destination is pickPoint ",shelfID, currentDest)
    else:
        distanceThreshold = 0.4
        currentDest = np.array([float(destination[num[0]][num[1]][2]),float(destination[num[0]][num[1]][3])])

    pos = np.array(sim.getAgentPosition(agent))
    distance = np.linalg.norm(currentDest - pos)
    dest = [num[0],num[1]]
    # print(pos,currentDest,distance)


    if distance < distanceThreshold:
        if num[1] >= len(destination[num[0]]) - 1:
            vPref = (0,0)
            print("つきました")
            return dest, vPref, None
        currentDest = np.array([float(destination[num[0]][num[1]+1][2]),float(destination[num[0]][num[1]+1][3])])
        if destination[num[0]][num[1]+1][-1] == "Pick":
            shelfID = destination[num[0]][num[1]][-2][0].split(".")[0]
            currentDest = searchShelfUbuntu.searchShelfFromID(shelfID)
            print("next destination is pickPoint ", shelfID, currentDest)

        dest = [num[0],num[1]+1]
    direction = (currentDest - pos) / distance
    vPref = sim.getAgentMaxSpeed(agent) /2 *direction
    # vPref = (velocity[0] / np.linalg.norm(velocity), velocity[1] / np.linalg.norm(velocity))
    print("renew vPref:",currentDest, pos, distance, direction, vPref)
    return dest, (vPref[0], vPref[1]), currentDest

def run(drawFlag):
    # print(destination)
    # Pass either just the position (the other parameters then use
    # the default values passed to the PyRVOSimulator constructor),
    # or pass all available parameters.
    agents = []
    agentDest = []
    writeFileNameList = []

    # for i in range(0,5):
    #     d = np.random.rand() * 5 - 3
    #     # d = d * 2 -1
    #     agents.append(sim.addAgent((0+d, 0+d)))
    #     agentDest.append([0,0])
    #     agents.append(sim.addAgent((100+d, 0+d)))
    #     agentDest.append([1,0])
    #     agents.append(sim.addAgent((100+d, 100+d)))
    #     agentDest.append([2,0])
    #     agents.append(sim.addAgent((0+d, 100+d)))
    #     agentDest.append([3,0])

    # # Obstacles are also supported.

    mapInfoList = []
    #  読み込み用リスト 棚
    shelfList = []
    #  読み込み用リスト 障害物
    obstacleList = []

    wayPointList = []
    destination = []

    # 表示用リスト 棚/障害物
    obstacles = []

    # pickleファイルからオブジェクトを復元
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelfList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacleList = pickle.load(f)

    with open(os.path.join(DATA_PATH, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'rb') as f:
    # with open(os.path.join(DATA_PATH, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'rb') as f:
        wayPointList = pickle.load(f)
        writeFileNameList.append(str(userNum))

    print (mapInfoList)
    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    startPoint = mapInfoList[0][2]

    destPos = []
    pickleDumpArray = []

    agentNo = 0
    distNo = 0

    agents.append(sim.addAgent(startPoint))
    agentDest.append([agentNo, distNo])
    destination.append(wayPointList)

    destPos.append([[float(destination[agentNo][distNo][2]), float(destination[agentNo][distNo][3])]])
    pickleDumpArray.append([[0, 0.0, (startPoint[0], startPoint[1]), (float(destination[agentNo][distNo][2]), float(destination[agentNo][distNo][3])), ["Start", "None"]]])

    # destPos = [] * len(agents)
    # pickleDumpArray = [] * len(agents)

    # print(len(agents), len(destPos))

    if drawFlag:
        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
        ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])
        objs = []

        img = plt.imread(os.path.join(DATA_PATH, "map/Map_image.png"))
        ax1.imshow(img, extent=[xlim[0], xlim[1], ylim[0], ylim[1]], aspect=1)


    # マップ外周を障害物として追加
    obstacles.append(sim.addObstacle([(xlim[0],ylim[0]),(xlim[1],ylim[0]),(xlim[1],ylim[1]),(xlim[0],ylim[1])]))
    # obstacleを障害物として追加
    obstacles.append([sim.addObstacle([(s[1],s[3]),(s[2],s[3]),(s[2],s[4]),(s[1],s[4])]) for s in shelfList])
    # shelfを障害物として追加
    obstacles.append([sim.addObstacle([(o[0],o[2]),(o[1],o[2]),(o[1],o[3]),(o[0],o[3])]) for o in obstacleList])

    # これ以降は障害物の追加をしても反映されないので注意
    sim.processObstacles()

    # for (i,agent) in enumerate(agents):
    #     # print(agentdest[i])
    #     dest, vPref, destPos[i] = setvPref(agent,agentDest[i],destination)
    #     agentDest[i] = dest
    #     sim.setAgentPrefVelocity(agent, vPref)
    #     print("\ndestination : ", destination[dest[0]][dest[1]][2:3], "vPref : ", vPref)
    #     sim.setAgentPrefVelocity(agent, vPref)
    #     print(sim.getAgentVelocity(agent))


    print('Simulation has %i agents and %i obstacle vertices in it.' %
          (sim.getNumAgents(), sim.getNumObstacleVertices()))

    print('Running simulation')

    # for step in range(200):
    step = 0
    stopCount = 0
    loopFlag = True
    prevPos = None
    while(loopFlag):

        # 目的地点に十分近い場合は目標の更新・そうでない場合は速度の更新を行う
        for (i,agent) in enumerate(agents):
            agentDest[i], vPref, destPos[i]= setvPref(agent,agentDest[i],destination)

            print("\ndestination : ", destination[agentDest[i][0]][agentDest[i][1]], "vPref : ", vPref)
            # print("\ndestination : ", destination[dest[0]][dest[1]], "vPref : ", vPref)
            sim.setAgentPrefVelocity(agent, vPref)
            # print(sim.getAgentVelocity(agent))

        # 全てのagentに対してdestPosがない(次の目標地点がない)場合はシミュレーションを終了
        flag = False
        for agentNo in agents:
            if destPos[agentNo] is not None:
                flag = True
        # if step > 1000:
        #     flag = False
        loopFlag = flag
        print("DestPos...", destPos)

        positions = ['(%5.3f, %5.3f)' % sim.getAgentPosition(agentNo)
                     for agentNo in agents]
        print('step=%2i  t=%.3f  %s' % (step, sim.getGlobalTime(), '  '.join(positions)))
        agentsPos = [(sim.getAgentPosition(agentNo)[0], sim.getAgentPosition(agentNo)[1]) for agentNo in agents]
        agentsPosx = [sim.getAgentPosition(agentNo)[0] for agentNo in agents]
        agentsPosy = [sim.getAgentPosition(agentNo)[1] for agentNo in agents]

        # agentsDestPos = [(destPos[agentNo][0], destPos[agentNo][1]) if destPos[agentNo] is not None else None for agentNo in agents]
        agentsDestPosx = [destPos[agentNo][0] if destPos[agentNo] is not None else float(destination[agentNo][-1][2]) for agentNo in agents]
        agentsDestPosy = [destPos[agentNo][1] if destPos[agentNo] is not None else float(destination[agentNo][-1][3]) for agentNo in agents]
        agentsDestAttribute = [["Pick", destination[agentDest[agentNo][0]][agentDest[agentNo][1]][-2][0].split(".")[0]]
                                if destination[agentDest[agentNo][0]][agentDest[agentNo][1]][-1] == "Pick"
                                else ["Turn", "None"] for agentNo in agents]
        # agentsDestPos = [(posx, posy) for (posx, posy) in zip(agentsDestPosx, agentsDestPosy)]
        agentsDestPos = [(posx, posy) for (posx, posy) in zip(agentsDestPosx, agentsDestPosy)]
        for agentNo in agents:
            # print([agentsPos[agentNo], agentsDestPos[agentNo]])
            pickleDumpArray[agentNo].append([step, sim.getGlobalTime(), agentsPos[agentNo], agentsDestPos[agentNo], agentsDestAttribute[agentNo]])

        if prevPos == agentsPos:
            stopCount += 1
            if stopCount > 10 / STEP_TIME:
                print("There was no movement for 10sec. Finish simulation")
                break
        prevPos = agentsPos


        if drawFlag:
            # x1 = [float(data[2]) for data in destination[0]]
            # y1 = [float(data[3]) for data in destination[0]]
            pt1 = ax1.scatter(agentsPosx,agentsPosy,color = '#c92b29', s=8)
            pt2 = ax1.scatter(agentsDestPosx,agentsDestPosy,color = '#2ca9e1', s=5)

            objs.append([pt1,pt2])

        step += 1

        # RVO2の1ステップ更新
        sim.doStep()


    if not os.path.isdir(os.path.join(DATA_PATH, "simulation")):
        os.mkdir(os.path.join(DATA_PATH, "simulation"))
    for agentNo in agents:
        print(writeFileNameList)
        with open(os.path.join(DATA_PATH, "simulation/"+ writeFileNameList[agentNo]+ "_simulatedTrajectory.pickle"), mode = 'wb') as f:
            pickle.dump(pickleDumpArray[agentNo], f)

    if drawFlag:
        interval = 100 * STEP_TIME
        ani = animation.ArtistAnimation(fig, objs, interval = 50, repeat = True)
        plt.show()

if __name__ == '__main__':
    drawFlag = True
    run(drawFlag)
