#!/usr/bin/python
# -*- coding: utf-8 -*-

# import copy
import os
# import matplotlib.pyplot as plt
import pickle
# import itertools
import csv
# from operator import attrgetter
import numpy as np
import sys

# 自作クラスファイルの読み込み(相対パス)
# from classdef import nodeDef_v2
# from classdef import pathDef
# from classdef import gridMapDef
import searchShelf
import configDef
import fileIO

conf = configDef.Config()
DATA_PATH = conf.dataPath

def run(dataPath, userNum, useBLEFlag):
    pathList = []

    with open(os.path.join(dataPath,"wms/WMS_comb_all.csv"),"r") as f:
        wmsAllData = csv.reader(f)
        header = next(wmsAllData)
        for line in wmsAllData:
            if int(line[0]) == userNum:
                uTime = float(line[2])
                shelfID = line[1]
                shelfPos = searchShelf.run(shelfID, dataPath)
                pathList.append([uTime, shelfPos[0], shelfPos[1],"pass"])

    if useBLEFlag:
        with open(os.path.join(dataPath,"wms/nearFromBLE"+str(userNum)+".csv"),"r") as f:
            bleAllData = csv.reader(f)
            for line in wmsAllData:
                uTime = float(line[0])
                xpos = float(line[1])
                ypos = float(line[2])
                action = line[3]
                pathList.append([uTime, xpos, ypos, action])

    # リストを時間でソート
    pathList.sort(key=lambda x:x[0])

    outputList = []
    outputList.append([conf.startPoint[0], conf.startPoint[1], "start"])
    for line in pathList:
        outputList.append([line[1], line[2], line[3]])
        print(line[1], line[2], line[3])

    with open(os.path.join(dataPath,"wms/pathList"+str(userNum)+".csv"), "w") as f:
        csvWriter = csv.writer(f)
        csvWriter.writerows(outputList)
        print("pathList is saved as", os.path.join(dataPath,"wms/pathList"+str(userNum)+".csv"))
        f.close()


if __name__ == '__main__':
    dataPath = DATA_PATH
    useBLEFlag = False
    userNum = 4
    run(dataPath, userNum, useBLEFlag)
