#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math
import copy
import numpy as np
import os
import matplotlib.pyplot as plt
import pickle

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")



def run():
    mapInfoList = []
    obstacles = []
    shelf = []
    ble = []

    # pickleファイルからオブジェクトを復元
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'ble.pickle'), mode = 'rb') as f:
        ble = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    cellSize = gridMap.gridmap[0][1].posx - gridMap.gridmap[0][0].posx
    print("cellSize", cellSize)

    xlim = mapInfoList[0]
    ylim = mapInfoList[1]
    print("mapSize...", xlim,ylim)

    obstacleList = []
    obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
    obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
    print("obstacles...",len(obstacleList))


    # path = []
    # with open(os.path.join(DATA_PATH, "goal_path.txt"), 'r') as f:
    #     lines = f.readlines()
    #     print(lines[0].split(" "))
    #     print(lines[0].split(" ")[4].split("("))
    #
    #     for line in lines:
    #         x = line.split(" ")[4].split("(")[1].split(")")[0].split(",")[0]
    #         y = line.split(" ")[4].split("(")[1].split(")")[0].split(",")[1]
    #         path.append([x,y])

    # 表示用pltの設定
    fig, (ax1) = plt.subplots(1,1,figsize=(11,7.5))
    ax1.set_xlim(xlim[0]-10,xlim[1]+10)
    ax1.set_ylim(ylim[0]-10,ylim[1]+10)
    # fig, (ax1) = plt.subplots(1,1,figsize=(7.5,11.5))
    # ax1.set_xlim(30,80)
    # ax1.set_ylim(-5,65)


    # ax1.tick_params(labelbottom="off",bottom="off") # x軸の削除
    # ax1.tick_params(labelleft="off",left="off") # y軸の削除


    color = '#2ca9e1'

    for (x1, x2, y1, y2) in obstacleList:
        ax1.add_patch(plt.Rectangle((x1,y1),x2-x1,y2-y1, fc ='#000000'))
        # plt.plot([x1,x2],[y1,y1] , color = '#000000')
        # plt.plot([x2,x2],[y1,y2] , color = '#000000')
        # plt.plot([x2,x1],[y2,y2] , color = '#000000')
        # plt.plot([x1,x1],[y2,y1] , color = '#000000')

    pointList = [(54.7, 49.0), (38.8, 5.2), (34.9, 4.9), (42.4, 9.7),
                (38.2, 2.5), (35.8, 7.3), (29.2, 10.9), (49.0, 5.2),
                (35.8, 9.7), (52.6, 14.8), (99.4, 4.3), (56.2, 4.9),
                (51.4, 5.2), (71.5, 26.5), (66.1, 24.4), (69.7, 26.5)]
    pointListx = [point[0] for point in pointList]
    pointListy = [point[1] for point in pointList]

    ax1.scatter(pointListx, pointListy)
    # for (i,node) in enumerate(path):
    #     if i == 0:
    #         prevNode = node
    #         continue
    #     else:
    #         plt.plot([node[0], prevNode[0]], [node[1], prevNode[1]], color = "#c92b29")
    #         prevNode = node

    # for (i,node) in enumerate(path[0].route):
    #     if i == 0:
    #         prevNode = node
    #         continue
    #     else:
    #         plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#c92b29')
    #         prevNode = node
    #
    # for (i,node) in enumerate(path[1].route):
    #     if i == 0:
    #         prevNode = node
    #         continue
    #     else:
    #         plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#2ca9e1')
    #         prevNode = node
    #
    # for (i,node) in enumerate(path[2].route):
    #     if i == 0:
    #         prevNode = node
    #         continue
    #     else:
    #         plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#00a1e9')
    #         prevNode = node

    plt.show()
    # plt.savefig("/Users/nouchi/Desktop/figure.png")
if __name__ == '__main__':
    run()
