#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math
import copy
import numpy as np
import os
import matplotlib.pyplot as plt
import pickle
import itertools

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
from classdef import bleDef

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

def run():
    mapInfoList = []
    obstacles = []
    shelf = []


    # # pickleファイルからオブジェクトを復元
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0]
    ylim = mapInfoList[1]
    print("mapSize...", xlim,ylim)
    print("obstacles", len(obstacles))
    print("shelves", len(shelf))

    # 別であらかじめ作成しておいたgridMapをロード
    # makeOccupancyGrid.py
    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)

    cellSize = gridMap.gridmap[0][1].posx - gridMap.gridmap[0][0].posx

    # 表示用pltの設定
    print("draw grid map...\n")
    fig, (ax1) = plt.subplots(1,1,figsize=(7.5,11.5))
    ax1.set_xlim(30,80)
    ax1.set_ylim(0,65)
    # fig, (ax1) = plt.subplots(1,1,figsize=(11,7.5))
    # ax1.set_xlim(xlim[0]-10,xlim[1]+10)
    # ax1.set_ylim(ylim[0]-10,ylim[1]+10)
    print("xlim", xlim[0],"  ",xlim[1])
    print("ylim", ylim[0],"  ",ylim[1])


    obstacleCount = 0
    shelfCount = 0

    obstacleColor = '#2ca9e1'
    shelfColor = '#2ca9e1'
    # shelfColor = '#c92b29'
    passageColot = '#ffffff'
    lineWidth = '0.3'

    for i, j in itertools.product(range(len(gridMap.gridmap)), range(len(gridMap.gridmap[0]))):
        node = gridMap.gridmap[i][j]

        x = node.posx
        y = node.posy

        if j == 0:
            print(i," / ",len(gridMap.gridmap))
        # if node.isShelf:
        if node.isObstacle:
            obstacleCount += 1
            rect = plt.Rectangle(node.ld, cellSize, cellSize,fc=obstacleColor, ec='#303030', lw=lineWidth)
            ax1.add_patch(rect)
        elif node.isShelf:
            shelfCount += 1
            rect = plt.Rectangle(node.ld, cellSize, cellSize,fc=shelfColor, ec='#303030', lw=lineWidth)
            ax1.add_patch(rect)
        else:
            rect = plt.Rectangle(node.ld, cellSize, cellSize,fc=passageColot, ec='#303030', lw=lineWidth)
            ax1.add_patch(rect)
        # else :
        #     if node.cost < 1.1:
        #         color = '#0000bb'
        #     elif node.cost < 1.5:
        #         color = '#33bbbb'
        #     elif node.cost < 2.0:
        #         color = '#00bb00'
        #     elif node.cost < 2.5:
        #         color = '#bbbb33'
        #     elif node.cost < 3.0:
        #         color = '#bb3300'
        #     else:
        #         color = '#bb0000'


    print("obstacle is ", obstacleCount)
    print("shelf is ", shelfCount)
    # pickupPoint = (52.626, 40.8105)
    # pt1 = plt.scatter(pickupPoint[0],pickupPoint[1],color = '#2ca9e1')

    plt.show()


if __name__ == '__main__':

    run()
