import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os
import pickle
import csv
import datetime
import time

import searchShelf
import drawResult
import animateResult
import fileIO

AGENT_NO = 4
RESULT_FREAQ = 1
MAX_VELOCITY = 1.5
MIN_VELOCITY = 0.3

TIME_THRESHOLD = 14852405450
# TIME_THRESHOLD = 1485240545 + 600

# startTime / wearingStart / endTime
TIME_TABLE_LIST = [ [], [1485239145.,1485240508.,1485252057.],
                    [1485239148.,1485240513.,1485248722.],
                    [1485239234.,1485240524.,1485252460.],
                    [1485239243.,1485240545.,1485252313.],
                    [1485239286.,1485240550.,1485252407.],
                    [1485239291.,1485240555.,1485252344.],
                    [1485239328.,1485240562.,1485252227.],
                    [1485239334.,1485240568.,1485252085.]]

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# picklePath = os.path.join(dataPath, "pickle")

class moveBetweenPick(object):
    def __init__(self, startShelfID, endShelfID, startTime, endTime, startStep, endStep, moveTime, startPos, endPos, action):
        self.startShelfID  = startShelfID
        self.endShelfID = endShelfID
        self.startTime = startTime
        self.endTime = endTime
        self.deffTime = endTime - startTime
        self.startStep = startStep
        self.endStep = endStep
        self.deffStep = endStep - startStep
        self.moveTime = moveTime
        self.startPos = startPos
        self.endPos = endPos
        self.action = action
        # self.setpTime = deffStep / moveTime

def overVelCheck(resultList, dataPath, agentNo, date, addList):
    timeRange = 1 / RESULT_FREAQ
    maxStepRange = MAX_VELOCITY * timeRange

    overVelList = []
    prevStep = None
    for step in resultList:
        if prevStep == None:
            prevStep = step
        prevTime = float(prevStep[2])
        utime = float(step[2])
        deffTime = utime - prevTime
        prevPos = np.array((float(prevStep[0]), float(prevStep[1])))
        pos = np.array((float(step[0]), float(step[1])))
        deffPos = np.linalg.norm(pos - prevPos)
        maxStepRange = MAX_VELOCITY * deffTime
        minStepRange = MIN_VELOCITY * deffTime
        if maxStepRange < deffPos:
            # print(prevPos, pos, prevTime, utime, deffTime, maxStepRange, deffPos, deffPos/deffTime, "over")
            overVelList.append([prevTime,utime,"overSpeed"])
        if deffPos < minStepRange and deffPos != 0:
            # print(prevPos, pos, prevTime, utime, deffTime, maxStepRange, deffPos, "slow")
            overVelList.append([prevTime,utime,"slowSpeed"])

            # print("hogehoge\n\n")
        prevStep = step
    # date = time.strftime("%m%d_%H%M", time.localtime())
    overVelList.extend(addList)
    overVelList.sort(key=lambda x:x[0])
    if not os.path.exists(os.path.join(dataPath,"csv/overSpeed"+date)):
        os.mkdir(os.path.join(dataPath,"csv/overSpeed"+date))
    filename = os.path.join(dataPath,"csv/overSpeed"+date+"/overSpeed_"+str(agentNo)+"_"+date+".csv")

    with open(filename, "w") as f:
        dataWriter = csv.writer(f)
        dataWriter.writerows(overVelList)


def isMove(utime, moveList):
    result = False
    for move in moveList:
        # print(move)
        if move[0] < utime and utime < move[1]:
            if move[-1] == "move":
                result = True
            else:
                result = False
            break
    # print(result)
    return result

def run(dataPath ,drawFlag, agentNo, date):
    picklePath = os.path.join(dataPath, "pickle")

    # print(writeFileNameList)
    # pickleファイルからオブジェクトを復元
    with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)
    print (mapInfoList)
    xlim = mapInfoList[0]
    ylim = mapInfoList[1]
    startPoint = mapInfoList[2]

    with open(os.path.join(dataPath, "simulation/"+ str(agentNo)+ "_simulatedTrajectory.pickle"), mode = 'rb') as f:
        agentTrajectory = pickle.load(f)
    with open(os.path.join(picklePath, "pickStepList"+str(agentNo)+".pickle"), mode = 'rb') as f:
        pickList = pickle.load(f)
    # print(pickList)

    # for line in pickList:
    #     print(line)
    writeFileNameList = []
    destPos = []
    pickleDumpArray = []
    agentMoveList = []
    wmsList = []

    # startTime =TIME_TABLE_LIST[agentNo]
    # with open(os.path.join(dataPath, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'rb') as f:
    # # with open(os.path.join(dataPath, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'rb') as f:
    #     wayPointList = pickle.load(f)
    #     writeFileNameList[agentNo].append(str(userNum))
    # distNo = 0
    # agents.append(sim.addAgent(startPoint))
    # agentDest.append([agentNo, distNo])
    # destination.append(wayPointList)
    # destPos.append([[float(destination[agentNo][distNo][2]), float(destination[agentNo][distNo][3])]])
    # pickleDumpArray.append([0, 0.0, (startPoint[0], startPoint[1]), (float(destination[agentNo][distNo][2]), float(destination[agentNo][distNo][3])), ["Start", "None"]])

    with open(os.path.join(picklePath, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(picklePath, 'gridMap.pickle'))
    # print(gridMap)
    cellSize = gridMap.gridmap[0][1].posx - gridMap.gridmap[0][0].posx

    with open(os.path.join(dataPath, 'labels/createdMoveLabel_mod'+str(agentNo)+'.csv'), mode = 'r') as f:
    # with open(os.path.join(dataPath, 'labels/prev1/allLabel'+str(agentNo)+'.csv'), mode = 'r') as f:
        moveData = csv.reader(f)
        header = next(moveData)
        header = next(moveData)
        for line in moveData:
            startTime = float(line[0])
            endTime = float(line[1])
            action = line[2]
            # print(line, endTime - startTime)
            agentMoveList.append([startTime, endTime, action])
    # リストを時間でソート
    agentMoveList.sort(key=lambda x:x[0])
    startTime = agentMoveList[0][0]
        # startTime = agentMoveList[0][0] if agentMoveList[0][0] < TIME_TABLE_LIST[agentNo][1] else TIME_TABLE_LIST[agentNo][1]
    print(len(agentMoveList))

    # with open(os.path.join(dataPath,"wms/WMS_comb.csv"), 'r') as f:
    #     wmsData = csv.reader(f)
    #     # header行を読み飛ばす
    #     header = next(wmsData)
    #     next(wmsData)
    #     for line in wmsData:
    #         terminalID = line[0]
    #         shelfNO = line[1]
    #         utime = int(line[2])
    #         if int(terminalID) == agentNo:
    #             if utime < startTime:
    #                 # print(shelfNO, utime, "xxxx")
    #                 continue
    #             wmsList.append([shelfNO, utime])
    #
    # print("length of wmsList of agent", agentNo, "...", len(wmsList))
    # # リストを時間でソート
    # wmsList.sort(key=lambda x:x[1])

    # wmsList = fileIO.loadWms(os.path.join(dataPath,"wms/WMS_comb_all.csv"), agentNo, startTime)
    # with open(os.path.join(dataPath,"wms/WMS_comb.csv"), 'r') as f:
    #     wmsData = csv.reader(f)
    #
    #     # header行を読み飛ばす
    #     header = next(wmsData)
    #     next(wmsData)
    #
    #     for line in wmsData:
    #         terminalID = line[0]
    #         shelfNO = line[1]
    #         utime = int(line[2])
    #
    #         if int(terminalID) == agentNo:
    #             if utime < startTime:
    #                 continue
    #             wmsList.append([utime, shelfNO, "pick"])

    pathList = fileIO.loadPassList(os.path.join(dataPath,"wms/pathList"+str(agentNo)+".csv"))

    # passList = fileIO.loadPassList(os.path.join(dataPath,"wms/nearFromBLE"+str(agentNo)+".csv"))
    # passList = []
    # with open(os.path.join(dataPath,"wms/nearFromBLE"+str(agentNo)+".csv"), 'r') as f:
    #     pList = csv.reader(f)
    #     header = next(pList)
    #     for line in pList:
    #         utime = float(line[0])
    #         posx = float(line[1])
    #         posy = float(line[2])
    #         action = line[3]
    #         if action == "Pass":
    #             passList.append([utime, (posx, posy), action])

    # リストを時間でソート
    # wmsList.sort(key=lambda x:x[0])
    eventList = []
    eventList.extend(wmsList)
    eventList.extend(passList)
    eventList.sort(key=lambda x:x[0])
    print("length of eventList...", len(eventList))

    # for line in eventList:
        # print(line)
    moveBetweenPickList = []
    prevEvent = None
    # prevWms = None
    # startShelfID = None
    # endShelfID = None
    # for wms in wmsList:
    noMoveTimeList = []
    for event in eventList:
        # print(event)
        if prevEvent is None:
            prevEvent = [startTime, startPoint, "Pass"]
            continue
        startTime = prevEvent[0]
        endTime = event[0]
        deffTime = endTime - startTime
        startStep = None
        endStep = None
        # startShelfID = searchShelf.run(int(prevEvent[1]),dataPath)if type(prevEvent[1]) == str else (int(prevEvent[1][0]), int(prevEvent[1][1]))
        # endShelfID = searchShelf.run(int(event[1]),dataPath) if type(event[1]) == str else (int(event[1][0]), int(event[1][1]))

        startShelfID = int(prevEvent[1]) if type(prevEvent[1]) == str else (int(prevEvent[1][0]), int(prevEvent[1][1]))
        endShelfID = int(event[1]) if type(event[1]) == str else (int(event[1][0]), int(event[1][1]))
        # startShelfID = (round(float(prevEvent[1][0]),1),round(float(prevEvent[1][1]),1))
        # endShelfID = (round(float(event[1][0]),1),round(float(event[1][1]),1))
        prevAction = prevEvent[2]
        action = event[2]
        prevEvent = event
        # print(startTime, endTime, deffTime, startShelfID, endShelfID)
        for line in pickList:
            # print(line)
            # if startShelfID == 707 :
            #     print(line,startShelfID, endShelfID,"707")

            if type(line[1]) == str and len(line[1].split(" ")) > 1:
                line[1] = (float(line[1].split(" ")[0][1:]), float(line[1].split(" ")[1][:-1]))
            elif type(line[1]) == str:
                line[1] = float(line[1])
                line[1]= int(line[1])
                # print(tmp)
                # line[1] = searchShelf.run(int(tmp),dataPath)
                # line[1] = (gridMap[searchShelf.run(int(line[1]),dataPath)[1]][searchShelf.run(int(line[1]),dataPath)[0]].posx, gridMap[searchShelf.run(int(line[1]),dataPath)[1]][searchShelf.run(int(line[1]),dataPath)[0]].posy)
            # lineID = int(line[1]) if type(line[1]) == int else (int(line[1][0]), int(line[1][1]))
            # if np.linalg.norm(np.array(startShelfID) - np.array(line[1])) < 1 and startStep is None:
            #     startStep = line[0]
            # if np.linalg.norm(np.array(endShelfID) - np.array(line[1])) < 1 and startStep is not None and startStep <= line[0]:
            #     endStep = line[0]
            # if endStep is not None and startStep is not None:
            #     break
        # print(startStep, endStep, startTime, endTime, startShelfID, endShelfID)

            if type(line[1]) == str and len(line[1].split(" ")) > 1:
                line[1] = (float(line[1].split(" ")[0][1:]), float(line[1].split(" ")[1][:-1]))
            elif type(line[1]) == str:
                line[1] = float(line[1])
                line[1] = int(line[1])
            lineID = int(line[1]) if type(line[1]) == int else (int(line[1][0]), int(line[1][1]))
            if startShelfID == lineID and startStep is None:
                startStep = line[0]
            if endShelfID == lineID and startStep is not None and startStep <= line[0]:
                endStep = line[0]
            if endStep is not None and startStep is not None:
                break
        # print(startStep, endStep, startTime, endTime, startShelfID, endShelfID)
        if endStep is None or startStep is None:
            continue
        deffStep = endStep - startStep

        moveTime = 0
        for line in agentMoveList:
            # if line[0] < time and time < line[1]:
                # action = line[2]
            if line[2] == "move" or line[2] == "Rturn_m" or line[2] == "Lturn_m" or line[2] == "Uturn":
                if startTime < line[1] and line[1] < endTime:
                    moveTime += line[1] - startTime if line[0] < startTime else line[1] - line[0]
                elif startTime < line[0] and line[0] < endTime:
                    moveTime += endTime - line[0] if endTime < line[1] else line[1] - line[0]
                elif line[0] < startTime and endTime < line[1]:
                    moveTime += endTime - startTime

        # print(startTime, endTime, moveTime, endTime - startTime, "fuga")
        if moveTime == 0 and endTime - startTime > 3:
            print("xxxxxx")
            noMoveTimeList.append([startTime, endTime, "overSpeed"])
        # print(startStep, endStep, startShelfID, endShelfID, len(agentTrajectory), agentTrajectory[-1])
        startPos = agentTrajectory[startStep + 1][2]
        endPos = agentTrajectory[endStep +1][2]
        # print(startShelfID, endShelfID, startTime, endTime, startStep, endStep, moveTime, startPos, endPos, action,"\n")
        moveBetweenPickList.append(moveBetweenPick(startShelfID, endShelfID, startTime, endTime, startStep, endStep, moveTime, startPos, endPos, action))
        # prevWms = wms

    # resultList = [["x","y","unixtime"]]
    resultList = []
    prevMove = None
    # if moveBetweenPickList[0].startTime < TIME_TABLE_LIST[agentNo][1]:
    uxtime = agentMoveList[0][0]
    # time = TIME_TABLE_LIST[agentNo][1]
    while (True):
        pos = moveBetweenPickList[0].startPos
        resultList.append([pos[0],pos[1],uxtime])
        uxtime += 1/ RESULT_FREAQ
        if moveBetweenPickList[0].startTime < uxtime:
            break

    # prevMove = moveBetweenPickList[0]
    for move in moveBetweenPickList:
        # print(move.startShelfID, move.endShelfID, move.startTime, move.endTime, move.moveTime)
        # if move.startTime <= 1485246586 + 10 and 1485246365 - 10 <= move.startTime:
        #     print(move, "piyo\n")
        # else:
        #     continue
        # if move.startShelfID == "707" or move.startShelfID == 707:
        #     print("\n\n\n",move,"707\n\n\n")
        if prevMove is None:
            prevMove = move
        else:
            stopTimeStart = prevMove.endTime
            # stopTimeStart = prevMove.endTime if TIME_TABLE_LIST[agentNo][1] < prevMove.endTime else TIME_TABLE_LIST[agentNo][1]
            stopTimeEnd = move.startTime
            stopStartPos = prevMove.endPos
            stopEndPos = move.startPos
            stopTimeRange = stopTimeEnd - stopTimeStart
            utime = float(stopTimeStart)
            # # startとendで場所が違ったら等速移動させるのもアリよね
            if stopEndPos != stopStartPos:
                deffTime = stopTimeEnd - stopTimeStart
                deffStep = prevMove.endStep - move.startStep
                stepRange = int(deffTime * RESULT_FREAQ) + 1
                stepInterval = deffStep / stepRange
            stepCount = 0


            if TIME_THRESHOLD < stopTimeEnd:
                continue


            while(utime < stopTimeEnd):
                stepCount += 1
                step = move.startStep + int(stepCount * stepInterval + 0.5)
                if step < len(agentTrajectory) -1:
                    pos = agentTrajectory[step + 1][2]
                # if not TIME_TABLE_LIST[agentNo][2] < utime:
                resultList.append([pos[0],pos[1],utime])
                utime += 1 / RESULT_FREAQ
        pathRange = 0
        prevStepPos = np.array(move.startPos)
        startTime = move.startTime
        # startTime = move.startTime if TIME_TABLE_LIST[agentNo][1] < move.startTime else TIME_TABLE_LIST[agentNo][1]
        pos = move.startPos
        # if not TIME_TABLE_LIST[agentNo][2] < utime:
        #     resultList.append([pos[0], pos[1], float(startTime)])

        utime = startTime + 1/ RESULT_FREAQ
        # flag = True
        stepCount = 0
        step = move.startStep
        prevPos = move.startPos
        endTime = move.endTime
        for step in range(move.startStep, move.endStep):
            # print(agentTrajectory[step][2],prevStepPos, type(agentTrajectory[step][2]), type(prevStepPos))
            pathRange += np.linalg.norm(np.array(agentTrajectory[step][2]) -  prevStepPos)

        # print("hoge  ",pathRange/move.moveTime if move.moveTime != 0 else "hoges")
        if move.moveTime == 0 or pathRange/move.moveTime < 1.5:
            stepRange = int(move.moveTime * RESULT_FREAQ) + 1
            stepInterval = move.deffStep / stepRange
            stepCount = 0
            while (True): #間を埋める処理
                # print(time, step, pos)
                if isMove(utime, agentMoveList):
                    stepCount += 1
                    step = move.startStep + int(stepCount * stepInterval + 0.5)
                    if step < len(agentTrajectory) -1:
                        pos = agentTrajectory[step + 1][2]
                        # print(pos[0],pos[1],time, step, stepInterval)
                    # if stepRange < stepCount:
                        # continue
                # for stepCount in range(stepRange):
                #     time = startTime + stepCount / RESULT_FREAQ
                #     step = move.startStep + stepCount
                #         break
                #     pos = agentTrajectory[step + 1][2]
                #     resultList.append([pos[0], pos[1], time])
                #     print(pos[0], pos[1], time)
                # if not TIME_TABLE_LIST[agentNo][2] < utime:
                # if 1485244120 < utime and utime < 1485244330:
                    # print([pos[0],pos[1], float(utime)])

                resultList.append([pos[0],pos[1], float(utime)])

                utime += 1 / RESULT_FREAQ
                if move.endTime < utime or move.endStep < step :
                    break
                # if endTime < time:
                #     flag = False
                #     continue

        else:
            stepCount = 0
            stepRange = int(move.endTime - move.startTime * RESULT_FREAQ) + 1
            stepInterval = move.deffStep / stepRange

            while (True):
                # if isMove(utime, agentMoveList):
                stepCount += 1
                step = move.startStep + int(stepCount * stepInterval + 0.5)
                if step < len(agentTrajectory) -1:
                    pos = agentTrajectory[step + 1][2]
                # if not TIME_TABLE_LIST[agentNo][2] < utime:
                # if 1485244120 < utime and utime < 1485244330:
                    # print([pos[0],pos[1], float(utime)])
                resultList.append([pos[0],pos[1], float(utime)])

                utime += 1 / RESULT_FREAQ
                if move.endTime < utime or move.endStep < step :
                    break
            print(stepCount, stepRange, stepInterval)
        print(move.startShelfID, move.endShelfID, move.startTime, move.endTime, move.moveTime)
        print("   ",stepRange, stepInterval,startTime, endTime)

        # if not TIME_TABLE_LIST[agentNo][2] < utime:
        #     resultList.append([move.endPos[0], move.endPos[1], float(endTime)])
        prevMove = move
    # print(type(l) for l in resultList)

    # date = time.strftime("%m%d_%H%M", time.localtime())


    resultList.sort(key=lambda x:x[2])
    tempList = []
    prevLine = None
    for line in resultList:
        if prevLine is None:
            prevLine = line
            continue
        if prevLine[2] == line[2]:
            continue
        tempList.append(line)
        prevLine = line

    prevTime = 0
    for line in tempList:
        utime = line[2]
        # if utime < prevTime:
        prevTime = utime





    # overVelCheck(tempList, dataPath, agentNo, date, noMoveTimeList)

    # animateResult.run(dataPath, drawFlag, agentNo, date, resultList)
    drawResult.run(dataPath, drawFlag, agentNo, date, resultList)


    # with open(os.path.join(picklePath, "hoge.pickle"), "wb") as f:
    #     pickle.dump(resultList,f)
    # if not os.path.exists(os.path.join(dataPath,"result/sim_result"+date)):
    #     os.mkdir(os.path.join(dataPath,"result/sim_result"+date))
    # filename = os.path.join(dataPath,"result/sim_result"+date+"/Terminal"+str(agentNo)+"_"+date+".csv")
    # with open(filename,"w") as f:
    #     dataWriter = csv.writer(f)
    #     dataWriter.writerow(["x","y","unixtime"])
    #     prevLine = None
    #     for line in tempList:
    #         # if TIME_TABLE_LIST[agentNo][1] < line[2] and line[2] < TIME_TABLE_LIST[agentNo][2]:
    #         if prevLine is None:
    #             dataWriter.writerow(line)
    #             prevLine = line
    #         elif prevLine[2] != line[2] and prevLine[2] < line[2]:
    #             dataWriter.writerow(line)
    #
    #         # elif prevLine[2] != line[2]:
    #         #     print("1")
    #         #     if prevLine[2] < line[2]:
    #         #         print("2")
    #         #         # print(TIME_TABLE_LIST[agentNo], line[2])
    #         #         if TIME_TABLE_LIST[agentNo][1] < line[2]:
    #         #             print("3")
    #         #             if line[2] < TIME_TABLE_LIST[agentNo][2]:
    #         #                 print("4")
    #         # print(prevLine[2], line[2], TIME_TABLE_LIST[agentNo], "time")
    #         prevLine = line
    #     # dataWriter.writerows(tempList)

    # if drawFlag:


if __name__ == '__main__':
    drawFlag = True
    date = time.strftime("%m%d_%H%M", time.localtime())
    run(DATA_PATH, drawFlag, AGENT_NO, date)
