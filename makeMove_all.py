import os
import time

import makeMove

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
date = time.strftime("%m%d_%H%M", time.localtime())

runList = [2,3,4,5,6,7,8]
for agentNo in runList:
    makeMove.run(DATA_PATH, False, agentNo, date)

# if __name__ == '__main__':
#     drawFlag = False
#     run(DATA_PATH, drawFlag, AGENT_NO)
