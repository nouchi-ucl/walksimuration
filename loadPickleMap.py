#!/usr/bin/env python
import rvo2
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os
import pickle

# pathの設定(直打ちです...)
# MAP_PATH = os.path.join("/media/sf_Ubuntu-simpy/workspace/sample_dataset_20170602/WarehouseB_30min/map")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

# 読み込み用リスト マップ外周
mapInfoList = []
#  読み込み用リスト 棚
shelfList = []
#  読み込み用リスト 障害物
obstacleList = []

# 表示用リスト 棚/障害物
# 命名適当すぎて...
obstacles = []

# pickleファイルからオブジェクトを復元
with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
    mapInfoList = pickle.load(f)

with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
    shelfList = pickle.load(f)

with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
    obstacleList = pickle.load(f)

print (mapInfoList)
xlim = mapInfoList[0][0]
ylim = mapInfoList[0][1]

# 表示用pltの設定
fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
ax1.set_xlim(xlim[0]-10,xlim[1]+10)
ax1.set_ylim(ylim[0]-10,ylim[1]+10)
print("xlim", xlim[0],"  ",xlim[1])
print("ylim", ylim[0],"  ",ylim[1])
objs = []


# sim環境の宣言
sim = rvo2.PyRVOSimulator(1/5., 1., 5, 1.5, 2, 0.4, 2)
# マップ外周を障害物として追加
obstacles.append(sim.addObstacle([(xlim[0],ylim[0]),(xlim[1],ylim[0]),(xlim[1],ylim[1]),(xlim[0],ylim[1])]))

obstacles.append([sim.addObstacle([(s[1],s[3]),(s[2],s[3]),(s[2],s[4]),(s[1],s[4])]) for s in shelfList])

obstacles.append([sim.addObstacle([(o[0],o[2]),(o[1],o[2]),(o[1],o[3]),(o[0],o[3])]) for o in obstacleList])

sim.processObstacles()

# pltで確認用に描画
# print("Num of vertices ", sim.getNumObstacleVertices())
# for i in range(0,sim.getNumObstacleVertices()):
# 	pos = []
# 	pos.append(sim.getObstacleVertex(i))
# 	pos.append(sim.getObstacleVertex(sim.getNextObstacleVertexNo(i)))
# 	ax1.plot([p[0] for p in pos], [p[1] for p in pos], color = '#000000')
# plt.show()
