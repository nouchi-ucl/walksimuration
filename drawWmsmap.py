#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
import csv
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import pathPlanning_v2
import searchShelf

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")


def run(pathfName):
    with open(os.path.join(DATA_PATH,"wms/WMS.csv"), 'r') as f:
        wmsData = csv.reader(f)

        # header行を読み飛ばす
        header = next(wmsData)
        next(wmsData)

        wmsList = []
        for line in wmsData:
            print(line)
            terminalID = line[0]
            shelfNO = line[1]
            utime = line[2]

            if int(terminalID) == userNum:
                wmsList.append([shelfNO, utime])

            # 邪魔な空行を読み飛ばす
            next(wmsData)

    # リストを時間でソート
    wmsList.sort(key=lambda x:x[1])

    startPos = None
    startPos = (43.3,46.6)
    # pathList = []
    # prevPos = None
    # prevPos = startPos
    # wmsList = [1157, 1118, 1013, 999, 990, 985, 225, 258, 1196, 1122, 1124, 812, 1008, 708, 709, 738, 726, 1002, 1007, 765, 709, 717, 716, 765, 765, 717, 738, 1004, 207, 708]
    # for (i, wms) in enumerate(wmsList):
    #     # if i >= len(wmsList) -1:
    #     if i > 10:
    #         break
    #     # スタート地点が指定されていない,かつリストの先頭の場合
    #     if prevPos is None:
    #         prevPos = searchShelf.run(wms)
    #         continue
    #     currentPos = searchShelf.run(wms)
    #     print("search path to ", prevPos, currentPos, "\n    This path is ", i, " of ", len(wmsList) -1)
    #     writeName = pathPlanning_v2.run(prevPos, currentPos, False)
    #     print("write to ", writeName)
    #     prevPos = currentPos
    #     pathList.append(os.path.join(DATA_PATH,writeName))

    # wayPointList = []
    # for path in pathList:
    #     with open(path, 'r') as f:
    #         pathLines = f.readlines()
    #     for line in pathLines:
    #         data = line.split(",")
    #         if data[-1] == "True":
    #             wayPointList.append(data)

    # # txtとpickleで出力
    # with open(os.path.join(DATA_PATH, "occupancyPath/pathFileList_"+str(userNum)+".txt"), 'w') as f:
    #     # f.write([fname+"\n" for fname in pathList])
    #     for fname in pathList:
    #         f.write(fname+"\n")
    # with open(os.path.join(DATA_PATH, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'wb') as f:
    # 	pickle.dump(wayPointList, f)

    # print("path of user ", userNum, " is saved to ", os.path.join(DATA_PATH, "occupancyPath/path_"+str(userNum)+".txt"))

    pathList = []
    with open(pathfName, 'r') as f:
        data = f.readlines()
        pathList = [line.split("\n")[0] for line in data]
    # 以下表示用
    # 各pickleファイルの読み込み

    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]

    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
    # print(gridMap)
    cellSize = gridMap[0][1].posx - gridMap[0][0].posx


    # 表示用pltの設定
    fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
    ax1.set_xlim(xlim[0]-10,xlim[1]+10)
    ax1.set_ylim(ylim[0]-10,ylim[1]+10)
    print("xlim", xlim[0],"  ",xlim[1])
    print("ylim", ylim[0],"  ",ylim[1])

    # # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
    # # 単純にpathの探索をするだけなら必要ない
    obstacleList = []
    obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
    obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
    print("obstacles...",len(obstacleList))

    for (x1, x2, y1, y2) in obstacleList:
        plt.plot([x1,x2],[y1,y1] , color = '#000000')
        plt.plot([x2,x2],[y1,y2] , color = '#000000')
        plt.plot([x2,x1],[y2,y2] , color = '#000000')
        plt.plot([x1,x1],[y2,y1] , color = '#000000')

    turnPointX = []
    turnPointY = []
    pickPointX = []
    pickPointY = []

    threshold = len(pathList)
    # threshold = 10

    # 各パスの描画と方向転換点，ピック点を描画
    for fName in pathList[:threshold]:
        pickleName = fName[:-3]+"pickle"
        pickleName = os.path.join(DATA_PATH, "path/"+ pickleName.split("/")[-1])
        with open(pickleName, mode='rb') as f:
            path = pickle.load(f)

        for (i,address) in enumerate(path.route):
            node = gridMap[address[0][1]][address[0][0]]
            if i == 0:
                print("Red path is ", path.routeCost, " in ", len(path.route) -1, " step\n")
                prevNode = node
                pickPointX.append(node.posx)
                pickPointY.append(node.posy)
                continue
            else:
                plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#07d34f')
                prevNode = node
                if address[1] == True:
                    turnPointX.append(node.posx)
                    turnPointY.append(node.posy)
    if startPos is not None:
        pt = ax1.scatter(startPos[0], startPos[1], color = '#2ca9e1')
    # pt1 = ax1.scatter(turnPointX,turnPointY,color = '#07d34f')
    pt2 = ax1.scatter(pickPointX[:threshold],pickPointY[:threshold],color = '#c92b29')

    plt.show()


if __name__ == '__main__':
    userNum = 5
    fname = os.path.join(DATA_PATH,"occupancyPath/pathFileList_"+ str(userNum)+ ".txt")
    run(fname)
