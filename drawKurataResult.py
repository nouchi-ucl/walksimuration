#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pickle
import itertools
import csv
import numpy as np
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import pathPlanning_v2
import searchShelf
import fileIO
import configDef

conf = configDef.Config()
DATA_PATH = conf.dataPath
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

# MIN MAX共に-1指定で全体を描画します
DRAW_THRESHOLD_MIN = -1
DRAW_THRESHOLD_MAX = -1

PICK_TIME = 0
DRAW_SIM_PATH = True
DRAW_OPT_PATH = False
DRAW_TURN_POINT = False
DRAW_WMS = True if conf.confType == "ipin" else False
DRAW_OUTR_WALL = True

def run(agentNo, drawFlag, dataPath):
    # 各pickleファイルの読み込み
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0]
    ylim = mapInfoList[1]
    startPos = (43.3,46.6)


    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
    # print(gridMap)
    # cellSize = gridMap[0][1].posx - gridMap[0][0].posx
    cellSize = gridMap.cellSize


    eventList = fileIO.loadPassList(os.path.join(dataPath,"wms/pathList"+str(agentNo)+".csv"))
    eventList.sort(key=lambda x:x[0])
    print("length of eventList...", len(eventList))

    recordPath = os.path.join(dataPath, "record/")
    fname = os.path.join(recordPath, "record_"+str(agentNo)+".pickle")
    with open(fname, 'rb') as f:
        records = pickle.load(f)

    fname = os.path.join(DATA_PATH,"occupancyPath/pathFileList_"+ str(agentNo)+ ".txt")
    pathList = []
    with open(fname, 'r') as f:
        data = f.readlines()
        pathList = [line.split("\n")[0] for line in data]

        kurataPath =  "/Users/nouchi/Desktop/IPIN2017_AIST/IPIN 2017 The trajectory at the time/Manual trajectory/Terminal4.csv"

    # drawFlag = False
    if drawFlag:
        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=((xlim[1]+20)/10,(ylim[1]+20)/10))
        # ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        # ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        ax1.set_xlim(xlim[0]-3,xlim[1]+3)
        ax1.set_ylim(ylim[0]-3,ylim[1]+3)
        # ax1.set_xlim(25,80)
        # ax1.set_ylim(0, 60)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])

        # # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
        # # 単純にpathの探索をするだけなら必要ない
        obstacleList = []
        obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
        obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
        if DRAW_OUTR_WALL:
            obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
        print("obstacles...",len(obstacleList))

        for (x1, x2, y1, y2) in obstacleList:
            ax1.add_patch(plt.Rectangle((x1,y1),x2-x1,y2-y1, fc ='#000000', fill = False))

        turnPointX = []
        turnPointY = []
        pickPointX = []
        pickPointY = []
        passedPointX = []
        passedPointY = []


        # 各パスの描画と方向転換点，ピック点を描画
        if DRAW_THRESHOLD_MIN == -1:
            drawMin = 0
        else:
            drawMin = DRAW_THRESHOLD_MIN
        if DRAW_THRESHOLD_MAX == -1:
            drawMax = len(pathList)
        else:
            drawMax = DRAW_THRESHOLD_MAX+1


        wmsList = []
        prevShelfID = -1
        if DRAW_WMS:
            with open(os.path.join(dataPath,"wms/WMS_comb_all.csv"),"r") as f:
                wmsAllData = csv.reader(f)
                header = next(wmsAllData)
                for line in wmsAllData:
                    if int(line[0]) == agentNo and prevShelfID != int(line[1]):
                        uTime = float(line[2])
                        shelfID = line[1]
                        shelfPos = searchShelf.run(shelfID, dataPath)
                        wmsList.append([uTime, shelfPos[0], shelfPos[1],"pass"])
                        prevShelfID = int(shelfID)
            wmsPickPosx = [line[1] for line in wmsList[drawMin: drawMax]]
            wmsPickPosy = [line[2] for line in wmsList[drawMin: drawMax]]

        for fName in pathList[drawMin:drawMax]:
            pickleName = fName[:-3]+"pickle"
            pickleName = os.path.join(DATA_PATH, "path/" +pickleName.split("/")[-1])
            with open(pickleName, mode='rb') as f:
                path = pickle.load(f)

            prevDirection = None
            for (i,address) in enumerate(path):
                node = gridMap.gridmap[address[0][1]][address[0][0]]
                if i == 0:
                    # print("Red path is ", path.routeCost, " in ", len(path) -1, " step\n")
                    prevNode = node
                    prevDirection = address[-1]
                    # pickPointX.append(node.posx)
                    # pickPointY.append(node.posy)
                    continue
                if i == len(path) -1:
                    if len(fName[:-3].split(",")[-1].split(" ")) > 1:
                        passedPointX.append(node.posx)
                        passedPointY.append(node.posy)
                    else:
                        pickPointX.append(node.posx)
                        pickPointY.append(node.posy)

                else:
                    if DRAW_OPT_PATH:
                        plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#07d34f')
                    prevNode = node
                    # print(address)
                    if address[-1] != prevDirection:
                        turnPointX.append(node.posx)
                        turnPointY.append(node.posy)
                    prevDirection = address[-1]
        # if startPos is not None:
            # pt = ax1.scatter(startPos[0], startPos[1], color = '#2ca9e1')


        if DRAW_TURN_POINT:
            pt1 = ax1.scatter(turnPointX,turnPointY,color = '#07d34f',s = 20)
        pt2 = ax1.scatter(passedPointX,passedPointY,color = '#A15DC4', s = 30)
        # pt3 = ax1.scatter(pickPointX,pickPointY,color = '#c92b29', s = 30)
        if DRAW_WMS:
            pt3 = ax1.scatter(wmsPickPosx,wmsPickPosy,color = '#c92b29', s = 30)

        stepThresholdMin = 0
        stepThresholdMax = 0

        # 各パスの描画と方向転換点，ピック点を描画
        if DRAW_THRESHOLD_MIN == -1:
            recordMin = 0
        else:
            recordMin = DRAW_THRESHOLD_MIN
        if DRAW_THRESHOLD_MAX == -1:
            recordMax = len(pathList)
        else:
            # 0からカウントが行くので実際にはDRAW_THRESHOLD_MAXで指定した数+1まで表示される
            recordMax = DRAW_THRESHOLD_MAX

        records = []
        with open(kurataPath,"r") as f:
            kurataData = csv.reader(f)
            header = next(kurataData)
            for line in kurataData:
                records.append(line[:-1])

        passCount = 0
        turnCount = 0
        minPassCount = 0
        maxPassCount = 0
        drawMin = 0
        drawMax = len(records)
        recordPickPoint = []
        for (i,record) in enumerate(records):
            if not record[2]== "":
                recordPickPoint.append((float(record[0]),float(record[1])))
                # print("pass now")
                passCount += 1
                if passCount == recordMin:
                    drawMin = i
                    minPassCount = passCount
                if passCount == recordMax:
                    drawMax = i
                    maxPassCount = passCount

        # for record in records:
        # よくわからないので直接指定しようかと
        # 00-10 0-81
        # 10-20 76-183
        # 20-30 178-282
        # 30-40 276-374
        # 40-50 369-483
        # 50-60 479-569
        # 60-70 564-664
        # 70-82 659-
        drawMin = 0
        drawMax = len(records)
        prevRecord = records[drawMin -1] if drawMin != 0 else records[0]
        if DRAW_SIM_PATH:
            for record in records[drawMin:drawMax-5]:
                    plt.plot([float(record[0]), float(prevRecord[0])], [float(record[1]), float(prevRecord[1])], color = '#2ca9e1')
                    prevRecord = record
        print("pass:",passCount)
        for (recordPos, posx, posy)in zip(recordPickPoint[recordMin:recordMax], wmsPickPosx,wmsPickPosy):
            print("record pos", recordPos, posx, posy)
        print(len(wmsList),len(wmsPickPosx), len(recordPickPoint), len(recordPickPoint[recordMin:recordMax]))
        print("passCount,",minPassCount, maxPassCount)
        plt.show()

if __name__ == "__main__":
    agentNo = 4
    drawFlag = True
    run(agentNo, drawFlag, DATA_PATH)
