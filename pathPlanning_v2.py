#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

ALLOW_DIAGONAL_MOVE = True
ROUTE_COST_THRESHOLD = 20000
DIAGONAL_MOVE_COST = 1.4
REQUIRED_PATH_NUM = 50
TURN_COST = 2.0


# x,yの座標からその点が含まれるセルのアドレスを返す point -> gridMap[address[1]][address[0]]
# Nodeインスタンスそのものを返すわけではないので注意
def searchNodeFromPoint(gridMap, point, cellSize):
    address = None
    print("searchNodeFrom Point", point, cellSize)

    for line in gridMap:
        yDist = abs(line[0].posy - point[1])
        if yDist <= cellSize:
            for node in line:
                xDist = abs(node.posx - point[0])
                if xDist <= cellSize:
                    address = node.address
    if address is None:
        print("Target point ", point, " is out of map region")
        sys.exit()
    return address

# パスの更新:1マス進んだ先のノード(newNode)をpathに追加していく
# 端部のノードの更新, routeへ追加, path全体コストの更新
# コストは斜め方向に移動する場合は x1.4
# 前回の移動方向に対して回転している場合はコストを追加
def updatePath(path, newNode, direction):
    newPath = copy.deepcopy(path)

    newPath.prevNode = newPath.edgeNode
    newPath.edgeNode = newNode
    # newPath.route.append(newNode)
    newPath.routeCost += newNode.cost
    turnFlag = False
    if newPath.moveDirection != direction and newPath.moveDirection is not None:
        newPath.routeCost += 2.0
        turnFlag = True
    newPath.moveDirection = direction
    newPath.route.append([newNode.address, turnFlag])

    return newPath


# pathの末端のセルから隣接するセル(neighborAdd)へ移動する際の計算を行う
#   移動先がgoal　-> pathをgoalPathに追加
#   移動先がobstacke/shelf/自身が以前通ったnode(ループが発生している) -> 移動できない(新しいpathとして追加しない)
#   移動先が既に他のpathが通過している
#       -> 他のpathがこのnodeを通った時のコストより大きい -> 移動しない(新しいpathとして追加しない)
#       -> ~~のコストより小さい -> 新しいpathとして追加
#   移動先はまだどのpathも通ったことのないnodeである -> 新しいpathとして追加
# 返り値は移動先がgoalの場合はTrue それ以外はFalse
def stepNewNode(gridMap, path, goal, neighborAdd, direction, currentPath, goalPath):
    # print("neighbor ", neighborAdd[1],neighborAdd[0])
    neighbor = gridMap[neighborAdd[1]][neighborAdd[0]]
    # print("neighbor ", neighbor.address, "/pos ", neighbor.posx, neighbor.posy)

    # 移動先がgoalの場合
    if neighbor.address == goal.address:
        newPath = updatePath(path, neighbor, direction)
        goalPath.append(newPath)
        print("Find path! cost is ", newPath.routeCost, ":  step is ", len(newPath.route), len(goalPath))
        return True

    # 移動先がobstacle/shelf/自身が以前通ったNode
    elif neighbor.isObstacle or neighbor.isShelf or path.isPassedPoint(neighbor, gridMap):
        return False
    # 既に他のpathが通過している
    elif neighbor.isStepped:
        newCost = path.routeCost + neighbor.cost
        if newCost > ROUTE_COST_THRESHOLD:
            return False
        if newCost < neighbor.minStepCost:
            newPath = updatePath(path, neighbor, direction)
            addX = neighbor.address[0]
            addY = neighbor.address[1]
            gridMap[addY][addX].minStepCost = newCost
            gridMap[addY][addX].minPathFromStart = newPath
            currentPath.append(newPath)
        else :
            return False
    else :
        newCost = path.routeCost + neighbor.cost
        if newCost > ROUTE_COST_THRESHOLD:
            return False
        newPath = updatePath(path, neighbor, direction)
        addX = neighbor.address[0]
        addY = neighbor.address[1]
        # print(addX, addY, len(gridMap[0]), len(gridMap))
        gridMap[addY][addX].minStepCost = newCost
        gridMap[addY][addX].minPathFromStart = newPath
        gridMap[addY][addX].isStepped = True
        currentPath.append(newPath)
    return False

# 作成したマップを使って,2地点間の最小コスト経路を探索
# requiredGoalPathNum の本数分探索が完了したら終了
# 通過セルが1つでも違う場合は別の経路として扱う
# 計算上,最速でゴールに到着したpathが最小コストになるとは限らない
#   方向転換によるコスト,斜めへの移動があるため
def pathPlanning(gridMap, startNode, goalNode):
    print("Path Planning... ", startNode, " ", goalNode)
    requiredGoalPathNum = REQUIRED_PATH_NUM
    findStepThreshold = 10
    goalPath = []
    currentPath = []
    neighborNodeList = []
    currentPath.append(pathDef.Path(gridMap[startNode[1]][startNode[0]]))
    goal = gridMap[goalNode[1]][goalNode[0]]
    goal.isObstacle = False
    # print("goal ", goal)
    lastFindCount = 0
    stepCount = 0
    while(True):
        # 最後にgoalに到達したstepから一定以上新しいgoal pathがみつからない場合探索を終了する
        if lastFindCount != 0 and lastFindCount + findStepThreshold < stepCount:
            print("goalPath is not enough ...", len(goalPath), " / ", requiredGoalPathNum, " but new path reach the goal has not found in last ", findStepThreshold, " steps.")
            return goalPath
        # goal pathが一定数見つかった場合　探索を終了する
        if len(goalPath) >= requiredGoalPathNum:
            print("goalPath has reached required number", len(goalPath), " and stop search path")
            return goalPath
        # pathを探索しきった場合(探索中のpathがなくなった場合)　探索を終了する
        if len(currentPath) == 0:
            print("ERROR : route could not find. Is the goal Node is in any obstacle ?")
            return goalPath
        prevPath = copy.deepcopy(currentPath)
        currentPath.clear()
        print("In ",stepCount," step, there are ",len(prevPath)," route. ", lastFindCount)
        stepCount += 1

        # 探索部分
        for path in prevPath:
            # print("currentNode ", path.edgeNode.address, "/pos ", path.edgeNode.posx, path.edgeNode.posy)
            # neighborNodeList.clear()
            if path.edgeNode.neighborUp is not None:
                if stepNewNode(gridMap, path, goal, path.edgeNode.neighborUp, 'up', currentPath, goalPath):
                    lastFindCount = stepCount
            if path.edgeNode.neighborDown is not None:
                if stepNewNode(gridMap, path, goal, path.edgeNode.neighborDown, 'down', currentPath, goalPath):
                    lastFindCount = stepCount
            if path.edgeNode.neighborRight is not None:
                if stepNewNode(gridMap, path, goal, path.edgeNode.neighborRight, 'right', currentPath, goalPath):
                    lastFindCount = stepCount
            if path.edgeNode.neighborLeft is not None:
                if stepNewNode(gridMap, path, goal, path.edgeNode.neighborLeft, 'left', currentPath, goalPath):
                    lastFindCount = stepCount

            if ALLOW_DIAGONAL_MOVE:
                if path.edgeNode.neighborRightUp is not None:
                    if stepNewNode(gridMap, path, goal, path.edgeNode.neighborRightUp, 'rightUp', currentPath, goalPath):
                        lastFindCount = stepCount
                if path.edgeNode.neighborRightDown is not None:
                    if stepNewNode(gridMap, path, goal, path.edgeNode.neighborRightDown, 'rightDown', currentPath, goalPath):
                        lastFindCount = stepCount
                if path.edgeNode.neighborLeftUp is not None:
                    if stepNewNode(gridMap, path, goal, path.edgeNode.neighborLeftUp, 'leftUp', currentPath, goalPath):
                        lastFindCount = stepCount
                if path.edgeNode.neighborLeftDown is not None:
                    if stepNewNode(gridMap, path, goal, path.edgeNode.neighborLeftDown, 'leftDown', currentPath, goalPath):
                        lastFindCount = stepCount

    return goalPath


def run(startPoint, goalPoint, shelfID, displayFlag):
    mapInfoList = []
    obstacles = []
    shelf = []

    # # pickleファイルからオブジェクトを復元
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    print("mapSize...", xlim,ylim)

    # 別であらかじめ作成しておいたgridMapをロード
    # makeOccupancyGrid.py
    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
    # print(gridMap)
    cellSize = gridMap[0][1].posx - gridMap[0][0].posx
    print("cellSize", cellSize)
    startNode = searchNodeFromPoint(gridMap, startPoint, cellSize)
    gridMap[startNode[1]][startNode[0]].isStepped = True
    goalNode = searchNodeFromPoint(gridMap, goalPoint, cellSize)
    # print(goalNode)
    path = pathPlanning(gridMap, startNode, goalNode)
    if len(path) == 0:
        print("Could not find path to ", goalPoint, " from ", startPoint, ". \nSo exit this program")
        sys.exit()

    # パスをコストの低いものから順にソート
    path = sorted(path, key=attrgetter('routeCost'))

    print("Find path, cost is ", path[0].routeCost)
    # for node in path[0].route:
        # print(node.posx, node.posy, "\n")

    if not os.path.isdir(os.path.join(DATA_PATH, "path")):
        os.mkdir(os.path.join(DATA_PATH, "path"))
    writeName = "path/pathfrom_"+ str(startPoint[0])+","+str(startPoint[1])+"_"+str(goalPoint[0])+","+str(goalPoint[1])+"_"+shelfID
    with open(os.path.join(DATA_PATH, writeName + ".txt"), 'w') as f:
        totalCost = 0.
        # for node in path[0].route:
        #     totalCost += node.cost
        #     f.write("("+str(node.address[0])+","+str(node.address[1])+")  "+str(node.isObstacle)+"  ("+str(node.posx)+","+str(node.posy)+") "+str(totalCost)+"\n")

        for address in path[0].route:
            print(address)
            node = gridMap[address[0][1]][address[0][0]]
            totalCost += node.cost
            turnFlag = "True"if address[1]else"False"
            f.write(str(node.address[0])+","+str(node.address[1])+","+str(node.posx)+","+str(node.posy)+","+str(totalCost)+","+turnFlag+"\n")
            # f.write("("+str(node.address[0])+","+str(node.address[1])+")  "+str(node.isObstacle)+"  ("+str(node.posx)+","+str(node.posy)+") "+str(totalCost)+"\n")
    print("write min cost path to ", os.path.join(DATA_PATH, writeName + ".txt"))
    with open(os.path.join(DATA_PATH, writeName + ".pickle") ,mode = 'wb') as f:
    	pickle.dump(path[0], f)

    if displayFlag:
        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
        ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])

        # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
        # 単純にpathの探索をするだけなら必要ない
        obstacleList = []
        obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
        obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
        obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
        print("obstacles...",len(obstacleList))

        for (x1, x2, y1, y2) in obstacleList:
            plt.plot([x1,x2],[y1,y1] , color = '#000000')
            plt.plot([x2,x2],[y1,y2] , color = '#000000')
            plt.plot([x2,x1],[y2,y2] , color = '#000000')
            plt.plot([x1,x1],[y2,y1] , color = '#000000')

        # コストの低いpath上位5ルートまでをマップに表示
        colorList = ['#a1a900','#e4007f', '#2ca9e1', '#07d34f', '#c92b29']
        for j in range(len(path) if len(path) < 5 else 5):
            for (i,address) in enumerate(path[j].route):
                node = gridMap[address[0][1]][address[0][0]]
                if i == 0:
                    print("Red path is ", path[j].routeCost, " in ", len(path[j].route) -1, " step\n")
                    prevNode = node
                    continue
                else:
                    plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = colorList[j])
                    prevNode = node

        plt.show()

    return writeName + ".txt"

if __name__ == '__main__':
    startPoint = (43.3,46.6)
    goalPoint = (43.,60.)
    # goalPoint = (85.,30.)
    # cellSize = 0.5
    # 表示切替用のフラグ
    displayFlag = True
    shelfID = searchShelf.searchShelfFromCoordinate(goalPoint)
    run(startPoint, goalPoint, shelfID, displayFlag)
