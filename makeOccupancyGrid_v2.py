#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math
import copy
import numpy as np
import os
import matplotlib.pyplot as plt
import pickle
import itertools

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
from classdef import bleDef

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

OBSTACLE_COST = 10000
NEIGHBOR_OBSTACLE_COST = 3.


# point1 point2を通る直線とpoint3との距離を返す
# point : (x,y)のタプル
def distancePointandLine(point1, point2, point3):
    u = np.array([point2[0] - point1[0], point2[1] - point1[1]])
    v = np.array([point3[0] - point1[0], point3[1] - point1[1]])
    L = abs(np.cross(u, v) / np.linalg.norm(u))
    return L

def distancePointandObstacle(x1,x2,y1,y2,point):
    minDist = distancePointandLine((x1,y1),(x2,y1),point)
    dist = distancePointandLine((x2,y1),(x2,y2),point)
    if dist < minDist:
        minDist = dist
    dist = distancePointandLine((x2,y2),(x1,y2),point)
    if dist < minDist:
        minDist = dist
    dist = distancePointandLine((x1,y2),(x1,y1),point)
    if dist < minDist:
        minDist = dist
    return minDist


# マップの初期化(マップを格子状に切って各セルの初期化まで)
# マップの中心を基準にしている(centerCell)
def initializeMap(xlim,ylim,startPoint, cellSize):
    xMin = xlim[0]
    xMax = xlim[1]
    yMin = ylim[0]
    yMax = ylim[1]
    startx = startPoint[0]
    starty = startPoint[1]

    # 外周の壁もセルとしておきたいので外に1マスずつ拡張している
    sizeX = (-1 * round((startx - xMin)/cellSize) -1, round((xMax - startx)/cellSize) +1)
    sizeY = (-1 * round((starty - yMin)/cellSize) -1, round((yMax - starty)/cellSize) +1)
    centerCell = (round((startx - xMin)/cellSize)+2, round((starty - yMin)/cellSize)+2)
    mapSize = (sizeX[1] - sizeX[0] +1, sizeY[1] - sizeY[0] +1)
    print("mapGridSize ", mapSize)
    print("sizeX",sizeX, "  sizeY",sizeY)
    # print([[(cellSize * x + startx,cellSize * y + starty, (x - sizeX[0], y - sizeY[0])) for x in range(sizeX[0] ,sizeX[0] +2)] for y in range(sizeY[0], sizeY[0] +2)])
    # print("\n\n\n")
    # print([[Node(cellSize * x + startx, cellSize * y + starty, x - sizeX[0], y - sizeY[0], mapSize).address for x in range(sizeX[0],sizeX[0] +2)] for y in range(sizeY[0], sizeY[0] +2)])

    # 各セルに初期化したノードインスタンスを置いていく
    gridMap = [[nodeDef.Node(cellSize * x + startx, cellSize * y + starty, x - sizeX[0], y - sizeY[0], mapSize, cellSize) for x in range(sizeX[0] ,sizeX[1] +1)] for y in range(sizeY[0], sizeY[1] +1)]

    return gridMap

# pointがx1,x2,y1,y2に囲まれた矩型領域内に存在するかどうかの判定
# x1<x2, y1<y2でないといけないので引数に注意
def inRectangle(point,x1,x2,y1,y2):
    x = point[0]
    y = point[1]
    if x1 <= x and x <= x2 and y1 <= y and y <= y2:
        return True
    else:
        return False


def addShelf(gridMap, cellSize, shelfList):
    cellSizeHalf = cellSize/2
    xMax = len(gridMap[0])
    yMax = len(gridMap)
    # print("addObstacle",xMax, yMax)
    for i, j in itertools.product(range(yMax), range(xMax)):
        node = gridMap[i][j]

        x = node.posx
        y = node.posy
        center = (x,y)
        # minDist = node.minDistObstacle

        for (x1,x2,y1,y2) in shelfList:
            if node.isObstacle or node.isShelf:
                break

            # セルの中心が棚の内部なら,コストを引き上げる
            if inRectangle(center,x1,x2,y1,y2):
                node.cost = OBSTACLE_COST
                node.isShelf = True
                # node.neighborAccessFlag[0] = True

        if i % 10 == 0 and j == 0:
            print("add shelf info to Node... ",i," / ",yMax)

    return gridMap

# 初期化されたマップに障害物の情報を追加していく
# 正確には各セルについて
#   セルの中心が障害物の内部か？
#       True ->セル全体を障害物とする
#       False->四隅の各点が障害物の内部かどうかで隣のセルへ移動できるかどうかを判定
# 各セル間の移動の可否を設定していく
# またセル自体が障害物でない場合は他の障害物との距離によってそのセルへの移動コストを設定する
# セルの中心が障害物に近いほど高いコストを持つようにする
def addObstacle(gridMap, cellSize, obstacleList):
    cellSizeHalf = cellSize/2
    xMax = len(gridMap[0])
    yMax = len(gridMap)
    testCount = 0
    # print("addObstacle",xMax, yMax)
    for i, j in itertools.product(range(yMax), range(xMax)):
        node = gridMap[i][j]

        x = node.posx
        y = node.posy
        center = (x,y)
        # minDist = node.minDistObstacle

        for (x1,x2,y1,y2) in obstacleList:
            if node.isObstacle:
                node.minDistObstacle = 0.
                testCount +=1
                break
            # セルの中心が障害物の内部なら,隣接セルからこのセルへの移動を不可に
            if inRectangle(center,x1,x2,y1,y2):
                node.cost = OBSTACLE_COST
                node.isObstacle = True
                node.neighborAccessFlag[0] = True
            # 四隅の各点が障害物の内部にあるかどうかで移動できない隣接セルを設定
            else:
                if inRectangle(node.uc,x1,x2,y1,y2):
                    node.neighborAccessFlag[8] = True
                if inRectangle(node.dc,x1,x2,y1,y2):
                    node.neighborAccessFlag[2] = True
                if inRectangle(node.rc,x1,x2,y1,y2):
                    node.neighborAccessFlag[6] = True
                if inRectangle(node.lc,x1,x2,y1,y2):
                    node.neighborAccessFlag[4] = True
                if inRectangle(node.ru,x1,x2,y1,y2):
                    node.neighborAccessFlag[9] = True
                if inRectangle(node.rd,x1,x2,y1,y2):
                    node.neighborAccessFlag[3] = True
                if inRectangle(node.lu,x1,x2,y1,y2):
                    node.neighborAccessFlag[7] = True
                if inRectangle(node.ld,x1,x2,y1,y2):
                    node.neighborAccessFlag[1] = True

        if i % 10 == 0 and j == 0:
            print("add obstacle info to Node... ",i," / ",yMax)

        # if minDist < 0.5:
        #     node.cost = (5. - minDist*10) **2 /10 + 1.
            # print("node cost has changed to ", node.cost)

    print ("obstacleCount...",testCount)
    return gridMap

def addCost(gridMap, cellSize):
    xMax = len(gridMap[0])
    yMax = len(gridMap)
    # print("addObstacle",xMax, yMax)
    for i, j in itertools.product(range(yMax), range(xMax)):
        node = gridMap[i][j]

        if i == 0:
            node.neighborDown = None
            node.neighborRightDown = None
            node.neighborLeftDown = None
        if j == 0:
            node.neighborLeft = None
            node.neighborLeftUp = None
            node.neighborLeftDown = None
        if i == yMax -1:
            node.neighborUp = None
            node.neighborRightUp = None
            node.neighborLeftUp = None
        if j == xMax -1:
            node.neighborRight = None
            node.neighborRightUp = None
            node.neighborRightDown = None

        if node.isObstacle or node.isShelf:
            if node.isObstacle:
                obstacleFlag = True
            else:
                obstacleFlag = False
            if i != 0:
                if obstacleFlag:
                    gridMap[i-1][j].neighborUp = None
                if gridMap[i-1][j].cost != OBSTACLE_COST:
                    gridMap[i-1][j].cost = NEIGHBOR_OBSTACLE_COST

                if j != 0:
                    if obstacleFlag:
                        gridMap[i-1][j-1].neighborRightUp = None
                    if gridMap[i-1][j-1].cost != OBSTACLE_COST:
                        gridMap[i-1][j-1].cost = NEIGHBOR_OBSTACLE_COST

            if j != 0:
                if obstacleFlag:
                    gridMap[i][j-1].neighborRight = None
                if gridMap[i][j-1].cost != OBSTACLE_COST:
                    gridMap[i][j-1].cost = NEIGHBOR_OBSTACLE_COST

                if i != yMax-1:
                    if obstacleFlag:
                        gridMap[i+1][j-1].neighborRightDown = None
                    if gridMap[i+1][j-1].cost != OBSTACLE_COST:
                        gridMap[i+1][j-1].cost = NEIGHBOR_OBSTACLE_COST

            if i != yMax-1:
                if obstacleFlag:
                    gridMap[i+1][j].neighborDown = None
                if gridMap[i+1][j].cost != OBSTACLE_COST:
                    gridMap[i+1][j].cost = NEIGHBOR_OBSTACLE_COST

                if j != xMax-1:
                    if obstacleFlag:
                        gridMap[i+1][j+1].neighborLeftDown = None
                    if gridMap[i+1][j+1].cost != OBSTACLE_COST:
                        gridMap[i+1][j+1].cost = NEIGHBOR_OBSTACLE_COST

            if j != xMax-1:
                if obstacleFlag:
                    gridMap[i][j+1].neighborLeft = None
                if gridMap[i][j+1].cost != OBSTACLE_COST:
                    gridMap[i][j+1].cost = NEIGHBOR_OBSTACLE_COST

                if i != 0:
                    if obstacleFlag:
                        gridMap[i-1][j+1].neighborLeftUp = None
                    if gridMap[i-1][j+1].cost != OBSTACLE_COST:
                        gridMap[i-1][j+1].cost = NEIGHBOR_OBSTACLE_COST
        if not node.isObstacle:
            accessFlag = [0,0,0, 0,0,0, 0,0,0, 0]

            if node.neighborAccessFlag[1]:
                accessFlag[1] += 2
                accessFlag[2] += 1
                accessFlag[4] += 1
            if node.neighborAccessFlag[2]:
                accessFlag[1] += 1
                accessFlag[2] += 2
                accessFlag[3] += 1
            if node.neighborAccessFlag[3]:
                accessFlag[2] += 1
                accessFlag[3] += 2
                accessFlag[6] += 1
            if node.neighborAccessFlag[4]:
                accessFlag[1] += 1
                accessFlag[4] += 2
                accessFlag[7] += 1
            if node.neighborAccessFlag[6]:
                accessFlag[3] += 1
                accessFlag[6] += 2
                accessFlag[9] += 1
            if node.neighborAccessFlag[7]:
                accessFlag[4] += 1
                accessFlag[7] += 2
                accessFlag[8] += 1
            if node.neighborAccessFlag[8]:
                accessFlag[7] += 1
                accessFlag[8] += 2
                accessFlag[9] += 1
            if node.neighborAccessFlag[9]:
                accessFlag[6] += 1
                accessFlag[8] += 1
                accessFlag[9] += 2

            if accessFlag[1] >=2:
                node.neighborLeftDown = None
            if accessFlag[2] >=2:
                node.neighborDown = None
            if accessFlag[3] >=2:
                node.neighborRightDown = None
            if accessFlag[4] >=2:
                node.neighborLeft = None
            if accessFlag[6] >=2:
                node.neighborRight = None
            if accessFlag[7] >=2:
                node.neighborLeftUp = None
            if accessFlag[8] >=2:
                node.neighborUp = None
            if accessFlag[9] >=2:
                node.neighborRightUp = None

    return gridMap

# gridMapの生成全般
def makeMap(xlim, ylim, startPoint, cellSize, obstacleList, shelfList):
    print("initialize map...\n")
    initMap = initializeMap(xlim, ylim, startPoint, cellSize)
    print("add obstacle on map...\n")
    obstMap = addObstacle(initMap, cellSize, obstacleList)
    print("add shelf on map...\n")
    shelfMap = addShelf(obstMap, cellSize, shelfList)
    gridMap = addCost(shelfMap, cellSize)

    return gridMap


def run(startPoint, goalPoint, cellSize):
    mapInfoList = []
    obstacles = []
    shelf = []
    ble = []

    # pickleファイルからオブジェクトを復元
    # mapの外周の情報
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)
    # 棚の情報(ここでは障害物と同じ扱い)
    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)
    # 障害物の情報
    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'ble.pickle'), mode = 'rb') as f:
        ble = pickle.load(f)


    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    print("mapSize...", xlim,ylim)

    # 棚と障害物を統合(位置情報しか使わないので統合できる)
    # 外周の壁も障害物として追加
    shelfList = []
    obstacleList = []
    shelfList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    # obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])

    obstacleList.extend([(43.5,45.,31.5,33)])

    obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
    print("obstacles...",len(obstacleList))


    # マップの作成
    gridMap = makeMap(xlim,ylim,startPoint, cellSize, obstacleList, shelfList)

    # pickleファイルとして書き出す
    print("save map (pickle object file) as ", os.path.join(PICKLE_PATH, "gridMap.pickle"),"\n")
    with open(os.path.join(PICKLE_PATH, "gridMap.pickle"),  mode='wb') as f:
        pickle.dump(gridMap, f)

    # 表示用pltの設定
    print("draw grid map...\n")
    fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
    ax1.set_xlim(xlim[0]-10,xlim[1]+10)
    ax1.set_ylim(ylim[0]-10,ylim[1]+10)
    print("xlim", xlim[0],"  ",xlim[1])
    print("ylim", ylim[0],"  ",ylim[1])

    for (x1, x2, y1, y2) in obstacleList:
        plt.plot([x1,x2],[y1,y1] , color = '#000000')
        plt.plot([x2,x2],[y1,y2] , color = '#000000')
        plt.plot([x2,x1],[y2,y2] , color = '#000000')
        plt.plot([x1,x1],[y2,y1] , color = '#000000')
    for (x1, x2, y1, y2) in shelfList:
        plt.plot([x1,x2],[y1,y1] , color = '#990000')
        plt.plot([x2,x2],[y1,y2] , color = '#990000')
        plt.plot([x2,x1],[y2,y2] , color = '#990000')
        plt.plot([x1,x1],[y2,y1] , color = '#990000')

    bleX = [ b.posx for b in ble]
    bleY = [ b.posy for b in ble]
    print(len(ble))
    print(ble[0].posx, ble[0].posy)
    pt1 = plt.scatter(bleX,bleY,color = '#2ca9e1')

    plt.show()

if __name__ == '__main__':
    startPoint = (43.3,46.6)
    goalPoint = (85.,30.)
    # cellSize = 0.5
    cellSize = 0.3

    run(startPoint, goalPoint, cellSize)
