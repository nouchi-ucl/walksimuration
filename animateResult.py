import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os
import pickle
import csv
import datetime
import time

import searchShelf

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")

AGENT_NO = 4
#会場の広さに関連する数字
#ちなみにBLEデータは左下が原点らしい
ENV_X = 110 #ヨコ方向
ENV_Y = 76.5 #タテ方向
MAP_IMAGE = os.path.join(DATA_PATH, "map/Map_image.png") #プレーンなやつ


def run(dataPath, drawFlag, agentNo, date, resultList):
    picklePath = os.path.join(dataPath, "pickle")
    mapInfoList = []
    #  読み込み用リスト 棚
    shelf = []
    #  読み込み用リスト 障害物
    obstacle = []

    # 表示用リスト 棚/障害物
    obstacles = []

    movePosx = []
    movePosy = []
    utime = []
    # pickleファイルからオブジェクトを復元
    with open(os.path.join(picklePath, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)
    print (mapInfoList)
    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    startPoint = mapInfoList[0][2]

    with open(os.path.join(picklePath, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(picklePath, 'obstacle.pickle'),mode = 'rb') as f:
        obstacle = pickle.load(f)

    # with open(os.path.join(dataPath,"result/Terminal"+str(agentNo)+".csv"), 'r') as f:
    #     moveData = csv.reader(f)
    #     # header行を読み飛ばす
    #     header = next(moveData)
    #     for line in moveData:
    #         # print(line)
    #         movePosx.append(line[0])
    #         movePosy.append(line[1])
    #         utime.append(line[2])
    movePosx = []
    movePosy = []
    moveData = resultList
    img = plt.imread(MAP_IMAGE)
    magX =  len(img[0])/ENV_X
    magY =  -1 * len(img)/ENV_Y
    for line in moveData:
        movePosx.append(line[0] *magX)
        movePosy.append(line[1] *magY + len(img))

    # startTime = float(resultList[0][2])
    wmsList = []
    with open(os.path.join(dataPath,"wms/WMS_comb.csv"), 'r') as f:
        wmsData = csv.reader(f)

        # header行を読み飛ばす
        header = next(wmsData)
        next(wmsData)
        for line in wmsData:
            # print(line)
            terminalID = line[0]
            shelfNO = line[1]
            utime = int(line[2])

            if int(terminalID) == agentNo:
                # if utime < startTime:
                #     # print(utime, shelfNO, "xxxx")
                #     continue
                wmsList.append([utime, searchShelf.run(shelfNO, dataPath)])
                # print(utime, shelfNO)
            # 邪魔な空行を読み飛ばす
            # next(wmsData)
    wmsList.sort(key=lambda x:x[0])
    wmsX = [wms[1][0] for wms in wmsList]
    wmsY = [wms[1][1] for wms in wmsList]

    passList = []
    with open(os.path.join(dataPath,"wms/nearFromBLE"+str(agentNo)+".csv"), 'r') as f:
        pList = csv.reader(f)
        header = next(pList)
        for line in pList:
            utime = float(line[0])
            posx = float(line[1])
            posy = float(line[2])
            action = line[3]
            if action == "Pass":
                passList.append([utime, (posx, posy), action])
    passList.sort(key=lambda x:x[0])
    passX = [p[1][0] *magX for p in passList]
    passY = [p[1][1] *magY + len(img) for p in passList]

    # fig, (ax1) = plt.subplots(1,1,figsize=(16,11))
    # ax1.set_xlim(25,80)
    # ax1.set_ylim(0, 60)
    #
    # # fig, (ax1) = plt.subplots(1,1,figsize=(16,11))
    # # ax1.set_xlim(xlim[0],xlim[1])
    # # ax1.set_ylim(ylim[0],ylim[1])
    # print("xlim", xlim[0],"  ",xlim[1])
    # print("ylim", ylim[0],"  ",ylim[1])
    #map画像の読み込み

    # 表示用pltの設定
    #図のオブジェクト作成と，イメージ表示
    fig = plt.figure(figsize=(int(len(img[0])/75), int(len(img)/75)))
    ax = fig.add_subplot(111)
    ax.spines["right"].set_color("none")  # 右消し
    ax.spines["left"].set_color("none")   # 左消し
    ax.spines["top"].set_color("none")    # 上消し
    ax.spines["bottom"].set_color("none") # 下消し
    ax.tick_params(labelbottom="off",bottom="off") # x軸の削除
    ax.tick_params(labelleft="off",left="off") # y軸の削除
    # ax.set_xticklabels([])
    # ax.box("off") #枠線の削除
    ax.imshow(img)

    objs = []

    img = plt.imread(os.path.join(dataPath, "map/Map_image.png"))
    # ax.imshow(img, extent=[xlim[0], xlim[1], ylim[0], ylim[1]], aspect=1)
    drawListx = []
    drawListy = []
    objs = []
    # for i in range(0,int(len(movePosx)/5)):
    #     drawListx.extend(x for x in movePosx[(i-1)*5:i*5])
    #     drawListy.extend(y for y in movePosy[(i-1)*5:i*5])
    #     im, = ax.plot(drawListx,drawListy, color = '#c92b29', lw=3)
    #     objs.append([im])
    passed = ax.scatter(passX, passY, color = "#A15DC4", s=70)
    # passed = ax.scatter(passX[:3], passY[:3], color = "#A15DC4", s=70)

    shelfX = []
    shelfY = []
    shelfList = [708, 709, 759, 727,817,723,725,765,1,767,766,728,718,
                735,486,1006,291,720,759,737,730,1008,1200,703,642,1198,
                1164,815,1072,722,730,1004,699,647,723,181,451,74,455,194,318,333]
    for s in shelfList:
        shelfX.append(searchShelf.run(s, dataPath)[0] *magX)
        shelfY.append(searchShelf.run(s, dataPath)[1] *magY + len(img))
    pick = ax.scatter(shelfX, shelfY, color = "#c92b29", s=70)


    for i in range(0,len(movePosx)):
        drawListx.extend([movePosx[i]])
        drawListy.extend([movePosy[i]])
        im, = ax.plot(drawListx,drawListy, color = '#2ca9e1', lw=4)
        objs.append([im])
    ani = animation.ArtistAnimation(fig, objs, interval = 100, repeat = True)
    ani.save(os.path.join(DATA_PATH,'result.mp4'))

    plt.show()
    # for (x,y,t) in zip(movePosx, movePosy, utime):
    #     if utime > TIME_THRESHOLD:
    #         break
    #     drawListx.append([x])
    #     drawListy.append([y])
    #     points = ax1.scatter(drawListx, drawListy, color = '#c92b29', s=3)
    #     text = ax1.text(xlim[0]+5, ylim[1]-5,"time:%.3f"% float(t))
    #     objs.append([points, text])
    # interval = 100 * STEP_TIME
    # ani = animation.ArtistAnimation(fig, objs, interval = interval, repeat = True)
    # plt.show()

    # else:
    #     obstacleList = []
    #     obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    #     obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacle])
    #     print("obstacles...",len(obstacleList))
    #
    #     for (x1, x2, y1, y2) in obstacleList:
    #         plt.plot([x1,x2],[y1,y1] , color = '#000000')
    #         plt.plot([x2,x2],[y1,y2] , color = '#000000')
    #         plt.plot([x2,x1],[y2,y2] , color = '#000000')
    #         plt.plot([x1,x1],[y2,y1] , color = '#000000')
    #     move = ax1.scatter(movePosx,movePosy,color = '#2ca9e1', s=8)
    #     # pick = ax1.scatter(wmsX[:5], wmsY[:5], color = "#01e807", s=30)
    #     passed = ax1.scatter(passX[:3], passY[:3], color = "#A15DC4", s=30)
    #
    #     shelfX = []
    #     shelfY = []
    #     shelfList = [708, 709, 759, 727]
    #     for s in shelfList:
    #         shelfX.append(searchShelf.run(s, dataPath)[0])
    #         shelfY.append(searchShelf.run(s, dataPath)[1])
    #     pick = ax1.scatter(shelfX, shelfY, color = "#c92b29", s=30)
# 708,709,759,727,817


# C0:1C:4D:44:77:5A,59342.07421,52791.35226,0,left
# C0:1C:4D:45:8B:09,58764.54585,42238.96908,0,right
# C0:1C:4D:46:C0:43,59303.66113,30564.91717,0,up
#
# C0:1C:4D:47:0E:9D,80097.3316,42156.47385,0,left
# C0:1C:4D:47:52:3C,79116.60914,19999.24691,0,left


        # ble = ax1.scatter(59.34207421,52.79135226, color = '#c92b29', s=15)
        # ble1 = ax1.scatter(58.76454585,42.23896908, color = '#c92b29', s=15)
        # ble2 = ax1.scatter(59.30366113,30.56491717, color = '#c92b29', s=15)
        # ble3 = ax1.scatter(80.0973316,42.15647385, color = '#c92b29', s=15)
        # ble4 = ax1.scatter(79.11660914,19.99924691, color = '#c92b29', s=15)
        # filename = os.path.join(DATA_PATH,"img/resultFig_"+str(agentNo)+".png")
        # date = time.strftime("%m%d_%H%M", time.localtime())
        # if not os.path.exists(os.path.join(dataPath,"img/img"+date)):
        #     os.mkdir(os.path.join(dataPath,"img/img"+date))
        # filename = os.path.join(dataPath,"img/img"+date+"/resultFig_"+str(agentNo)+"_"+date+".png")
        # plt.savefig(filename)
        # print("saveFig to ",filename)
        # if drawFlag:
        #     plt.show()

if __name__ == '__main__':
    drawFlag = True
    date = time.strftime("%m%d_%H%M", time.localtime())

    run(DATA_PATH, drawFlag, AGENT_NO, date)
