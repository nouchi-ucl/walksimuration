#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
import csv
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import pathPlanning_v2
import searchShelf

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

userNum = 3

with open(os.path.join(DATA_PATH, "occupancyPath/pathFileList_"+str(userNum)+".txt"), 'r') as f:
    pathList = f.readlines()

wayPointList = []
for path in pathList:
    # print(path)
    if len(path.split(".")) < 2:
        continue
    with open(path.split("\n")[0], 'r') as f:
        endPoint = os.path.splitext(path.split("\n")[0])
        # print(endPoint)
        endPoint = (os.path.splitext(path.split("\n")[0])[0].split("_")[-1].split(","))
        # endPoint = (os.path.basename(path.split("\n")[0]).split("_")[-1].split(","))
        # print(endPoint)

        pathLines = f.readlines()
        for line in pathLines:
            data = line.split("\n")[0].split(",")
            if data[-1] == "True":
                wayPointList.append(data)
        wayPointList[-1][2] = str(endPoint[0])
        wayPointList[-1][3] = str(endPoint[1])
        print(wayPointList[-1])

# wayPointList = []
# for path in pathList:
#     with open(path.split("\n")[0], 'r') as f:
#         pathLines = f.readlines()
#     for line in pathLines:
#         data = line.split("\n")[0].split(",")
#         if data[-1] == "True":
#             wayPointList.append(data)

# txtとpickleで出力
with open(os.path.join(DATA_PATH, "occupancyPath/pathFileList_"+str(userNum)+".txt"), 'w') as f:
    # f.write([fname+"\n" for fname in pathList])
    for fname in pathList:
        f.write(fname)
with open(os.path.join(DATA_PATH, "occupancyPath/wayPointList_"+str(userNum)+".pickle"),mode = 'wb') as f:
    pickle.dump(wayPointList, f)
