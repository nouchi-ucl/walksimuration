#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
import csv
import numpy as np
from operator import attrgetter

# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import pathPlanning_v2
import searchShelf

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")
# DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")

DRAW_THRESHOLD_MIN = 8
DRAW_THRESHOLD_MAX = 12

PICK_TIME = 0

START_TIME = 1485239148
CUT_TIME = [START_TIME] * 9
CUT_TIME[2] = START_TIME + 1490
CUT_TIME[3] = START_TIME + 1350
CUT_TIME[4] = START_TIME + 1350
CUT_TIME[5] = START_TIME + 1330
CUT_TIME[6] = START_TIME + 1330
CUT_TIME[7] = START_TIME + 1250
CUT_TIME[8] = START_TIME + 1345


def run(userNum, drawFlag):
    # 各pickleファイルの読み込み
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    startPos = (43.3,46.6)


    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
    # print(gridMap)
    cellSize = gridMap[0][1].posx - gridMap[0][0].posx

    with open(os.path.join(DATA_PATH,"wms/WMS.csv"), 'r') as f:
        wmsData = csv.reader(f)

        # header行を読み飛ばす
        header = next(wmsData)
        next(wmsData)

        wmsList = []
        for line in wmsData:
            # print(line)
            terminalID = line[0]
            shelfNO = int(line[1])
            utime = int(line[2])

            if int(terminalID) == userNum:
                if utime < CUT_TIME[userNum]:
                    # print(shelfNO, utime, "xxxx")
                    continue
                wmsList.append([shelfNO, utime])
                print(shelfNO, utime)
            # 邪魔な空行を読み飛ばす
            # next(wmsData)
    print("length of wmsList...", len(wmsList))
    # リストを時間でソート
    wmsList.sort(key=lambda x:x[1])

    moveList = []
    with open(os.path.join(DATA_PATH,"agent6_label.csv"), 'r') as f:
        labelData = csv.reader(f)
        next(labelData)
        next(labelData)
        for label in labelData:
            if label[2] == 'move':
                moveList.append([label[0],label[1]])

    moveList.sort(key=lambda x:x[0])

    with open(os.path.join(DATA_PATH, 'simulation/'+ str(userNum) + '_simulatedTrajectory.pickle'), mode = 'rb') as f:
    # with open(os.path.join(DATA_PATH, 'simulation/'+ str(userNum) + '_simulatedTrajectory_time.pickle'), mode = 'rb') as f:
        agentTrajectory = pickle.load(f)

    result = []
    pathLength = 0.
    prevStep = None
    prevTime = 0.
    prevPos = None
    prevState = None
    prevPick = "start"
    prevRecord = None
    for record in agentTrajectory:
        # print(record)
        if prevPos is None:
            prevPos = np.array(record[2])
            prevStep = record[0]
            prevTime = record[1]
            currentState = None
            prevWmsTime = CUT_TIME[userNum]
            prevRecord = record
            continue
        currentPos = np.array(record[2])
        distance = np.linalg.norm(currentPos - prevPos)
        pathLength += distance
        # print(currentPos, prevPos, distance)
        # print(record[-1])
        prevState = currentState
        currentState = record[-1][0]

        if prevState == 'Pick' and currentState == 'Turn':
        # if record[-1][0] == 'Pick':
            print("pick", prevRecord[-1][1])
            step = prevRecord[0]
            time = prevRecord[1]
            diffStep = step - prevStep
            diffTime = time - prevTime
            pick = prevRecord[-1][1]
            for wms in wmsList:
                # print(wms[0], prevRecord[-1][1])
                # print("hoge" if wms[0] == int(prevRecord[-1][1]) else "false")
                if wms[0] == int(prevRecord[-1][1]):
                    wmsTime = wms[1]
                    diffTimeFromStart = wmsTime - CUT_TIME[userNum]

                    moveTime = 0
                    for move in moveList:
                        if prevWmsTime < int(move[0]) and int(move[0]) <= wmsTime:
                            if wmsTime < int(move[1]):
                                moveTime += wmsTime - int(move[0])
                            else:
                                moveTime += int(move[1]) - int(move[0])
                    print("moveTime", moveTime)
                    diffWmsTime = wmsTime - prevWmsTime - PICK_TIME
                    if moveTime == 0:
                        moveTime = 1
                    # result.append([prevPick, pick, diffStep, diffWmsTime, pathLength, pathLength/diffWmsTime, diffWmsTime/pathLength, wmsTime, diffTimeFromStart])
                    # print("prevPick :",prevPick, "pick :",pick, "step :",step, "time :",wmsTime, "\n    pathLength :", pathLength, "diffStep :",diffStep, "diffTime :",diffWmsTime, " length/time :", pathLength/diffWmsTime, "time/length :", diffWmsTime/pathLength)
                    result.append([prevPick, pick, diffStep, diffWmsTime, moveTime, pathLength, pathLength/moveTime, moveTime/pathLength, wmsTime, diffTimeFromStart])
                    # result.append([prevPick, pick, moveTime, pathLength, pathLength/moveTime])
                    print("prevPick :",prevPick, "pick :",pick, "step :",step, "time :",wmsTime, "\n    pathLength :", pathLength, "diffStep :",diffStep, "diffTime :",diffWmsTime, " length/time :", pathLength/diffWmsTime, "time/length :", diffWmsTime/pathLength)

                    prevWmsTime = wmsTime
            pathLength = 0.
            prevTime = time
            prevStep = step
            prevPick = pick
        prevRecord = record
        prevPos = currentPos

    with open(os.path.join(DATA_PATH, "simulateResult_"+str(userNum)+".txt"), "w") as f:
        f.write("Terminal ID:"+str(userNum)+",pickTime:"+str(PICK_TIME)+"\n")
        f.write("prevPick, pick, diffStep, diffTime, moveTime, pathLength, length/time, time/length, time, diffTimeFromStart\n")
        for line in result:
            print(line)
            f.write(",".join(map(str, line))+"\n")

        # f.write("\n\n\n???\nprevPick, pick, diffWmsTime, moveTime, pathLength, path/moveTime\n")
        # for line in result:
        #     if line[-1] < 0.4:
        #         array = [line[0],line[1],line[3],line[4],line[5],line[6]]
        #         f.write(",".join(map(str, array))+"\n")

    # drawFlag = False
    if drawFlag:


        # 表示用pltの設定
        fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
        # ax1.set_xlim(xlim[0]-10,xlim[1]+10)
        # ax1.set_ylim(ylim[0]-10,ylim[1]+10)
        ax1.set_xlim(20,65)
        ax1.set_ylim(15, 60)
        print("xlim", xlim[0],"  ",xlim[1])
        print("ylim", ylim[0],"  ",ylim[1])

        # # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
        # # 単純にpathの探索をするだけなら必要ない
        obstacleList = []
        obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
        obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
        obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
        print("obstacles...",len(obstacleList))

        for (x1, x2, y1, y2) in obstacleList:
            plt.plot([x1,x2],[y1,y1] , color = '#000000')
            plt.plot([x2,x2],[y1,y2] , color = '#000000')
            plt.plot([x2,x1],[y2,y2] , color = '#000000')
            plt.plot([x1,x1],[y2,y1] , color = '#000000')

        turnPointX = []
        turnPointY = []
        pickPointX = []
        pickPointY = []

        fname = os.path.join(DATA_PATH,"occupancyPath/pathFileList_"+ str(userNum)+ ".txt")
        pathList = []
        with open(fname, 'r') as f:
            data = f.readlines()
            pathList = [line.split("\n")[0] for line in data]

        # 各パスの描画と方向転換点，ピック点を描画
        for fName in pathList[DRAW_THRESHOLD_MIN:DRAW_THRESHOLD_MAX]:
            pickleName = fName[:-3]+"pickle"
            pickleName = os.path.join(DATA_PATH, "path/" +pickleName.split("/")[-1])
            with open(pickleName, mode='rb') as f:
                path = pickle.load(f)

            for (i,address) in enumerate(path.route):
                node = gridMap[address[0][1]][address[0][0]]
                if i == 0:
                    print("Red path is ", path.routeCost, " in ", len(path.route) -1, " step\n")
                    prevNode = node
                    # pickPointX.append(node.posx)
                    # pickPointY.append(node.posy)
                    continue
                if i == len(path.route) -1:
                    pickPointX.append(node.posx)
                    pickPointY.append(node.posy)
                else:
                    # plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#07d34f')
                    prevNode = node
                    if address[1] == True:
                        turnPointX.append(node.posx)
                        turnPointY.append(node.posy)
        # if startPos is not None:
            # pt = ax1.scatter(startPos[0], startPos[1], color = '#2ca9e1')
        # pt1 = ax1.scatter(turnPointX,turnPointY,color = '#07d34f')
        pt2 = ax1.scatter(pickPointX,pickPointY,color = '#c92b29')

        stepThresholdMin = 0
        stepThresholdMax = 0
        for i in range(DRAW_THRESHOLD_MIN):
            stepThresholdMin += result[i][2]
        for i in range(DRAW_THRESHOLD_MAX):
            stepThresholdMax += result[i][2]
        print(stepThresholdMin, stepThresholdMax, agentTrajectory[0], agentTrajectory[0][2][0])

        agentPosx = [pos[2][0] for pos in agentTrajectory[stepThresholdMin :stepThresholdMax]]
        agentPosy = [pos[2][1] for pos in agentTrajectory[stepThresholdMin :stepThresholdMax]]
        timeCount = [float(pos[1]) for pos in agentTrajectory[stepThresholdMin :stepThresholdMax]]
        # agentPosx = [pos[2][0] for pos in agentTrajectory[:1000]]
        # agentPosy = [pos[2][1] for pos in agentTrajectory[:1000]]
        # timeCount = [float(pos[1]) for pos in agentTrajectory[:1000]]

        prevPosx = agentPosx[0]
        prevPosy = agentPosy[0]
        for i in range(1, len(agentPosx)):
            posx = agentPosx[i]
            posy = agentPosy[i]
            plt.plot([posx, prevPosx], [posy, prevPosy], color = '#2ca9e1')
            prevPosx = posx
            prevPosy = posy

        plt.show()


if __name__ == "__main__":
    userNum = 2
    drawFlag = True
    run(userNum, drawFlag)
