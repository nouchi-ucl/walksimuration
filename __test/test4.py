import simpy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class agent(object):

    step = 5
    def __init__(self, env, number, mean, std, position, firstDistNum):
        # シミュレーション環境への参照
        self.env = env

        self.number = number

        # 時速の平均値
        self.mean = mean
        # 時速の標準偏差
        self.std = std

        # 現在向いている向き
        self.direction = np.array([0.0,0.0])
        # 現在の時速
        self.velocity = 0.0
        # 現在位置
        self.position = position
        # 直前の位置
        self.prev_position = position
        # 目的地のリスト(前から順番に)
        self.distinationList = []
        self.distinationList.append(np.array([75., 25.]))
        self.distinationList.append(np.array([50., 50.]))
        self.distinationList.append(np.array([75., 75.]))
        self.distinationList.append(np.array([25., 75.]))
        self.distinationList.append(np.array([25., 25.]))

        self.distCount = firstDistNum % len(self.distinationList)
        self.distination = self.distinationList[self.distCount]


    def update_velocity(self):
        # 現在時速を更新
        # 現在時速は 平均 mean, 標準偏差 std の正規分布に従う
        v = np.random.normal(self.mean, self.std)
        if v < 0:
            v = 0
        return v

    def update_position(self):
        # 現在位置を更新
        self.prev_position = self.position
        self.position = np.array(self.position + self.velocity * self.direction / 3600 * 1000 )

        # 目的地周辺まできたら目的地を更新する、なければ終了
        # 今回はループするように設定しておく
        distPos = np.linalg.norm(self.distination - self.position)
        if distPos < 5 :
            self.distCount += 1
            if self.distCount >= len(self.distinationList):
                self.distCount = 0
            self.distination = self.distinationList[self.distCount]

        # directionの更新(障害物がない設定なので目的地との直線になる)
        distPos = np.linalg.norm(self.distination - self.position)
        # print(distPos, self.distination - self.position)
        self.direction = (self.distination - self.position) / distPos
        # print(self.position, self.direction, self.direction / np.linalg.norm(self.direction))

        self.direction = self.direction / np.linalg.norm(self.direction)
        # self.position += self.velocity / 3600 * 1000
        # return self.position, self.direction
        # return [x,y],[d_x,d_y]
        return self.position, self.direction

    def run(self):
        # 自身のプロセス (generator) を返すメソッド
        while True:
            if env.now % self.step == 0:
                # step が経過するたび、現在時速を更新 (乱数をふりなおす)
                self.velocity = self.update_velocity()
            # form = '現在時刻 {0:2d} 位置: {1:.1f},{2:.1f} m 時速 {3:.1f} km 方向{4:.1f},{5:.1f}'
            # print(form.format(self.env.now, self.position[0], self.position[1], self.velocity, self.direction[0], self.direction[1]))
            print("time ", self.env.now, " pos ", self.position, " dist ", self.distination, "\n      velocity ", self.velocity, " direction ", self.direction, "  ")
            # self.update_position()
            self.position, self.direction = self.update_position()
            yield self.env.timeout(1)


# 環境の初期設定
def init_env(env, num_agents=4, mean=72, std=10):
    agents = []
    for i in range(0,num_agents):
        if i % 4 == 0:
            x = 25
            y = 25
        elif i % 4 == 1:
            x = 25
            y = 75
        elif i % 4 == 2:
            x = 75
            y = 75
        elif i % 4 == 3:
            x = 75
            y = 25
        newAgent = agent(env, i, mean=10, std=5, position=np.array([x,y]), firstDistNum=i)
        env.process(newAgent.run())
        agents.append(newAgent)
    return env, agents

np.random.seed(1)
env = simpy.Environment()
numAgents = 10
env, agents = init_env(env, num_agents=numAgents)


# m = agent(env,mean=10, std=5, position = np.array([25., 25.]))
# env.process(m.run())
# env.run(until=100)

# 描画用の figure, axes を作成
fig, (ax1) = plt.subplots(1, 1, figsize=(7, 7))
ax1.set_xlim(0,100)
ax1.set_ylim(0,100)
objs = []

step = 100
for i in range(step * numAgents):
    # 各グループの位置を描画
    x = [25,25,75,75,50]
    y = [25,75,75,25,50]
    pt1 = ax1.scatter([m.position[0] for m in agents],[m.position[1] for m in agents], color='#c92b2b')
    pt2 = ax1.scatter(x, y, color='#005ec4')
    objs.append([pt1,pt2])
    # シミュレーション環境を 1 ステップ進める (1単位時間でなく、次のプロセスが実行されるまで進める)
    env.step()

ani = animation.ArtistAnimation(fig, objs, interval=1, repeat=True)
plt.show()
