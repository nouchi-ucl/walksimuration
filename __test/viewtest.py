#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import os
import matplotlib.pyplot as plt
import pickle
import itertools
import csv
from operator import attrgetter
import time
import datetime


# 自作クラスファイルの読み込み(相対パス)
from classdef import nodeDef
from classdef import pathDef
import pathPlanning_v2
import searchShelf

DATA_PATH = os.path.join("/Users/nouchi/VirtualBox VMs/Ubuntu-simpy/workspace/sample_dataset_20170626/WarehouseB_30min")
# DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join(DATA_PATH, "pickle")


def run(pathfName):

    userNum = 6
    with open(os.path.join(DATA_PATH,"wms/WMS.csv"), 'r') as f:
        wmsData = csv.reader(f)

        # header行を読み飛ばす
        header = next(wmsData)
        next(wmsData)

        wmsList = []
        for line in wmsData:
            print(line)
            terminalID = line[0]
            shelfNO = line[1]
            utime = line[2]

            if int(terminalID) == userNum:
                wmsList.append([shelfNO, utime])

            # 邪魔な空行を読み飛ばす
            next(wmsData)

    # リストを時間でソート
    wmsList.sort(key=lambda x:x[1])

    startPos = None
    startPos = (43.3,46.6)


    pathList = []
    with open(pathfName, 'r') as f:
        data = f.readlines()
        pathList = [line.split("\n")[0] for line in data]
    # 以下表示用
    # 各pickleファイルの読み込み

    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]

    with open(os.path.join(PICKLE_PATH, 'gridMap.pickle'), mode = 'rb') as f:
        gridMap = pickle.load(f)
    # print(os.path.join(PICKLE_PATH, 'gridMap.pickle'))
    # print(gridMap)
    cellSize = gridMap[0][1].posx - gridMap[0][0].posx


    # 表示用pltの設定
    fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
    ax1.set_xlim(xlim[0]-10,xlim[1]+10)
    ax1.set_ylim(ylim[0]-10,ylim[1]+10)
    print("xlim", xlim[0],"  ",xlim[1])
    print("ylim", ylim[0],"  ",ylim[1])

    # # 棚と障害物と外壁とを統合したリスト(確認用に描画する時に必要)
    # # 単純にpathの探索をするだけなら必要ない
    obstacleList = []
    obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])
    obstacleList.extend([(xlim[0] - cellSize,xlim[0],ylim[0],ylim[1]),(xlim[1],xlim[1] + cellSize, ylim[0],ylim[1]),(xlim[0],xlim[1],ylim[0] - cellSize,ylim[0]),(xlim[0],xlim[1],ylim[1],ylim[1] + cellSize)])
    print("obstacles...",len(obstacleList))

    for (x1, x2, y1, y2) in obstacleList:
        plt.plot([x1,x2],[y1,y1] , color = '#000000')
        plt.plot([x2,x2],[y1,y2] , color = '#000000')
        plt.plot([x2,x1],[y2,y2] , color = '#000000')
        plt.plot([x1,x1],[y2,y1] , color = '#000000')

    turnPointX = []
    turnPointY = []
    pickPointX = []
    pickPointY = []

    threshold = 10

    for (i,wms) in enumerate(wmsList[:threshold]):
        print(wms)
        wmspos = prevPos = searchShelf.run(wms)
        utime = datetime.datetime.fromtimestamp(int(wms[1]))
        strtime = utime.strftime('%H:%M:%S')
        plt.scatter(wmspos[0],wmspos[1], color = '#2ca9e1')
        plt.text(wmspos[0],wmspos[1],str(i+1), size = 20, color = '#c92b29')
        plt.text(wmspos[0]+0.5,wmspos[1]-0.5,strtime, size = 15)
    # 各パスの描画と方向転換点，ピック点を描画
    # for fName in pathList[:threshold]:
    #     pickleName = fName[:-3]+"pickle"
    #     with open(pickleName, mode='rb') as f:
    #         path = pickle.load(f)
    #
    #     for (i,address) in enumerate(path.route):
    #         node = gridMap[address[0][1]][address[0][0]]
    #         if i == 0:
    #             print("Red path is ", path.routeCost, " in ", len(path.route) -1, " step\n")
    #             prevNode = node
    #             pickPointX.append(node.posx)
    #             pickPointY.append(node.posy)
    #             continue
    #         else:
    #             plt.plot([node.posx, prevNode.posx], [node.posy, prevNode.posy], color = '#07d34f')
    #             prevNode = node
    #             if address[1] == True:
    #                 turnPointX.append(node.posx)
    #                 turnPointY.append(node.posy)
    # if startPos is not None:
    #     pt = ax1.scatter(startPos[0], startPos[1], color = '#2ca9e1')
    # # pt1 = ax1.scatter(turnPointX,turnPointY,color = '#07d34f')
    # pt2 = ax1.scatter(pickPointX[:threshold],pickPointY[:threshold],color = '#c92b29')

    plt.show()


if __name__ == '__main__':
    # userNum = 2
    fname = os.path.join(DATA_PATH,"occupancyPath/pathFileList_6.txt")
    run(fname)
