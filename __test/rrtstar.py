#!/usr/bin/python
# -*- coding: utf-8 -*-
u"""
@brief: Path Planning Sample Code with Randamized Rapidly-Exploring Random Trees (RRT)
@author: AtsushiSakai(@Atsushi_twi)
@license: MIT
"""

import random
import math
import copy
import numpy as np
import os

# MAP_Path = os.path.join("/media/sf_Ubuntu-simpy/workspace/sample_dataset_20170602/WarehouseB_30min/map")
# MAP_Path = os.path.join("/Users/nouchi/Desktop/ipin_comp/map")
# MAP_Path = os.path.join("/Users/nouchi/Desktop/map")
DATA_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp")
PICKLE_PATH = os.path.join("/Users/nouchi/Desktop/ipin_comp/pickle")



class RRT():
    u"""
    Class for RRT Planning
    """

    def __init__(self, start, goal, obstacleList, randArea,
                 expandDis=0.2, goalSampleRate=40, maxIter=100000):
        u"""
        Setting Parameter
        start:Start Position [x,y]
        goal:Goal Position [x,y]
        obstacleList:obstacle Positions [[x,y,size],...]
        randArea:Ramdom Samping Area [min,max]
        """
        self.start = Node(start[0], start[1])
        self.end = Node(goal[0], goal[1])
        self.randAreaX = randArea[0]
        self.randAreaY = randArea[1]
        # self.minrand = randArea[0]
        # self.maxrand = randArea[1]
        self.expandDis = expandDis
        self.goalSampleRate = goalSampleRate
        self.maxIter = maxIter

    def Planning(self, animation=True):
        u"""
        Pathplanning
        animation: flag for animation on or off
        """
        animation = False

        self.nodeList = [self.start]
        for i in range(self.maxIter):
            # ランダムな目標地点を設定 (一定確率で目的地になる)
            rnd = self.get_random_point()
            # rndに一番近いノードを探す
            nind = self.GetNearestListIndex(self.nodeList, rnd)
            # 新しくノードを伸ばす
            newNode = self.steer(rnd, nind)
            #  print(newNode.cost)

            if self.__CollisionCheck(newNode, obstacleList):
                nearinds = self.find_near_nodes(newNode)
                newNode = self.choose_parent(newNode, nearinds)
                self.nodeList.append(newNode)
                self.rewire(newNode, nearinds)

            if animation:
                self.DrawGraph(rnd)

            if i % (self.maxIter / 20) == 0:
                print("Path Making... ", (i/self.maxIter) * 100, "%")

        # generate coruse
        lastIndex = self.get_best_last_index()
        path = self.gen_final_course(lastIndex)
        return path

    def choose_parent(self, newNode, nearinds):
        if len(nearinds) == 0:
            return newNode

        dlist = []
        for i in nearinds:
            dx = newNode.x - self.nodeList[i].x
            dy = newNode.y - self.nodeList[i].y
            d = math.sqrt(dx ** 2 + dy ** 2)
            theta = math.atan2(dy, dx)
            if self.check_collision_extend(self.nodeList[i], theta, d):
                dlist.append(self.nodeList[i].cost + d)
            else:
                dlist.append(float("inf"))

        mincost = min(dlist)
        minind = nearinds[dlist.index(mincost)]

        if mincost == float("inf"):
            print("mincost is inf")
            return newNode

        newNode.cost = mincost
        newNode.parent = minind

        return newNode

    def steer(self, rnd, nind):

        # expand tree
        nearestNode = self.nodeList[nind]
        theta = math.atan2(rnd[1] - nearestNode.y, rnd[0] - nearestNode.x)
        theta = theta + math.pi / 8
        direction = round(theta / (math.pi / 4), 0) * math.pi /4
        newNode = copy.deepcopy(nearestNode)
        if direction % (math.pi/2) == 0.0:
            newNode.x += self.expandDis * math.cos(direction)
            newNode.y += self.expandDis * math.sin(direction)
        else:
            newNode.x += self.expandDis * math.cos(direction) * math.sqrt(2)
            newNode.y += self.expandDis * math.sin(direction) * math.sqrt(2)

        newNode.cost += self.expandDis
        newNode.parent = nind
        return newNode

    def get_random_point(self):

        if random.randint(0, 100) > self.goalSampleRate:
            rnd = [random.uniform(self.randAreaX[0], self.randAreaX[1]),
                   random.uniform(self.randAreaY[0], self.randAreaY[1])]
        else:  # goal point sampling
            rnd = [self.end.x, self.end.y]

        return rnd

    def get_best_last_index(self):

        disglist = [self.calc_dist_to_goal(
            node.x, node.y) for node in self.nodeList]
        goalinds = [disglist.index(i) for i in disglist if i <= self.expandDis]
        #  print(goalinds)

        mincost = min([self.nodeList[i].cost for i in goalinds])
        for i in goalinds:
            if self.nodeList[i].cost == mincost:
                return i

        return None

    def gen_final_course(self, goalind):
        path = [[self.end.x, self.end.y]]
        while self.nodeList[goalind].parent is not None:
            node = self.nodeList[goalind]
            path.append([node.x, node.y])
            goalind = node.parent
        path.append([self.start.x, self.start.y])
        return path

    def calc_dist_to_goal(self, x, y):
        return np.linalg.norm([x - self.end.x, y - self.end.y])

    def find_near_nodes(self, newNode):
        nnode = len(self.nodeList)
        r = 50.0 * math.sqrt((math.log(nnode) / nnode))
        #  r = self.expandDis * 5.0
        dlist = [(node.x - newNode.x) ** 2 +
                 (node.y - newNode.y) ** 2 for node in self.nodeList]
        nearinds = [dlist.index(i) for i in dlist if i <= r ** 2]
        return nearinds

    def rewire(self, newNode, nearinds):
        nnode = len(self.nodeList)
        for i in nearinds:
            nearNode = self.nodeList[i]

            dx = newNode.x - nearNode.x
            dy = newNode.y - nearNode.y
            d = math.sqrt(dx ** 2 + dy ** 2)

            scost = newNode.cost + d

            if nearNode.cost > scost:
                theta = math.atan2(dy, dx)
                theta = theta + math.pi / 8
                direction = round(theta / (math.pi / 4), 0) * math.pi /4
                # newNode = copy.deepcopy(nearestNode)
                # if direction % (math.pi/2) == 0.0:
                #     newNode.x += self.expandDis * math.cos(direction)
                #     newNode.y += self.expandDis * math.sin(direction)
                # else:
                #     newNode.x += self.expandDis * math.cos(direction) * math.sqrt(2)
                #     newNode.y += self.expandDis * math.sin(direction) * math.sqrt(2)

                if self.check_collision_extend(nearNode, direction, d):
                # if self.check_collision_extend(nearNode, theta, d):
                    nearNode.parent = nnode - 1
                    nearNode.cost = scost

    def check_collision_extend(self, nearNode, theta, d):

        tmpNode = copy.deepcopy(nearNode)

        for i in range(int(d / self.expandDis)):
            tmpNode.x += self.expandDis * math.cos(theta)
            tmpNode.y += self.expandDis * math.sin(theta)
            if not self.__CollisionCheck(tmpNode, obstacleList):
                return False

        return True

    def DrawGraph(self, rnd=None):
        u"""
        Draw Graph
        """
        import matplotlib.pyplot as plt
        plt.clf()
        if rnd is not None:
            plt.plot(rnd[0], rnd[1], "^k")
        for node in self.nodeList:
            if node.parent is not None:
                plt.plot([node.x, self.nodeList[node.parent].x], [
                         node.y, self.nodeList[node.parent].y], "-g")

        for (x1, x2, y1, y2) in obstacleList:
            # plt.plot(ox, oy, "ok", ms=30 * size)
            # pos = [(x1,y1),(x2,y1),(x2,y2),(x1,y2)]
            # plt.plot([p[0] for p in pos], [p[1] for p in pos], color = '#000000')
            plt.plot([x1,x2],[y1,y1] , color = '#000000')
            plt.plot([x2,x2],[y1,y2] , color = '#000000')
            plt.plot([x2,x1],[y2,y2] , color = '#000000')
            plt.plot([x1,x1],[y2,y1] , color = '#000000')


        plt.plot(self.start.x, self.start.y, "xr")
        plt.plot(self.end.x, self.end.y, "xr")
        plt.axis([self.randAreaX[0], self.randAreaX[1], self.randAreaY[0], self.randAreaY[1]])
        # img = plt.imread(os.path.join(MAP_Path, "Map_image_R1.png"))
        # plt.imshow(img, extent=[xlim[0], xlim[1], ylim[0], ylim[1]], aspect=1)

        plt.grid(True)
        plt.pause(0.01)

    def GetNearestListIndex(self, nodeList, rnd):
        dlist = [(node.x - rnd[0]) ** 2 + (node.y - rnd[1])
                 ** 2 for node in nodeList]
        minind = dlist.index(min(dlist))

        return minind

    # def __CollisionCheck(self, node, obstacleList):
    #     for (ox, oy, size) in obstacleList:
    #         dx = ox - node.x
    #         dy = oy - node.y
    #         d = dx * dx + dy * dy
    #         if d <= size ** 2:
    #             return False  # collision
    #
    #     return True  # safe

    def __CollisionCheck(self, node, obstacleList):
        # print(obstacleList[0])
        # print(len(obstacleList))
        for(x1,x2,y1,y2) in obstacleList:
            if x1 <= node.x and node.x <= x2 and y1 <= node.y and node.y <= y2:
                return False
        return True

class Node():
    u"""
    RRT Node
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.cost = 0.0
        self.parent = None


if __name__ == '__main__':
    print("Start rrt start planning")
    import matplotlib.pyplot as plt
    import pickle
    # ====Search Path with RRT====
    # obstacleList = [
    #     (3, 5, 1, 2),
    #     (3, 6, 2, 3),
    #     # (3, 8, 5, 7),
    #     # (8, 10, 2, 5),
    #     # (7, 9, 9, 10),
    #     (1, 2, 2, 6)
    # ]  # [x,y,size(radius)]


    mapInfoList = []
    obstacleList = []
    obstacles = []
    shelf = []

    # pickleファイルからオブジェクトを復元
    with open(os.path.join(PICKLE_PATH, 'mapInfo.pickle'),mode = 'rb') as f:
        mapInfoList = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'shelf.pickle'),mode = 'rb') as f:
        shelf = pickle.load(f)

    with open(os.path.join(PICKLE_PATH, 'obstacle.pickle'),mode = 'rb') as f:
        obstacles = pickle.load(f)

    # print (mapInfoList)
    xlim = mapInfoList[0][0]
    ylim = mapInfoList[0][1]
    #
    # # 表示用pltの設定
    # fig, (ax1) = plt.subplots(1,1,figsize=(7,7))
    # ax1.set_xlim(xlim[0]-10,xlim[1]+10)
    # ax1.set_ylim(ylim[0]-10,ylim[1]+10)
    # print("xlim", xlim[0],"  ",xlim[1])
    # print("ylim", ylim[0],"  ",ylim[1])
    # objs = []



    # マップ外周を障害物として追加
    # obstacles.append(sim.addObstacle([(xlim[0],ylim[0]),(xlim[1],ylim[0]),(xlim[1],ylim[1]),(xlim[0],ylim[1])]))

    obstacleList.extend([(s[1],s[2],s[3],s[4]) for s in shelf])
    # obstacleList.append([(o[0],o[1],o[2],o[3]) for o in obstacles])
    obstacleList.extend([(o[0],o[1],o[2],o[3]) for o in obstacles])



    # Set Initial parameters
    rrt = RRT(start=[43.3, 46.6], goal=[85, 30],
              randArea=[xlim, ylim], obstacleList=obstacleList)
    path = rrt.Planning(animation=True)

    # Draw final path
    rrt.DrawGraph()
    plt.plot([x for (x, y) in path], [y for (x, y) in path], '-r')
    plt.grid(True)
    plt.pause(0.01)  # Need for Mac
    plt.show()
