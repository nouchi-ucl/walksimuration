#!/usr/bin/env python
import rvo2
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os

# from pandas import DataFrame as df
# %matplotlib inline


numofPeople = 50
theta = 2* np.pi / numofPeople
radius = 30
sim = rvo2.PyRVOSimulator(0.5, 10., 2*numofPeople, 1.5, 2, 1., 2)


gain = 10
offset_x= 0.2
offset_green = 0.6

def sigmoid(x, gain=1, offset_x=0):
    return ((np.tanh(((x+offset_x)*gain)/2)+1)/2)

def colorBarRGB(x):
    x = (x * 2) - 1
    red = sigmoid(x, gain, -1*offset_x)
    blue = 1-sigmoid(x, gain, offset_x)
    green = sigmoid(x, gain, offset_green) + (1-sigmoid(x,gain,-1*offset_green))
    green = green - 1.0
    return (blue,green,red)
#
# #入力値は0.0〜1.0の範囲
# data = [colorBarRGB(x*0.001) for x in range(0,1000)]
#
# color = df(data)
# color.plot()

def setvPref(agent, dest):
    pos = np.array(sim.getAgentPosition(agent))
    # pos = sim.getAgentPosition(agent)
    # currentDest = np.array(dest[0], dest[1])
    currentDest = np.array(dest)
    distance = np.linalg.norm(currentDest - pos)
    # print("pos ",pos," dest  ",dest)
    if distance < 0.3:
        vPref = (0,0)
        # print("つきました")
        return vPref
    direction = (currentDest - pos) / distance
    vPref = sim.getAgentMaxSpeed(agent) /2 *direction
    # print("setvPref", pos, dest, currentDest, distance, vPref)
    # vPref = (velocity[0] / np.linalg.norm(velocity), velocity[1] / np.linalg.norm(velocity))
    return (vPref[0], vPref[1])

# Pass either just the position (the other parameters then use
# the default values passed to the PyRVOSimulator constructor),
# or pass all available parameters.
agents = []
agentDest = []
agentColor = []
obstacles = []

# numofPeople = 15
# theta = 2* np.pi / numofPeople
# radius = 30

for i in range(0,numofPeople):
    x = radius * np.cos(i*theta)
    y = radius * np.sin(i*theta)
    agents.append(sim.addAgent((x,y)))
    xInv = -1 * x
    yInv = -1 * y
    agentDest.append((xInv, yInv))
    # agentColor.append(colorBarRGB(i*theta))
    print(x,y,xInv,yInv)



for (agent,dest) in zip(agents,agentDest):
    # print(agentDest[i])
    vmax = sim.getAgentMaxSpeed(agent)
    v = np.random.normal(vmax,0.2)
    sim.setAgentMaxSpeed(agent,v)
    vPref = setvPref(agent,dest)
    sim.setAgentPrefVelocity(agent, vPref)
    print(sim.getAgentPosition(agent), dest, vPref)
    # print(sim.getAgentVelocity(agent))
    # print("\nagent num ", agent, "vpref  ", vPref)

# obstacles.append(sim.addObstacle([(-20,0),(-15,-5),(-10,0),(-15,5)]))
# obstacles.append(sim.addObstacle([(0,-20),(5,-15),(0,-10),(-5,-15)]))
# obstacles.append(sim.addObstacle([(20,0),(15,5),(10,0),(15,-5)]))
# obstacles.append(sim.addObstacle([(0,20),(-5,15),(0,10),(5,15)]))
# sim.processObstacles()



fig, (ax1) = plt.subplots(1,1,figsize=(15,15))
ax1.set_xlim(-1*radius-20,radius+20)
ax1.set_ylim(-1*radius-20,radius+20)
objs = []


for step in range(300):
    sim.doStep()

    positions = ['(%5.3f, %5.3f)' % sim.getAgentPosition(agent_no)
                 for agent_no in agents]

    x = [sim.getAgentPosition(agent_no)[0] for agent_no in agents]
    y = [sim.getAgentPosition(agent_no)[1] for agent_no in agents]

    x1 = [0,radius,0,-1*radius]
    y1 = [radius,0,-1*radius,0]
    pt1 = ax1.scatter(x1,y1,color = '#2ca9e1')
    pt2 = ax1.scatter(x,y,color = '#c92b29')

    # pt2 = []
    # for (agent, color) in zip(agents, agentColor):
    #     x = sim.getAgentPosition(agent)[0]
    #     y = sim.getAgentPosition(agent)[1]
    #     r = '%02x' % int(color[0]*255)
    #     g = '%02x' % int(color[1]*255)
    #     b = '%02x' % int(color[2]*255)
        # colorCode = '#'+r+g+b
        # print(r,g,b,colorCode)
        # pt2.append(ax1.scatter(x,y,color=colorCode))

    for (agent,dest) in zip(agents,agentDest):
        # print(agentDest[i])
        vPref = setvPref(agent,dest)
        sim.setAgentPrefVelocity(agent, vPref)
        # print(sim.getAgentVelocity(agent))
        # print("\nagent num ", agent, "vpref  ", vPref)

    # for i in range(0,sim.getNumObstacleVertices()):
    #     pos = []
    #     pos.append(sim.getObstacleVertex(i))
    #     pos.append(sim.getObstacleVertex(sim.getNextObstacleVertexNo(i)))
    #     ax1.plot([p[0] for p in pos], [p[1] for p in pos], color = '#000000')

    objs.append([pt1,pt2])

ani = animation.ArtistAnimation(fig, objs, interval = 100, repeat = True)
ani.save('/media/sf_Ubuntu-simpy/ring.mp4')
# ani.save('/Users/nouchi/Desktop/ring.mp4')

plt.show()
