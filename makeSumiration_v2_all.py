#!/usr/bin/python
# -*- coding: utf-8 -*-
import os

import makeSumiration_v2

DATA_PATH = os.path.join("/media/sf_Ubuntu-simpy/workspace/test_dataset/WarehouseB_easy")

runlist = [[2],[3],[4],[5],[6],[7],[8]]
# runlist = [2,3,4,5,6,7,8]
for userNum in runlist:

    makeSumiration_v2.run(DATA_PATH, False, userNum)
